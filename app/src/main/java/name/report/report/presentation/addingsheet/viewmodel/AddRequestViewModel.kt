package name.report.report.presentation.addingsheet.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import name.report.report.App
import name.report.report.domain.TransmittedReportRequest
import name.report.report.domain.usecases.requests.CreateRequestUseCase
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class AddRequestViewModel : ViewModel(){
    @Inject
    lateinit var useCase: CreateRequestUseCase

    val addRequestLiveData = MutableLiveData<Boolean>()

    init {
        App.appComponent.plusAddRequestComponent().inject(this)
    }

    fun clickOnAddRequestFab(request: TransmittedReportRequest){
        useCase.execute(request)
                .subscribeBy(
                        onSuccess = {addRequestLiveData.value = true},
                        onError = {addRequestLiveData.value = false}
                )
    }
}
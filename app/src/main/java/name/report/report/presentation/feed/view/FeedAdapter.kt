package name.report.report.presentation.feed.view

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import name.report.report.Blur
import name.report.report.GlideApp
import name.report.report.MainActivity
import name.report.report.R
import name.report.report.utils.inflate
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.rv_item_request_feed.view.*
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.recycler_item_in_action.view.*
import name.report.report.domain.*
import name.report.report.presentation.*
import name.report.report.utils.visible
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*


@Suppress("DEPRECATION")
class FeedAdapter(private var feed: List<FeedUiModel> = listOf())
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var events: PublishSubject<UiEvent> = PublishSubject.create()

    fun events() = events.share()

    fun update(feed: List<FeedUiModel>) {
        this.feed = feed
        notifyDataSetChanged()
    }

    companion object {
        const val REQUEST_TYPE = 0
        const val CUR_USER_REQUEST = 1
        const val INCOMING_TYPE = 2
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = when (holder) {
        is RequestHolder -> holder.bind(feed[position] as RequestFeedUiModel)
        is CurUserHolder -> holder.bind(feed[position] as CurFeedUiModel)
        is IncomingHolder -> holder.bind(feed[position] as IncomingFeedUiModel)
        else -> throw IllegalArgumentException("Unknown holder type: ${holder::class}")
    }


    override fun onCreateViewHolder(parent: ViewGroup, type: Int) = when (type) {
        REQUEST_TYPE -> RequestHolder(parent.inflate(name.report.report.R.layout.rv_item_request_feed))
        CUR_USER_REQUEST -> CurUserHolder(parent.inflate(name.report.report.R.layout.recycler_item_in_action))
        INCOMING_TYPE -> IncomingHolder(parent.inflate(name.report.report.R.layout.rv_item_request_feed))
        else -> throw IllegalArgumentException("Unknown holder type: $type")
    }

    override fun getItemCount() = feed.size

    override fun getItemViewType(position: Int) = when (feed[position]) {
            is RequestFeedUiModel -> REQUEST_TYPE
            is CurFeedUiModel -> CUR_USER_REQUEST
            is IncomingFeedUiModel -> INCOMING_TYPE
        }

    inner class RequestHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var alertRequest: RequestToAlert

        init {
            RxView.clicks(itemView)
                    .map { ClickOnRequestItem(alertRequest) }
                    .subscribe(events)
        }

        @SuppressLint("SetTextI18n")
        fun bind(request: RequestFeedUiModel) = with(itemView) {
            if(request.photoUrl==null){
                ivUserImage.setImageDrawable(context.resources.getDrawable(name.report.report.R.drawable.anon_photo))
            }else{
                name.report.report.GlideApp.with(context)
                        .load(request.photoUrl)
                        .centerCrop()
                        .into(ivUserImage)
            }
            val pos = request.position
            val position: LatLng
            position = when(pos){
                is Point -> {
                    LatLng(pos.coordinate.lat, pos.coordinate.lng)
                }
                is Polygon -> {
                    LatLng((pos.coordinates[0].lat+pos.coordinates[2].lat)/2, (pos.coordinates[0].lng+pos.coordinates[2].lng)/2)
                }
                else -> {
                    LatLng(0.toDouble(), 0.toDouble())
                }
            }

            if(request.title!!.length>50)
                tvRequestTitle.text = request.title.substring(0, 50)+"..."
            else
                tvRequestTitle.text = request.title
            tvRequestTime.text = request.time.split(" ")[1]


            val photoUrl = request.photoUrl.toString()
            val title = request.title.toString()
            val description = request.description.toString()
            val time = request.time
            val place = request.place.toString()
            val userId = request.userId.toString()
            alertRequest = RequestToAlert(
                    position,
                    title,
                    description,
                    photoUrl,
                    request.requestId,
                    place,
                    time,
                    userId,
                    request.followed)
        }
    }

    inner class CurUserHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var alertRequest: RequestToAlertCur

        init {
            RxView.clicks(itemView)
                    .map {
                        ClickOnCurRequestItem(alertRequest)
                    }
                    .subscribe(events)
            RxView.longClicks(itemView)
                    .map {
                        Timber.e("RE:: check")
                        LongClickOnRequestItem(alertRequest)
                    }
                    .subscribe(events)
        }

        fun bind(request: CurFeedUiModel) = with(itemView) {
            Timber.e("RE::: $request")

            if(request.photoUrl==null){
                ivCurUserRequest.setImageDrawable(context.resources.getDrawable(name.report.report.R.drawable.anon_photo))
            }else{
                GlideApp.with(context)
                        .load(request.photoUrl)
                        .centerCrop()
                        .into(ivCurUserRequest)
            }

            if(request.place==null){
                ivCurUser.setImageDrawable(context.resources.getDrawable(name.report.report.R.drawable.anon_photo))
            }else{
                GlideApp.with(context)
                        .load(request.place)
                        .centerCrop()
                        .into(ivCurUser)
            }

            lastMessage.text = request.title
            tvCurUserTitle.text = request.description

            val df1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
            val df2 = SimpleDateFormat("hh:mm", Locale.getDefault())
            try {
                val result1 = df1.parse(request.time)
                val time = df2.format(result1)
                tvCurUserTime.text = time
            } catch (e: Exception) {
                e.printStackTrace()
                tvCurUserTime.text = request.time
            }


            val photoUrl = request.photoUrl.toString()
            val title = request.title.toString()
            val description = request.description.toString()
            val time = request.time
            val place = request.place.toString()
            val usreId = request.userId.toString()
            alertRequest = RequestToAlertCur(
                    LatLng(0.toDouble(), 0.toDouble()),
                    title,
                    description,
                    photoUrl,
                    request.requestId,
                    place,
                    time,
                    usreId,
                    context,
                    MainActivity.instance)
        }
    }

    inner class IncomingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var alertRequest: RequestToAlertIncoming

        init {
            RxView.clicks(itemView)
                    .map { ClickOnIncomingRequest(alertRequest) }
                    .subscribe(events)
        }

        @SuppressLint("SetTextI18n")
        fun bind(request: IncomingFeedUiModel) = with(itemView) {
            if(request.photoUrl==null){
                ivUserImage.setImageDrawable(context.resources.getDrawable(name.report.report.R.drawable.anon_photo))
            }else{
                name.report.report.GlideApp.with(context)
                        .load(request.photoUrl)
                        .centerCrop()
                        .into(ivUserImage)
            }
            if(request.description!!.length>50)
                tvRequestTitle.text = request.description.substring(0, 50)+"..."
            else
                tvRequestTitle.text = request.description

            Timber.e("RE:: ->${request.title}-")
            if(request.title!="REQUEST_IN_AREA"){
                reps.visible()
                reps.text = request.title + " REP"
            }

            val photoUrl = request.photoUrl.toString()
            val title = request.description.toString()
            val description = request.description.toString()

            val userId = request.userId.toString()
            alertRequest = RequestToAlertIncoming(
                     title,
                    description,
                    userId,
                    photoUrl,
                    context,
                    request.requestId)
        }
    }
}
package name.report.report.presentation.intro

import agency.tango.materialintroscreen.SlideFragmentBuilder
import android.Manifest
import android.content.Intent
import android.os.Bundle
import name.report.report.App
import name.report.report.MainActivity
import name.report.report.R
import name.report.report.data.cache.AppPreferences

import timber.log.Timber
import javax.inject.Inject


class IntroActivity : agency.tango.materialintroscreen.MaterialIntroActivity() {

    @Inject
    lateinit var appPreferences: AppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
            addSlide(SlideFragmentBuilder()
                    .backgroundColor(R.color.transparent)
                    .image(R.drawable.intro1)
                    .buttonsColor(R.color.colorPrimary)
                    .title(getString(R.string.on_demond_fs))
                    .description(getString(R.string.on_demond_ff))
                    .build())
            addSlide(SlideFragmentBuilder()
                    .backgroundColor(R.color.transparent)
                    .image(R.drawable.intro2)
                    .buttonsColor(R.color.colorPrimary)
                    .title(getString(R.string.on_demond_ss))
                    .description(getString(R.string.on_demond_sf))
                    .build())
            addSlide(SlideFragmentBuilder()
                    .backgroundColor(R.color.transparent)
                    .image(R.drawable.intro3)
                    .buttonsColor(R.color.colorPrimary)
                    .title(getString(R.string.on_demond_ts))
                    .description(getString(R.string.on_demond_tf))
                    .build())
            addSlide(SlideFragmentBuilder()
                    .backgroundColor(R.color.transparent)
                    .image(R.drawable.intro4)
                    .buttonsColor(R.color.colorPrimary)
                    .title(getString(R.string.where_are_you))
                    .description(getString(R.string.allow_report))
                    .build())
        }
}

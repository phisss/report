package name.report.report.presentation.map.view

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

import java.util.ArrayList

/**
 * Created by Hero_in_heaven on 08.01.2018.
 */

class OnMarkerClickListenerCollection : GoogleMap.OnMarkerClickListener {
    private val registeredListeners = ArrayList<GoogleMap.OnMarkerClickListener>()

    fun registerListener(listener: GoogleMap.OnMarkerClickListener) {
        registeredListeners.add(listener)
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        registeredListeners.forEach {
            it.onMarkerClick(marker)
        }
        return true
    }
}

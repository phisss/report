package name.report.report.presentation.searchplace

import java.util.ArrayList

/**
 * Created by Hero_in_heaven on 07.12.2017.
 */

interface SavedPlaceListener {
    fun onSavedPlaceClick(mResultList: ArrayList<SavedAddress>, position: Int)
}
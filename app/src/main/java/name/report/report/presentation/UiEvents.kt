package name.report.report.presentation

import name.report.report.domain.RequestToAlert
import name.report.report.domain.RequestToAlertCur
import name.report.report.domain.RequestToAlertIncoming

sealed class UiEvent

class Refresh : UiEvent()

data class ClickOnRequestItem(val requestToAlert: RequestToAlert) : UiEvent()
data class ClickOnCurRequestItem(val requestToAlertCur: RequestToAlertCur) : UiEvent()
data class LongClickOnRequestItem(val requestToAlertCur: RequestToAlertCur) : UiEvent()
data class ClickOnIncomingRequest(val requestToAlertIncoming: RequestToAlertIncoming): UiEvent()
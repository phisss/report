package name.report.report.presentation.slider.view

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.rxkotlin.subscribeBy
import name.report.report.App
import name.report.report.MainActivity
import name.report.report.chat.DefaultMessagesActivity
import name.report.report.chat.DefaultMessagesActivityInfo
import name.report.report.domain.DeleteRequest
import name.report.report.domain.ReportRequest
import name.report.report.domain.usecases.requests.DeleteRequestUseCase
import name.report.report.domain.usecases.requests.GetCurrentUserRequestsRemoteUseCase
import name.report.report.presentation.*
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Hero_in_heaven on 06.12.2017.
 */
class SliderViewModel : ViewModel() {

    init{
        App.appComponent.plusFeedComponent().inject(this)
    }

    @Inject
    lateinit var useCase: GetCurrentUserRequestsRemoteUseCase

    @Inject
    lateinit var deleteUseCase: DeleteRequestUseCase

    val curUserRequests = MutableLiveData<List<ReportRequest>>()
    val openAlert = MutableLiveData<DeleteRequest>()
    val startProgressBar = MutableLiveData<Boolean>()
    val clearRequest = MutableLiveData<Boolean>()
    val mainActivity = MutableLiveData<MainActivity>()

    fun onEvent(event: UiEvent) {
        when (event) {
            is Refresh -> {}
            is ClickOnCurRequestItem -> {
                if (event.requestToAlertCur._id=="user") {
                    clearRequest.value = true
                    DefaultMessagesActivityInfo.open(mainActivity.value!!, event.requestToAlertCur.description)
                }else {
                    DefaultMessagesActivity.open(event.requestToAlertCur.context,
                            event.requestToAlertCur._id,
                            event.requestToAlertCur.title,
                            event.requestToAlertCur.userId)
                }
            }
            is LongClickOnRequestItem -> {
                openAlert.value = DeleteRequest(event.requestToAlertCur._id, event.requestToAlertCur.title, event.requestToAlertCur.userId)
            }
        }
    }

}
@file:Suppress("DEPRECATION")

package name.report.report.presentation.feed.view

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import com.google.android.gms.location.LocationServices
import name.report.report.App
import name.report.report.R
import name.report.report.presentation.main.viewmodel.MainActivityViewModel
import name.report.report.presentation.slider.view.SliderViewModel
import name.report.report.utils.gone
import name.report.report.utils.toDp
import name.report.report.utils.visible
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.delete_alert.view.*
import kotlinx.android.synthetic.main.fragment_feed.*
import name.report.report.domain.*
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class FeedFragment : Fragment() {

    private var curUserHash = HashMap<String, ReportRequest>()
    private var curUserList = ArrayList<ReportRequest>()
    private var arrayListToCur = ArrayList<ReportRequest>()
    private var incomingList = ArrayList<ReportRequest>()
    private var incomingHash = HashMap<String, ReportRequest>()

    val adapter by lazy {FeedAdapter()}
    companion object {
        const val FEED_TYPE = 0
        const val CUR_USER_REQUEST_TYPE = 1
        const val INCOMING_REQUESTS_TYPE = 2
        const val TYPE_KEY = "key"
        fun newInstance(type: Int) = FeedFragment().apply {
            arguments = Bundle().apply {
                putInt(TYPE_KEY, type)
            }
        }
    }

    private val emptyArea = MutableLiveData<Boolean>()
    private val emptyToYou = MutableLiveData<Boolean>()

    private val emptyMe = MutableLiveData<Boolean>()
    private val emptyFollow = MutableLiveData<Boolean>()

    private val type: Int by lazy { arguments!!.getInt(TYPE_KEY) }


    private val mainActivityViewModel: MainActivityViewModel by lazy {
        ViewModelProviders.of(activity!!).get(MainActivityViewModel::class.java)
    }
    private val curUserViewModel: SliderViewModel by lazy {
        ViewModelProviders.of(parentFragment!!).get(SliderViewModel::class.java)
    }

    @Inject
    lateinit var mapper: FeedMapper

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.plusFeedComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?)
            = inflater.inflate(name.report.report.R.layout.fragment_feed, container, false)!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when(type){
            FEED_TYPE -> {
                adapter.events().subscribe(mainActivityViewModel::onEvent)
                noItemsImage.setImageDrawable(resources.getDrawable(name.report.report.R.drawable.no_feed))
                noReq.text = getString(R.string.no_requests)
            }
            CUR_USER_REQUEST_TYPE -> {
                adapter.events().subscribe(curUserViewModel::onEvent)
                noItemsImage.setImageDrawable(resources.getDrawable(name.report.report.R.drawable.ic_you_dont_have_send_request))
                noReq.text = getString(R.string.no_requests_2)
            }
            INCOMING_REQUESTS_TYPE -> {
                adapter.events().subscribe(mainActivityViewModel::onEvent)
                noItemsImage.setImageDrawable(resources.getDrawable(name.report.report.R.drawable.ic_no_notifications))
                noReq.text = getString(R.string.no_requests_3)
            }
        }

        initEmptyLayoutVisible()

        rvFeed.adapter = adapter
        rvFeed.layoutManager = LinearLayoutManager(context)

        val liveData: MutableLiveData<List<ReportRequest>> = when(type) {
            FEED_TYPE ->  mainActivityViewModel.requestsInFeed
            CUR_USER_REQUEST_TYPE -> curUserViewModel.curUserRequests
            INCOMING_REQUESTS_TYPE -> mainActivityViewModel.requestsToUser
            else -> throw IllegalStateException("BAD TYPE")
        }


        if(type == CUR_USER_REQUEST_TYPE) {
            mainActivityViewModel.firstReport.observe(this, Observer {
                adapter.update(mapper.map(it!!, CUR_USER_REQUEST_TYPE))
                noFeedLL.gone()
            })
            curUserViewModel.clearRequest.observe(this, Observer {
                adapter.update(mapper.map(listOf(), CUR_USER_REQUEST_TYPE))
            })
        }

        liveData.observe(this, Observer {
            //TODO(" переписать на useCase ")
            when (type) {
                CUR_USER_REQUEST_TYPE -> {
                    var i = 0
                    curUserHash.clear()
                    emptyMe.value = it!!.isEmpty()
                    getFollowedRequests()
                    if(it.isEmpty())
                        adapter.update(mapper.map(it, type))
                    it.forEach {
                        request ->
                        mainActivityViewModel.getMessageUseCase.execute(request._id)
                                .subscribeBy(
                                        onSuccess = { dialog ->
                                            mainActivityViewModel.getUserByIdUseCase.execute(dialog.messages.last().userId!!)
                                                    .subscribeBy(
                                                            onSuccess = { user ->
                                                                if(request.message!!.length>25)
                                                                    request.message = request.message!!.substring(0, 25)+"..."
                                                                request.description = request.message
                                                                request.place = mainActivityViewModel.getCurrUserUseCase.execute()!!.photo
                                                                request.message = if (dialog.messages.last().message != null)
                                                                    dialog.messages.last().message.toString()
                                                                else
                                                                    dialog.messages.last().attachments!![0].type

                                                                request.user = ReportUser(photo = user.photo, userId = user.userId)
                                                                i++
                                                                if (i == it.size) {
                                                                    it.forEach {
                                                                        curUserHash.put(it._id, it)
                                                                    }
                                                                    curUserList.clear()
                                                                    curUserHash.forEach{
                                                                        curUserList.add(it.value)
                                                                    }
                                                                    adapter.update(mapper.map(curUserList, type))
                                                                }
                                                            },
                                                            onError = {}
                                                    )
                                        },
                                        onError = {}
                                )
                    }
                }
                FEED_TYPE ->{
                    if(it!!.isEmpty())
                        noFeedLL.visible()
                    else
                        noFeedLL.gone()
                    mainActivityViewModel.getFollowDialogsUseCase.execute()
                            .subscribeBy(
                                    onSuccess = {
                                        followedRequests ->
                                        it.forEach {
                                            var flag = false
                                            followedRequests.forEach {
                                                followedRequest ->
                                                if(followedRequest.request!=null){
                                                    if(followedRequest.request._id == it._id)
                                                        flag = true
                                                }
                                            }
                                            it.answered = flag
                                        }
                                        adapter.update(mapper.map(it, type))
                                    },
                                    onError ={

                                    }
                            )

                }
                INCOMING_REQUESTS_TYPE -> {
                    incomingHash.clear()
                    getInYourArea()
                    it!!.forEach{
                        incomingHash.put(it._id, it)
                    }
                    incomingList.clear()
                    incomingHash.forEach{
                        incomingList.add(it.value)
                    }
                    adapter.update(mapper.map(incomingList, type))
                }
            }
            progressBar.gone()
        })

        curUserViewModel.openAlert.observe(this, Observer {
            if(type == CUR_USER_REQUEST_TYPE) {
                if(it!!.userId == mainActivityViewModel.getCurrUserUseCase.execute()!!.userId)
                    createAlert(it.title, it._id)
                else
                    createAlertToUnfollow(it.title, it._id)
            }
        })
    }

    private fun initEmptyLayoutVisible(){
        emptyArea.value = false
        emptyToYou.value = false
        emptyMe.value = false
        emptyFollow.value = false

        emptyToYou.observe(this, Observer {
            if(type == INCOMING_REQUESTS_TYPE)
                if(it!!&&emptyArea.value!!)
                    noFeedLL.visible()
                else
                    noFeedLL.gone()
        })
        emptyArea.observe(this, Observer {
            if(type == INCOMING_REQUESTS_TYPE)
                if(it!!&&emptyToYou.value!!)
                    noFeedLL.visible()
                else
                    noFeedLL.gone()
        })

        emptyMe.observe(this, Observer {
            if(type == CUR_USER_REQUEST_TYPE)
                if(it!!&&emptyFollow.value!!)
                    noFeedLL.visible()
                else
                    noFeedLL.gone()

        })
        emptyFollow.observe(this, Observer {
            if(type == CUR_USER_REQUEST_TYPE)
                if(it!!&&emptyMe.value!!)
                    noFeedLL.visible()
                else
                    noFeedLL.gone()
        })
    }

    private fun getFollowedRequests(){
        mainActivityViewModel.getFollowDialogsUseCase.execute()
                .subscribeBy(
                        onSuccess = {
                            var i = 0
                            arrayListToCur.clear()
                            emptyFollow.value = it.isEmpty()
                            it.forEach {
                                if (it.request != null)
                                    arrayListToCur.add(it.request)
                            }
                            arrayListToCur.forEach { request ->
                                mainActivityViewModel.getMessageUseCase.execute(request._id)
                                        .subscribeBy(
                                                onSuccess = { dialog ->
                                                    mainActivityViewModel.getUserByIdUseCase.execute(dialog.messages.last().userId!!)
                                                            .subscribeBy(
                                                                    onSuccess = { user ->
                                                                        if (request.message!!.length > 25)
                                                                            request.message = request.message!!.substring(0, 25) + "..."
                                                                        request.description = request.message
                                                                        dialog.users.forEach {
                                                                            if(it.userId == request.userId)
                                                                                request.place = it.photo
                                                                        }
                                                                        request.message = if (dialog.messages.last().message != null)
                                                                            dialog.messages.last().message.toString()
                                                                        else
                                                                            dialog.messages.last().attachments!![0].type
                                                                        request.user = ReportUser(photo = user.photo, userId = user.userId)
                                                                        i++
                                                                        if (i == arrayListToCur.size) {
                                                                            arrayListToCur.forEach {
                                                                                curUserHash.put(it._id, it)
                                                                            }
                                                                            curUserList.clear()
                                                                            curUserHash.forEach {
                                                                                curUserList.add(it.value)
                                                                            }
                                                                            adapter.update(mapper.map(curUserList, type))
                                                                        }
                                                                    }
                                                            )
                                                }
                                        )
                            }
                        }
                )
    }

    @SuppressLint("MissingPermission")
    private fun getInYourArea(){
        val locationProvider = LocationServices.getFusedLocationProviderClient(activity!!)
        locationProvider.lastLocation.addOnSuccessListener {
            if (it != null) {
                mainActivityViewModel.requests.getRequestsInRadiusRemoteUseCase.execute(
                        it.longitude, it.latitude, 1)
                        .subscribeBy(
                                onSuccess = {
                                    it.forEach{
                                        it.description = "REQUEST_IN_AREA"
                                        incomingHash.put(it._id, it)
                                    }
                                    incomingList.clear()
                                    incomingHash.forEach{
                                        incomingList.add(it.value)
                                    }
                                    adapter.update(mapper.map(incomingList, type))

                                },
                                onError = {

                                }
                        )
            }
        }

    }

    @SuppressLint("InflateParams")
    private fun createAlert(text: String, id: String) {
        val view = layoutInflater.inflate(name.report.report.R.layout.delete_alert, null)
        val deleteRequestTV = view.findViewById<TextView>(name.report.report.R.id.deleteRequestText)
        val delBtn = view.findViewById<TextView>(name.report.report.R.id.delete)
        val closeDelBtn = view.findViewById<TextView>(name.report.report.R.id.closeDelete)
        deleteRequestTV.text = text
        val adb = AlertDialog.Builder(activity!!, name.report.report.R.style.CustomAlertDialog)
        adb.setView(view)
        val alert = adb.create()
        alert!!.show()
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(alert.window!!.attributes)
        lp.width = resources.displayMetrics.widthPixels - 80.toDp(resources)
        alert.window!!.attributes = lp
        view.delete.text = getString(R.string.request_delete)
        delBtn.setOnClickListener({
            curUserViewModel.deleteUseCase.execute(id)
                    .subscribeBy(
                            onSuccess = {
                                Timber.e("RE:: >> DELETE SUCCESS")
                                curUserViewModel.useCase.execute()
                                        .subscribeBy(
                                                onSuccess = {
                                                    curUserViewModel.curUserRequests.value = it
                                                },
                                                onError = {
                                                }
                                        )
                            },
                            onError = {
                            }
                    )
            alert.dismiss()
        })
        closeDelBtn.setOnClickListener({ alert.dismiss() })
    }

    @SuppressLint("InflateParams")
    private fun createAlertToUnfollow(text: String, id: String) {
        val view = layoutInflater.inflate(name.report.report.R.layout.delete_alert, null)
        val deleteRequestTV = view.findViewById<TextView>(name.report.report.R.id.deleteRequestText)
        view.deleteTitle.text = getString(R.string.unfollow_request)
        deleteRequestTV.text = text
        val adb = AlertDialog.Builder(activity!!, name.report.report.R.style.CustomAlertDialog)
        adb.setView(view)
        val alert = adb.create()
        alert!!.show()
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(alert.window!!.attributes)
        lp.width = resources.displayMetrics.widthPixels - 80.toDp(resources)
        alert.window!!.attributes = lp

        view.delete.text = getString(R.string.unfollow).toUpperCase()
        view.delete.setOnClickListener({
           mainActivityViewModel.leaveUseCase.execute(id)
                    .subscribeBy(
                            onSuccess = {
                                curUserViewModel.useCase.execute()
                                        .subscribeBy(
                                                onSuccess = {
                                                    curUserViewModel.curUserRequests.value = it
                                                },
                                                onError = {

                                                }
                                        )
                            },
                            onError = {

                            }
                    )

            alert.dismiss() })
        view.closeDelete.setOnClickListener({ alert.dismiss() })
    }
}

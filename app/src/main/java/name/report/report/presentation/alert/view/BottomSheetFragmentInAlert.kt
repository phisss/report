package name.report.report.presentation.alert.view

import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.design.widget.BottomSheetDialogFragment
import android.support.design.widget.Snackbar
import android.view.View
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.request_bottom_sheet.*
import kotlinx.android.synthetic.main.request_bottom_sheet.view.*
import name.report.report.GlideApp
import name.report.report.MainActivity
import name.report.report.R
import name.report.report.chat.DefaultMessagesActivity
import name.report.report.domain.RequestToAlert
import name.report.report.presentation.main.viewmodel.MainActivityViewModel
import name.report.report.utils.gone
import name.report.report.utils.visible
import timber.log.Timber


class BottomSheetFragmentInAlert : BottomSheetDialogFragment() {


    private val viewModel: MainActivityViewModel by lazy { ViewModelProviders.of(activity!!).get(MainActivityViewModel::class.java) }

    companion object {
        var sendRequest = MutableLiveData<RequestToAlert>()
    }

    private var onFollowClickListener: View.OnClickListener = View.OnClickListener{
        view ->
        viewModel.joinUseCase.execute(viewModel.reportRequest.value!!._id)
                .subscribeBy(
                        onSuccess = {
                            Snackbar.make(view, getString(R.string.now_you_follow), Snackbar.LENGTH_SHORT).show()
                            view.followTV.text = getString(R.string.unfollow)
                            view.followIV.setImageDrawable(resources.getDrawable(R.drawable.ic_notifications_off))
                            view.followLL.setOnClickListener(onUnfollowClickListener)
                        },
                        onError = {
                            Snackbar.make(view, getString(R.string.something_went_wrong), Snackbar.LENGTH_SHORT).show()
                        }
                )
    }

    private var onUnfollowClickListener: View.OnClickListener = View.OnClickListener{
        view ->
        viewModel.leaveUseCase.execute(viewModel.reportRequest.value!!._id)
                .subscribeBy(
                        onSuccess = {
                            Snackbar.make(view, getString(R.string.now_you_not_follow), Snackbar.LENGTH_SHORT).show()
                            view.followTV.text = getString(R.string.follow)
                            view.followIV.setImageDrawable(resources.getDrawable(R.drawable.ic_notifications_on))
                            view.followLL.setOnClickListener(onFollowClickListener)
                        },
                        onError = {
                            Snackbar.make(view, getString(R.string.something_went_wrong), Snackbar.LENGTH_SHORT).show()
                        }
                )
    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val view = View.inflate(context, R.layout.request_bottom_sheet, null)
        dialog.setContentView(view)
        view.showOnMapLL.setOnClickListener({
            dismiss()
            viewModel.movingFromAlert.value = true
            viewModel.posToMove.value = viewModel.reportRequest.value!!.position
            (activity!! as MainActivity).setPanelState(name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.COLLAPSED)
        })

        view.openChatLL2.setOnClickListener({
            dismiss()
            DefaultMessagesActivity.open(context!!,
                    viewModel.reportRequest.value!!._id,
                    viewModel.reportRequest.value!!.description,
                    viewModel.reportRequest.value!!.userId)
            Timber.e("RE:: ${viewModel.reportRequest.value}")
        })
        view.followLL.setOnClickListener (onFollowClickListener)
        if(viewModel.reportRequest.value!!.followed){
            view.followTV.text = getString(R.string.unfollow)
            view.followIV.setImageDrawable(resources.getDrawable(R.drawable.ic_notifications_off))
            view.followLL.setOnClickListener(onUnfollowClickListener)
        }else{
            view.followTV.text = getString(R.string.follow)
            view.followIV.setImageDrawable(resources.getDrawable(R.drawable.ic_notifications_on))
            view.followLL.setOnClickListener(onFollowClickListener)

        }
        viewModel.getFollowDialogsUseCase.execute()
                .subscribeBy(
                        onSuccess = {
                            try {
                                view.followTV.text = getString(R.string.follow)
                                view.followIV.setImageDrawable(resources.getDrawable(R.drawable.ic_notifications_on))
                                view.followLL.setOnClickListener(onFollowClickListener)
                            }catch (e: Exception){
                                e.printStackTrace()
                            }
                            it.forEach{
                                if(it.request!=null)
                                    if (it.request._id == viewModel.reportRequest.value!!._id) {
                                        view.followTV.text = getString(R.string.unfollow)
                                        view.followIV.setImageDrawable(resources.getDrawable(R.drawable.ic_notifications_off))
                                        view.followLL.setOnClickListener(onUnfollowClickListener)
                                    }
                            }
                        },
                        onError ={

                        }
                )


        viewModel.fromMap.observe(this, Observer{
            view.alertTitle.text = viewModel.reportRequest.value?.description
            view.alertDate.text = viewModel.reportRequest.value?.time!!.split(" ")[1]

            if(it!!)
                view.showOnMapLL.gone()
            else
                view.showOnMapLL.visible()
            try {
                val photoUrl = viewModel.reportRequest.value?.photoUrl
                if(photoUrl.equals("null")){
                    view.alertUserImage.setImageDrawable(context!!.resources.getDrawable(name.report.report.R.drawable.anon_photo))
                }else{
                    GlideApp.with(context)
                            .load(photoUrl)
                            .centerCrop()
                            .into(view.alertUserImage)
                }
            }catch (e: Exception) {}
        })
    }
}

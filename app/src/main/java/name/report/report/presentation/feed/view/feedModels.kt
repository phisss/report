package name.report.report.presentation.feed.view

import name.report.report.domain.Position

sealed class FeedUiModel

sealed class CurUiModel

data class RequestFeedUiModel (
        val time: String,
        val photoUrl: String?,
        val title: String?,
        val requestId: String,
        val position: Position?,
        val description: String?,
        val place: String?,
        val userId: String?,
        val followed: Boolean
) : FeedUiModel()

data class CurFeedUiModel (
        val time: String,
        val photoUrl: String?,
        val title: String?,
        val requestId: String,
        val position: Position?,
        val description: String?,
        val place: String?,
        val userId: String?
) : FeedUiModel()

data class IncomingFeedUiModel (
        val time: String,
        val photoUrl: String?,
        val title: String?,
        val requestId: String,
        val position: Position?,
        val description: String?,
        val place: String?,
        val userId: String?
) : FeedUiModel()

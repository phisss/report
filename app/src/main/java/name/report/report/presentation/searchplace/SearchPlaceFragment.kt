package name.report.report.presentation.searchplace

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import name.report.report.R
import name.report.report.presentation.main.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.fragment_search_place.*


class SearchPlaceFragment: Fragment(){

    companion object {
        val instance
            get() = SearchPlaceFragment()
    }

    private val viewModel: MainActivityViewModel by lazy { ViewModelProviders.of(activity!!).get(MainActivityViewModel::class.java) }


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
            = inflater.inflate(name.report.report.R.layout.fragment_search_place, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.searchCreated.value = viewModel.searchCreated.value == null
        viewModel.adapter.observe(this, Observer {
            if(rvSearchResult!=null) {
                rvSearchResult.adapter = it
                rvSearchResult.layoutManager = LinearLayoutManager(activity)
            }
        })
    }
}
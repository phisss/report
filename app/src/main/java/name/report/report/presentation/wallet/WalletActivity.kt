package name.report.report.presentation.wallet

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager

import kotlinx.android.synthetic.main.wallet_activity.*
import name.report.report.R
import name.report.report.domain.RepointtoList
import timber.log.Timber

class WalletActivity : AppCompatActivity() {

    private val repointList = ArrayList<RepointtoList>()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wallet_activity)
        closeWallet.setOnClickListener({finish()})
        balance.text = intent.getIntExtra("num", 0).toString() + " REPOINTS"
        window.statusBarColor = ContextCompat.getColor(this, R.color.main_hint)

        val userName = intent.getStringExtra("user").split(".")
        val userPhoto = intent.getStringExtra("photo").split(" ")
        val userRepoints = intent.getStringExtra("rep").split(".")
        var i = intent.getStringExtra("user").split(".").size-2


        while (i >= 0 ) {
            val item = RepointtoList(userPhoto[i], userName[i], userRepoints[i])
            Timber.e("RE:: $item")
            repointList.add(item)
            i--
        }

        rvRepointList.setHasFixedSize(true)
        rvRepointList.layoutManager = LinearLayoutManager(this)
        rvRepointList.adapter = RepointListAdapter(repointList)
    }

}

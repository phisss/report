package name.report.report.presentation.search

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import kotlinx.android.synthetic.main.custom_tab.view.*
import kotlinx.android.synthetic.main.fragment_search.*
import name.report.report.R
import name.report.report.SearchViewPagerAdapter

/**
 * Created by Hero_in_heaven on 12.01.2018.
 */
class SearchByTopicFragment : Fragment() {


    companion object {
        val instance
            get() = SearchFragment()
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
            = inflater.inflate(name.report.report.R.layout.fragment_search_by_topics, container, false)



}
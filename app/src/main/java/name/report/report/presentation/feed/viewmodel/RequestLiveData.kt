package name.report.report.presentation.feed.viewmodel

import android.arch.lifecycle.LiveData
import name.report.report.domain.ReportRequest
import name.report.report.domain.usecases.requests.GetRequestsInRadiusRemoteUseCase
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class RequestLiveData(
        val getRequestsInRadiusRemoteUseCase: GetRequestsInRadiusRemoteUseCase
) : LiveData<List<ReportRequest>>() {

    private val disposables = CompositeDisposable()

    override fun onActive() {
        Timber.e("RE:: onActive")
        getRequestsInRadiusRemoteUseCase.execute(0.0, 0.0, 200000000000000)
                .subscribeBy(
                        onError = { Timber.d("RE:: Error") }
                )
                .addTo(disposables)
    }

    override fun onInactive() {
        Timber.e("RE:: onInactive")
        disposables.clear()
    }
}

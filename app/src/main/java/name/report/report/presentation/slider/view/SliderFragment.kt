package name.report.report.presentation.slider.view

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.michael.easydialog.EasyDialog
import name.report.report.MainActivity
import name.report.report.ViewPagerAdapter
import name.report.report.presentation.main.viewmodel.MainActivityViewModel
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.custom_tab.view.*
import kotlinx.android.synthetic.main.extra_layout.view.*
import kotlinx.android.synthetic.main.fragment_tabs.*
import kotlinx.android.synthetic.main.slider_main.*
import name.report.report.domain.ReportRequest
import name.report.report.R


@Suppress("DEPRECATION")
/**
* Created by Hero_in_heaven on 26.11.2017.
*/

class SliderFragment : Fragment() {

    companion object {
        val instance
            get() = SliderFragment()
    }

    private val tabIcons = intArrayOf(R.drawable.ic_exclude_red,
            R.drawable.ic_bell,
            R.drawable.ic_incoming_red,
            R.drawable.ic_exclude_gray,
            R.drawable.ic_bell_c,
            R.drawable.ic_incoming_gray)

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_tabs, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Tab init
        curUserViewModel.mainActivity.value = activity!! as MainActivity
        tabsInitialization()
        updateTabs(true, false, false)
    }

    private val curUserViewModel: SliderViewModel by lazy{ ViewModelProviders.of(this).get(SliderViewModel::class.java)}
    private val mainActivityViewModel: MainActivityViewModel by lazy{ ViewModelProviders.of(activity!!).get(MainActivityViewModel::class.java)}

    @SuppressLint("InflateParams")
    private fun tabsInitialization() {
        val adapter = ViewPagerAdapter(childFragmentManager)
        viewPager.offscreenPageLimit = 3
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.getTabAt(0)!!.customView = LayoutInflater.from(activity!!.applicationContext).inflate(name.report.report.R.layout.custom_tab, null)
        tabLayout.getTabAt(1)!!.customView = LayoutInflater.from(activity!!.applicationContext).inflate(name.report.report.R.layout.custom_tab, null)
        tabLayout.getTabAt(2)!!.customView = LayoutInflater.from(activity!!.applicationContext).inflate(name.report.report.R.layout.custom_tab, null)
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                updateTabs(tab.position == 0, tab.position == 1, tab.position == 2)
                updateTabs(tab.position)
                if((activity!! as MainActivity).appPreferences.guideShown())
                    if(!(activity!! as MainActivity).appPreferences.tabClicked(tab.position)) {
                        showTips(tab.position)
                        (activity!! as MainActivity).appPreferences.setTabClicked(tab.position)
                    }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                updateTabs(tab.position == 0, tab.position == 1, tab.position == 2)
                updateTabs(tab.position)
            }
        })
        tabLayout.getTabAt(0)!!.customView!!.image.setImageDrawable(resources.getDrawable(tabIcons[3]))
        tabLayout.getTabAt(1)!!.customView!!.image.setImageDrawable(resources.getDrawable(tabIcons[1]))
        tabLayout.getTabAt(2)!!.customView!!.image.setImageDrawable(resources.getDrawable(tabIcons[5]))
    }

    private fun showTips(position: Int){
        val view = activity!!.layoutInflater.inflate(R.layout.extra_layout, null)
        view.tipText.text = when(position){
            0 -> getString(R.string.tip_1)
            1 -> getString(R.string.tip_2)
            2 -> getString(R.string.tip_3)
            else -> ""
        }
        EasyDialog(context)
                .setLayout(view)
                .setBackgroundColor(Color.parseColor("#55000000"))
                .setLocationByAttachedView(tabLayout.getTabAt(position)!!.customView)
                .setGravity(EasyDialog.GRAVITY_TOP)
                .setAnimationTranslationShow(EasyDialog.DIRECTION_X, 1000, -600f, 100f, -50f, 50f, 0f)
                .setAnimationAlphaShow(1000, 0.3f, 1.0f)
                .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, -50f, 800f)
                .setAnimationAlphaDismiss(500, 1.0f, 0.0f)
                .setTouchOutsideDismiss(true)
                .setMatchParent(true)
                .setMarginLeftAndRight(24, 24)
                .show()
    }

    private fun updateTabs(position: Int){
        if(position == 1)
            curUserViewModel.useCase.execute()
                    .subscribeBy(
                            onSuccess = {
                                curUserViewModel.curUserRequests.value = it
                                curUserViewModel.startProgressBar.value = false
                            },
                            onError = {
                                curUserViewModel.startProgressBar.value = true
                            }
                    )
        if(position == 2) {
            mainActivityViewModel.getDialogsToYouUseCase.execute()
                    .subscribeBy(
                            onSuccess = {
                                val list = ArrayList<ReportRequest>()
                                it.forEach{
                                    if(it.request!=null)
                                        list.add(it.request)
                                }
                                mainActivityViewModel.requestsToUser.value = list
                            },
                            onError = {}
                    )
        }


    }

    private fun updateTabs(first: Boolean, second: Boolean, third: Boolean) {
        setTabs(tabLayout.getTabAt(0)!!.customView!!.image, 0, first)
        setTabs(tabLayout.getTabAt(1)!!.customView!!.image, 1, second)
        setTabs(tabLayout.getTabAt(2)!!.customView!!.image, 2, third)
    }

    private fun setTabs(iv: ImageView, image: Int, isVisible: Boolean) {
        if (isVisible)
            iv.setImageDrawable(resources.getDrawable(tabIcons[image]))
        else
            iv.setImageDrawable(resources.getDrawable(tabIcons[image + 3]))
    }
}

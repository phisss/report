package name.report.report.presentation.addingsheet.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.location.Geocoder
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.*
import name.report.report.MainActivity
import name.report.report.R
import name.report.report.domain.Coordinate
import name.report.report.domain.TransmittedReportRequest
import name.report.report.presentation.addingsheet.viewmodel.AddRequestViewModel
import name.report.report.presentation.main.viewmodel.MainActivityViewModel
import timber.log.Timber
import android.widget.Toast
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.adding_fragment.*
import name.report.report.domain.Polygon
import java.io.IOException
import java.util.*


/**
* Created by Hero_in_heaven on 29.11.2017.
*/

class AddRequestFragment : Fragment() {

    private var pos = Coordinate(0.0, 0.0)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(name.report.report.R.layout.adding_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = ViewModelProviders.of(activity!!).get(MainActivityViewModel::class.java)
        val addViewModel = ViewModelProviders.of(this).get(AddRequestViewModel::class.java)

        etRequestText.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                activity!!.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
            } else {
                activity!!.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
            }
        }
        switcher.setOnCheckedChangeListener{ _, isChecked ->
            viewModel.isWidden.value = isChecked
        }
        addFriendsRL.setOnClickListener{
            Snackbar.make(activity!!.currentFocus, getString(R.string.we_will_add), Snackbar.LENGTH_SHORT).show()
        }
        viewModel.sendRequestLiveData.observe(this, Observer {
            if (etRequestText.text.toString() == "" ) {
                Toast.makeText(context, R.string.add_empty_fields_error, Toast.LENGTH_SHORT).show()
            }else {
                viewModel.markerPosLiveData.observe(this, Observer {
                    pos = Coordinate(it!!.position.latitude, it.position.longitude)
                })

                val list = ArrayList<Coordinate>()
                val dist: Float = if(switcher.isChecked){
                    500
                }else{
                    50
                }.toFloat()

                val add: Float = 180f/Math.PI.toFloat() * dist/6378137
                val addcos: Float = add/Math.cos(pos.lat).toFloat()
                list.add(Coordinate(pos.lng + addcos, pos.lat + add))
                list.add(Coordinate(pos.lng - addcos, pos.lat + add))
                list.add(Coordinate(pos.lng - addcos, pos.lat - add))
                list.add(Coordinate(pos.lng + addcos, pos.lat - add))
                list.add(list[0])

                addViewModel.clickOnAddRequestFab(TransmittedReportRequest(
                        Polygon(list),
                        etRequestText.text.toString(),
                        getAddress(LatLng(pos.lat, pos.lng)).toString(),
                        100,
                        " "))
                (activity!! as MainActivity).resetUI()
            }
        })



        addViewModel.addRequestLiveData.observe(this, Observer {
            (activity!! as MainActivity).hideKeyBoard()
            if (it!!) {
                Timber.e("RE:: Request Adding: Request added!")
            } else
                Timber.e("RE:: Request Adding: Error while adding request")
        })
    }

    private fun getAddress(location: LatLng): String? {
        return try {
            val addresses = Geocoder(context, Locale.getDefault()).getFromLocation(location.latitude, location.longitude, 1)
            addresses[0].getAddressLine(0)
        } catch (e: IOException) {
            e.printStackTrace()
            "Unknown address"
        }

    }
}

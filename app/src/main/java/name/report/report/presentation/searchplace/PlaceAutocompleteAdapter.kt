package name.report.report.presentation.searchplace

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.text.style.StyleSpan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.RelativeLayout
import android.widget.TextView


import name.report.report.R
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.model.LatLngBounds

import java.util.ArrayList
import java.util.concurrent.TimeUnit

class PlaceAutocompleteAdapter(private var mContext: Context, private val layout: Int, private val mGoogleApiClient: GoogleApiClient,
                               public var mBounds: LatLngBounds?, private val mPlaceFilter: AutocompleteFilter?) : RecyclerView.Adapter<PlaceAutocompleteAdapter.PlaceViewHolder>(), Filterable {
    private var mListener: PlaceAutoCompleteInterface
    internal var mResultList: ArrayList<PlaceAutocomplete>? = null

    interface PlaceAutoCompleteInterface {
        fun onPlaceClick(mResultList: ArrayList<PlaceAutocomplete>?, position: Int)
    }

    init {
        this.mListener = mContext as PlaceAutoCompleteInterface
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val results = FilterResults()
                // Skip the autocomplete query if no constraints are given.
                if (constraint != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    mResultList = getAutocomplete(constraint)
                    if (mResultList != null) {
                        // The API successfully returned results.
                        results.values = mResultList
                        results.count = mResultList!!.size
                    }
                }
                return results
            }

            override fun publishResults(constraint: CharSequence, results: FilterResults?) {
                if (results != null && results.count > 0) {
                    // The API returned at least one result, update the data.
                    notifyDataSetChanged()
                } else {
                    // The API did not return any results, invalidate the data set.
                    //notifyDataSetInvalidated();
                }
            }
        }
    }

    private fun getAutocomplete(constraint: CharSequence?): ArrayList<PlaceAutocomplete>? {
        if (mGoogleApiClient.isConnected) {
            val results = Places.GeoDataApi
                    .getAutocompletePredictions(mGoogleApiClient, constraint!!.toString(),
                            mBounds, mPlaceFilter)

            val autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS)

            val status = autocompletePredictions.status
            if (!status.isSuccess) {
                autocompletePredictions.release()
                return null
            }

            val iterator = autocompletePredictions.iterator()
            val resultList = ArrayList<PlaceAutocomplete>(autocompletePredictions.count)
            while (iterator.hasNext()) {
                val prediction = iterator.next()
                resultList.add(PlaceAutocomplete(prediction.placeId.toString(),
                        prediction.getPrimaryText(STYLE_BOLD),
                        prediction.getSecondaryText(STYLE_BOLD)))

            }

            autocompletePredictions.release()

            return resultList
        }
        return null
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): PlaceViewHolder {
        val layoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val convertView = layoutInflater.inflate(layout, viewGroup, false)
        return PlaceViewHolder(convertView)
    }


    override fun onBindViewHolder(mPredictionHolder: PlaceViewHolder, i: Int) {

        mPredictionHolder.mAddress.text = mResultList!![i].description

        mPredictionHolder.mAddress2.text = mResultList!![i].description2

        mPredictionHolder.mParentLayout.setOnClickListener { mListener.onPlaceClick(mResultList, i) }

    }

    override fun getItemCount(): Int {
        return if (mResultList != null)
            mResultList!!.size
        else
            0
    }

    inner class PlaceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mParentLayout: RelativeLayout = itemView.findViewById(name.report.report.R.id.predictedRow)
        var mAddress: TextView = itemView.findViewById(name.report.report.R.id.address)
        var mAddress2: TextView = itemView.findViewById(name.report.report.R.id.address2)
    }

    /**
     * Holder for Places Geo Data Autocomplete API results.
     */
    inner class PlaceAutocomplete internal constructor(var placeId: CharSequence, var description: CharSequence, internal var description2: CharSequence) {

        override fun toString(): String {
            return description.toString()
        }
    }

    companion object {
        private val TAG = "PlaceAutocompleteAdapter"
        private val STYLE_BOLD = StyleSpan(Typeface.BOLD)
    }
}

package name.report.report.presentation.search

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import name.report.report.R
import name.report.report.SearchViewPagerAdapter
import kotlinx.android.synthetic.main.custom_tab.view.*
import kotlinx.android.synthetic.main.fragment_search.*

@Suppress("DEPRECATION")
class SearchFragment : Fragment() {


    companion object {
        val instance
            get() = SearchFragment()
    }
    private val tabIcons = intArrayOf(
            name.report.report.R.drawable.search_place_red,
            name.report.report.R.drawable.ic_feed,
            name.report.report.R.drawable.search_place_gray,
            name.report.report.R.drawable.ic_feed_g
            )

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
            = inflater.inflate(name.report.report.R.layout.fragment_search, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Tab init
        tabsInitialization()
        updateTabs(true, false)

    }

    @SuppressLint("InflateParams")
    private fun tabsInitialization() {
        val adapter = SearchViewPagerAdapter(childFragmentManager)
        searchViewPager.offscreenPageLimit = 2
        searchViewPager.adapter = adapter
        searchTabLayout.setupWithViewPager(searchViewPager)
        searchTabLayout.getTabAt(0)!!.customView = LayoutInflater.from(activity!!.applicationContext).inflate(name.report.report.R.layout.custom_tab, null)
        searchTabLayout.getTabAt(1)!!.customView = LayoutInflater.from(activity!!.applicationContext).inflate(name.report.report.R.layout.custom_tab, null)
        searchTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                updateTabs(tab.position == 0, tab.position == 1)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                if(tab.position == 0)
                    Toast.makeText(context, getString(R.string.we_will_add), Toast.LENGTH_SHORT).show()
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
    }



    private fun updateTabs(first: Boolean, second: Boolean) {
        setTabs(searchTabLayout.getTabAt(0)!!.customView!!.image, 0, first)
        setTabs(searchTabLayout.getTabAt(1)!!.customView!!.image, 1, second)
    }

    private fun setTabs(iv: ImageView, image: Int, isVisible: Boolean) {
        if (isVisible)
            iv.setImageDrawable(resources.getDrawable(tabIcons[image]))
        else
            iv.setImageDrawable(resources.getDrawable(tabIcons[image + 2]))
    }
}

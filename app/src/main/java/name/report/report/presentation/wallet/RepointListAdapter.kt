package name.report.report.presentation.wallet

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import java.util.ArrayList

import de.hdodenhof.circleimageview.CircleImageView
import name.report.report.App
import name.report.report.GlideApp
import name.report.report.R
import name.report.report.domain.RepointtoList
import timber.log.Timber

class RepointListAdapter(repointList: ArrayList<RepointtoList>) : RecyclerView.Adapter<RepointListAdapter.RepointListHolder>() {
    private val repoints: List<RepointtoList>

    init {
        this.repoints = repointList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepointListHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.wallet_user_repoint_item, parent, false)
        return RepointListHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RepointListHolder, position: Int) {

        if(repoints[position].userName=="null null") {
            holder.personName.text = "Unlogged user"
            holder.imageView.setImageDrawable(App.context.getDrawable(R.drawable.anon_photo))
        }else {
            holder.personName.text = repoints[position].userName
            GlideApp.with(App.context)
                    .load(repoints[position].userPhoto)
                    .into(holder.imageView)
        }
        holder.repoints.text = "+" + repoints[position].userRepoints + " rep"
    }

    override fun getItemCount(): Int {
        return repoints.size
    }

    class RepointListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: CircleImageView = itemView.findViewById(R.id.userPhoto)
        var personName: TextView = itemView.findViewById(R.id.userName)
        var repoints: TextView = itemView.findViewById(R.id.userRep)
    }

}
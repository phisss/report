package name.report.report.presentation.map.view

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.IntentSender
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.location.Location
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager

import name.report.report.domain.*
import name.report.report.domain.Polygon
import name.report.report.presentation.main.viewmodel.MainActivityViewModel
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.maps.android.MarkerManager
import kotlinx.android.synthetic.main.slider_main.*
import name.report.report.utils.*
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.google.maps.android.ui.IconGenerator
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.alert_open_from_map.view.*
import kotlinx.android.synthetic.main.alert_photo.view.*
import kotlinx.android.synthetic.main.custom_infowindow.view.*
import kotlinx.android.synthetic.main.fragment_tabs.*
import kotlinx.android.synthetic.main.main_content.view.*
import name.report.report.*
import name.report.report.R
import name.report.report.chat.DefaultMessagesActivity
import name.report.report.data.cache.AppPreferences
import name.sothree.slidinguppanel.SlidingUpPanelLayout
import org.aaronhe.rxgooglemapsbinding.RxGoogleMaps
import rx.subscriptions.Subscriptions
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.HashMap

@Suppress("DEPRECATION", "UNUSED_EXPRESSION", "NAME_SHADOWING")

class MapFragment : Fragment(),
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    //надо будет написать усключения
    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnectionSuspended(p0: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnected(p0: Bundle?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    @Inject
    lateinit var appPreferences: AppPreferences

    private val markerClickListener = OnMarkerClickListenerCollection()
    private var radius = 50
    private var googleMap: GoogleMap? = null
    private lateinit var centerPos: LatLng
    private lateinit var mClusterManager: ClusterManager<MyItem>
    private var isAnchored = false
    private var mPreviousCameraPosition: CameraPosition? = null
    private var googleApiClient: GoogleApiClient? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var marker: Marker? = null
    private var circle: Circle? = null
    private var isPreviousExpanded = true
    private var notMovedFromUserRequest = true
    private var pinsHashMap = HashMap<String, RequestToAlert>()
    private var usersHashMap = HashMap<String, Marker>()
    private val clusterItemsHash = HashMap<String, MyItem>()
    private val requestHashMap = HashMap<String, ReportRequest>()
    private val subscriber = Subscriptions.from()

    fun getCenterPos() = centerPos
    fun getZoom() = googleMap?.cameraPosition?.zoom
    fun getMarker() = marker
    fun getCircle() = circle
    fun getPP() = isPreviousExpanded
    fun setPP(flag: Boolean){ isPreviousExpanded = flag }

    private lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("MissingPermission")
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        googleApiClient = GoogleApiClient.Builder(activity!!.applicationContext!!)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!.applicationContext)
        try {
            MapsInitializer.initialize(activity!!.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        settingsRequest()
        return inflater.inflate(name.report.report.R.layout.slider_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(MainActivityViewModel::class.java)
        map.onCreate(savedInstanceState)
        map.onResume() // needed to get the map to display immediately
        map.getMapAsync(this)
    }

    override fun onMapReady(p0: GoogleMap?){
        googleMap = p0

        //ON CAMERA MOVE
        RxGoogleMaps.cameraPositionChanges(googleMap!!)
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe{ onMapMoved() }

        //ON MAP CLICK
        RxGoogleMaps.longClicks(googleMap!!)
                .subscribe{ mapLongClick(it) }

        //ON POI CLICK
        googleMap?.setOnPoiClickListener({ clickOnPoi(it) })

        //CLUSTER INITIALIZATION
        clusterInitialization()

        //ON MARKER CLICK
        initOnMarkerClickListener()

        //MAP UI
        setMapUI()

        viewModel.posToMove.observe(this, Observer {
            if(!appPreferences.guideShown()) {
                marker = googleMap?.addMarker(MarkerOptions()
                        .alpha(0.97f)
                        .position(it!!)
                        .icon(ContextCompat.getDrawable(context!!, R.drawable.oval_people_ic)?.toBitmapDescriptor()))!!
                zoomToCoordinates(17f, LatLng(it!!.latitude+0.0004, it.longitude), 1000)
            }else
                zoomToCoordinates(17f, it!!, 1000)
        })

        viewModel.requestsInFeed.observe(this, Observer {
            setPinsOnMap(it!!)
        })

        viewModel.isWidden.observe(this, Observer {
            circle!!.radius = if(it!!)
                500.0
            else
                50.0
            radius = circle!!.radius.toInt()
        })

        viewModel.showInFragment.observe(this, Observer {
            if (it != null) {
                requestToUserBS(it)
                viewModel.showRequestFromChat.value = null
                viewModel.showInFragment.value = null
            }
        })

        zoomToMyCoordinates()
        addingRequestViewModels()
    }

    private fun getIdForMarker(m: Marker): String? {
        for (p in pinsHashMap)
            if (p.value.position == m.position)
                return p.key
        return "Error while getting id"
    }

    private fun addingRequestViewModels(){
        viewModel.addRequest.observe(this, Observer {
            if(it!!)
                mapLongClick(googleMap?.cameraPosition?.target!!)
        })
        viewModel.addRequestFromSearchList.observe(this, Observer {
            mapLongClick(it!!)
        })
    }

    private fun mapLongClick(position: LatLng){
        zoomToCoordinates(15f, LatLng(position.latitude-0.0009, position.longitude), 800)
        (activity as MainActivity).setUiToAdding()
        marker?.remove()
        circle?.remove()
        marker = googleMap?.addMarker(MarkerOptions()
                .position(position)
                .alpha(0.5f)
                .icon(ContextCompat.getDrawable(context!!, name.report.report.R.drawable.ic_pin)?.toBitmapDescriptor()))
        viewModel.markerPosLiveData.value = marker
        circle = googleMap?.addCircle(CircleOptions().center(position)
                .radius(radius.toDouble())
                .strokeWidth(3f)
                .strokeColor(resources.getColor(name.report.report.R.color.main_red))
                .fillColor(resources.getColor(name.report.report.R.color.main_red_alpha)))
    }

    private fun onMapMoved(){
        (activity!! as MainActivity).updateNavInfo()
        (activity as MainActivity).updateRepInfo()
        if (mPreviousCameraPosition === null || mPreviousCameraPosition !== googleMap?.cameraPosition) {
            mPreviousCameraPosition = googleMap?.cameraPosition
            setMapMoving(isAnchored)
            val topPos = LatLng(googleMap?.projection!!.fromScreenLocation(android.graphics.Point(resources.getWidth(), 0)).latitude,
                    googleMap?.projection!!.fromScreenLocation(android.graphics.Point(resources.getWidth(), 0)).longitude)
            val centerPos = LatLng(googleMap?.projection!!.fromScreenLocation(android.graphics.Point(resources.getWidth(), resources.getHeight())).latitude,
                    googleMap?.projection!!.fromScreenLocation(android.graphics.Point(resources.getWidth(), resources.getHeight())).longitude)

            val loc1 = Location("")
            loc1.latitude = topPos.latitude
            loc1.longitude = topPos.longitude

            val loc2 = Location("")
            loc2.latitude = centerPos.latitude
            loc2.longitude = centerPos.longitude

            val radius = loc1.distanceTo(loc2).toLong()
            viewModel.radius.value = radius
            if(notMovedFromUserRequest)
                viewModel.useCase.execute(centerPos.longitude, centerPos.latitude, radius).subscribeBy(
                        onSuccess = {
                            setUsersOnMap(it)
                        },
                        onError = {
                            it.printStackTrace()
                        }
                )
            else{
                notMovedFromUserRequest = true
            }
            if(!viewModel.sliderMoved.value!!) {
                viewModel.runProgressBar.value = true
                viewModel.requests.getRequestsInRadiusRemoteUseCase.execute(centerPos.longitude, centerPos.latitude, radius)
                        .subscribeBy(
                                onSuccess = {
                                    viewModel.requestsInFeed.value = it

                                },
                                onError = {
                                    viewModel.runProgressBar.value = false
                                    Snackbar.make(map, "Check your INTERNET connection", Snackbar.LENGTH_SHORT).show()
                                }
                        )
            }
            mClusterManager.cluster()
            viewModel.movingFromAlert.value = false
            viewModel.sliderMoved.value = false
            (activity as MainActivity).mAdapter!!.mBounds = LatLngBounds(
                    LatLng(googleMap?.cameraPosition?.target!!.latitude-1,
                            googleMap?.cameraPosition?.target!!.longitude-1),
                    LatLng(googleMap?.cameraPosition?.target!!.latitude+1,
                            googleMap?.cameraPosition?.target!!.longitude+1))
            viewModel.adapter.value = (activity as MainActivity).mAdapter
        }

    }

    @SuppressLint("SimpleDateFormat")
    private fun requestToUserBS(userId: String){
        if(userId=="user") {
            requestToUserInTutorial()
        }else{
            viewModel.getTokensUseCase.execute().subscribeBy(
                    onSuccess = {
                        if(it.type==TokenType.DEMO){
                            (activity!! as  MainActivity).showRegisterAlert()
                        }else{
                            (activity!! as MainActivity).resetUI()
                            (activity!! as MainActivity).getCloseFabAdding().visible()
                            (activity!! as MainActivity).getBottomSheetBehavior2().state = BottomSheetBehavior.STATE_EXPANDED
                            (activity!! as MainActivity).setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED)
                            (activity!! as MainActivity).getSend().setOnClickListener {
                                (activity!! as MainActivity).hideKeyBoard()
                                (activity!! as MainActivity).getCloseFabAdding().gone()
                                (activity!! as MainActivity).getBottomSheetBehavior2().state = BottomSheetBehavior.STATE_COLLAPSED
                                viewModel.createRequestToUserUseCase.execute(TransmittedReportRequest(
                                        Point(Coordinate(0.0, 0.0)),
                                        (activity!! as MainActivity).getET().text.toString() + " ",
                                        "Request to user",
                                        100,
                                        (activity!! as MainActivity).getHorizontalPicker().value.toString()
                                ), userId)
                                        .subscribeBy(
                                                onSuccess = {
                                                    Snackbar.make(activity!!.currentFocus, "Chat created", Snackbar.LENGTH_LONG).show()
                                                },
                                                onError = {}
                                        )
                            }
                        }
                    },
                    onError = {}
            )
        }
    }

    private fun requestToUserInTutorial(){
        (activity!! as MainActivity).getGuide().bottomClouds.gone()
        (activity!! as MainActivity).getBottomSheetBehavior2().state = BottomSheetBehavior.STATE_EXPANDED
        (activity!! as MainActivity).hideTextClick()
        (activity!! as MainActivity).viewPager.setCurrentItem(1, false)
        (activity!! as MainActivity).getSend().setOnClickListener {
            (activity!! as MainActivity).getBottomSheetBehavior2().state = BottomSheetBehavior.STATE_COLLAPSED
            (activity!! as MainActivity).getGuide().topClouds.visible()
            (activity!! as MainActivity).getGuide().stepDescription.visible()
            (activity!! as MainActivity).getGuide().stepLabel.visible()
            (activity!! as MainActivity).getGuide().bottomClouds.gone()
            (activity!! as MainActivity).getGuide().stepLabel.text = getString(R.string.first)
            (activity!! as MainActivity).getGuide().stepDescription.text = getString(R.string.wow)
            marker?.remove()
            viewModel.firstReport.value = listOf(ReportRequest("user",
                    (activity!! as MainActivity).getET().text.toString() + " ",
                    (activity!! as MainActivity).getET().text.toString()+ " ",
                    null,
                    "user",
                    "",
                    true,
                    true,
                    1000,
                    SimpleDateFormat("HH:mm a").format(Calendar.getInstance(TimeZone.getDefault()).time),
                    null,
                    "",
                    ReportUser(""),
                    Point(Coordinate(0.0, 0.0))))
            (activity as MainActivity).setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED)
        }
    }

    private fun initOnMarkerClickListener(){
        val clickListener = GoogleMap.OnMarkerClickListener { marker ->
            when(marker.alpha){
                0.97f -> requestToUserBS("user")
                0.98f -> createAlertFromMap(pinsHashMap[getIdForMarker(marker)]!!)
                0.99f -> {
                    usersHashMap.forEach{
                        if(it.value == marker) {
                            requestToUserBS(it.key)
                            notMovedFromUserRequest = true
                            zoomToCoordinates(16f,
                                    LatLng(marker.position.latitude-0.0009, marker.position.longitude),
                                    800)
                        }
                    }
                }
            }
            true
        }
        mClusterManager.setOnClusterClickListener {
            val builder = LatLngBounds.builder()
            it.items.forEach {
                builder.include(it.position)
            }
            val bounds = builder.build();
            googleMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
            true
        }
        markerClickListener.registerListener(clickListener)
        markerClickListener.registerListener(mClusterManager)
        googleMap?.setOnMarkerClickListener(markerClickListener)
    }

    private fun setUsersOnMap(users: List<UserPosition>) {
        users.forEach {
            if(it.user!=null) {
                val user = it.user!!
                val userId = user.userId
                val userCoordinate = (it.currentPosition as Point).coordinate
                if (user.userId != viewModel.getCurrUserUseCase.execute()!!.userId) {
                    val icon = if (userId == "vk_179667459" || userId == "vk_104732776" || userId == "vk_101332283" || userId == "fb_884165718423540" || userId == "vk_91340492" || userId == "vk_190519990" || userId == "fb_173815483215716" || userId == "fb_10155674980409457")
                        BitmapDrawable(resources, Bitmap.createScaledBitmap((resources.getDrawable(R.drawable.admin) as BitmapDrawable).bitmap, 30, 48, true)).toBitmapDescriptor()
                    else
                        ContextCompat.getDrawable(context!!, name.report.report.R.drawable.oval_people_ic)?.toBitmapDescriptor()

                    if (usersHashMap.containsKey(userId)) {
                        val itPos = LatLng(userCoordinate.lat, userCoordinate.lng)
                        if (usersHashMap.getValue(userId).position != itPos)
                            usersHashMap.getValue(userId).position = itPos
                        if(googleMap?.cameraPosition?.zoom!! < 10f) {
                            usersHashMap.getValue(userId).remove()
                            usersHashMap.remove(userId)
                        }
                    } else {
                        if(googleMap?.cameraPosition?.zoom!! > 10f)
                            usersHashMap.put(userId, googleMap?.addMarker(MarkerOptions()
                                    .alpha(0.99f)
                                    .position(LatLng(
                                            userCoordinate.lat,
                                            userCoordinate.lng))
                                    .icon(icon))!!)
                    }
                }
            }
        }
    }

    private fun setPinsOnMap(requestList: List<ReportRequest>){
        requestList.forEach {
            val pos = it.position
            val photoUrl = if(it.user!=null)
                it.user!!.photo.toString()
            else
                "null"
            val position: LatLng
            position = when (pos) {
                is Point -> LatLng(pos.coordinate.lat, pos.coordinate.lng)
                is Polygon -> LatLng((pos.coordinates[0].lat+pos.coordinates[2].lat)/2, (pos.coordinates[0].lng+pos.coordinates[2].lng)/2)
                else -> LatLng(0.0, 0.0)
            }
            if(!requestHashMap.containsKey(it._id)) {
                val myItem = MyItem(position.latitude, position.longitude, it.message!!)
                mClusterManager.addItem(myItem)
                clusterItemsHash.put(it._id, myItem)
                requestHashMap.put(it._id, it)
                pinsHashMap.put(it._id, RequestToAlert(
                        position,
                        it.message.toString(),
                        it.description.toString(),
                        photoUrl,
                        it._id,
                        it.place.toString(),
                        it.time,
                        it.userId,
                        it.answered
                ))
            }
        }
        requestHashMap.forEach{
            Timber.e("RE:: ${it.value.message}")
        }
        mClusterManager.cluster()
    }

    @SuppressLint("MissingPermission")
    private fun setMapUI(){
        googleMap?.uiSettings?.isMyLocationButtonEnabled = false
        googleMap?.uiSettings?.isCompassEnabled = false
        googleMap?.uiSettings?.isMapToolbarEnabled = false
        googleMap?.uiSettings?.isCompassEnabled = false
    }

    fun setMapMoving(flag: Boolean){
        isAnchored = flag
        centerPos = if(isAnchored)
            getScreenCenterPos(googleMap?.projection!!, false)
        else
            getScreenCenterPos(googleMap?.projection!!, true)
    }

    fun zoomToCoordinates(zoom: Float, position: LatLng, time: Int) {
        val builder = CameraPosition.Builder()
        builder.zoom(zoom)
        builder.target(position)
        googleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()), time, null)
    }

    @SuppressLint("MissingPermission")
    fun zoomToMyCoordinates() {
        try {
            googleMap?.isMyLocationEnabled = true
            if(appPreferences.guideShown()) {
                val locationProvider = LocationServices.getFusedLocationProviderClient(context!!)
                locationProvider.lastLocation.addOnSuccessListener {
                    mFusedLocationClient?.lastLocation?.addOnSuccessListener {
                        zoomToCoordinates(15f, LatLng(it.latitude, it.longitude), 800)
                    }
                }
            }
        }catch (e: Exception){e.printStackTrace()}

    }

    private fun getScreenCenterPos(projection: Projection, flag: Boolean): LatLng {
        val markerPosBottom  = if(flag)
            android.graphics.Point(resources.getWidth(), resources.getHeight()/2 - 56.toDp(resources)/2)
        else
            android.graphics.Point(resources.getWidth(), resources.getHeight() + resources.getHeight()/2 - 56.toDp(resources)/2)
        return LatLng(projection.fromScreenLocation(markerPosBottom).latitude, projection.fromScreenLocation(markerPosBottom).longitude)
    }

    private fun settingsRequest() {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = (30 * 1000).toLong()
        locationRequest.fastestInterval = (5 * 1000).toLong()
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)
        val result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                    status.startResolutionForResult(activity!!, 2)
                } catch (e: IntentSender.SendIntentException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        map?.onResume()
    }

    override fun onPause() {
        super.onPause()
        map?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        map?.onDestroy()
        subscriber.unsubscribe()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map?.onLowMemory()
    }

    private fun clusterInitialization(){
        try {
            mClusterManager = ClusterManager(context!!, googleMap)
            mClusterManager.renderer = OwnIconRendered(context!!, googleMap!!, mClusterManager)
            mClusterManager.cluster()
        } catch (e: Exception) {
            e.printStackTrace()

        }
    }

    private inner class OwnIconRendered internal constructor(context: Context, map: GoogleMap, clusterManager: ClusterManager<MyItem>) : DefaultClusterRenderer<MyItem>(context, map, clusterManager) {
        override fun onBeforeClusterItemRendered(myItem: MyItem?, markerOptions: MarkerOptions?) {
            super.onBeforeClusterItemRendered(myItem, markerOptions)

            val view = layoutInflater.inflate(R.layout.custom_infowindow, null)
            view.title.text = myItem!!.title

            val generator = IconGenerator(context)
            generator.setContentView(view)
            generator.setBackground(resources.getDrawable(R.drawable.infowindow_bg))
            val icon = generator.makeIcon()

            markerOptions!!
                    .icon(BitmapDescriptorFactory.fromBitmap(icon))
                    .title(myItem.title)
                    .alpha(0.98f)
        }

        override fun shouldRenderAsCluster(cluster: Cluster<MyItem>?) = cluster?.size!! > 1

        override fun getColor(clusterSize: Int) = Color.parseColor("#ff2d55")
    }

    private fun clickOnPoi(poi: PointOfInterest){
        Places.GeoDataApi.getPlaceById((activity!! as MainActivity).mGoogleApiClient, poi.placeId).setResultCallback( {
            if (it.status.isSuccess && it.count > 0) {
                it.forEach{
                    AlertDialog.Builder(context!!)
                            .setTitle(poi.name)
                            .setMessage("Address: " + it.address.toString() +
                                    "\nAttributions: " + it.attributions +
                                    "\nPhone Number: " + it.phoneNumber +
                                    "\nPlaceTypes: " + it.placeTypes +
                                    "\nWebsite: " + it.websiteUri +
                                    "\nLocale: " + it.locale +
                                    "\nPrice Level: " + it.priceLevel +
                                    "\nRating: " + it.rating
                            )
                            .show()
                }
                it.release()
            }
        })

        Places.GeoDataApi.getPlacePhotos((activity!! as MainActivity).mGoogleApiClient, poi.placeId).setResultCallback {
            if (it.status.isSuccess) {
                it.photoMetadata.forEach{
                    photo ->
                    photo.getPhoto((activity!! as MainActivity).mGoogleApiClient).setResultCallback {
                        alertFromPlace(BitmapDrawable(resources, it.bitmap))
                    }
                }
                it .photoMetadata.release()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun createAlertFromMap(request: RequestToAlert){
        val view = layoutInflater.inflate(name.report.report.R.layout.alert_open_from_map, null)
        val adb = AlertDialog.Builder(activity!!, name.report.report.R.style.CustomAlertDialog)
        adb.setView(view)
        val alert = adb.create()
        alert!!.show()
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(alert.window!!.attributes)
        lp.width = resources.displayMetrics.widthPixels - 60.toDp(resources)
        alert.window!!.attributes = lp
        GlideApp.with(context)
                .load(request.photoUrl)
                .into(view.userPhotoInAlertFromMap)
        viewModel.getUserByIdUseCase.execute(request.userId)
                .subscribeBy(
                        onSuccess = {
                            view.userNameInAlertFromMap.text  = if(it.firstName == "null"){
                                view.userPhotoInAlertFromMap.setImageDrawable(resources.getDrawable(R.drawable.anon_photo))
                                "Unlogged user"
                            } else
                                it.firstName + " " + it.lastName
                        },
                        onError = {

                        }
                )
        view.requestTextInAlertFromMap.text = request.description
        view.cancelLL.setOnClickListener({
            alert.dismiss() })
        view.openLL.setOnClickListener({
            DefaultMessagesActivity.open(context!!,
                    request._id,
                    request.description,
                    request.userId)
            alert.dismiss() })
    }

    @SuppressLint("SetTextI18n")
    private fun alertFromPlace(photo: Drawable){
        val view = layoutInflater.inflate(name.report.report.R.layout.alert_photo, null)
        val adb = AlertDialog.Builder(activity!!, name.report.report.R.style.CustomAlertDialog)
        adb.setView(view)
        adb.setCancelable(true)
        val alert = adb.create()
        alert!!.show()
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(alert.window!!.attributes)
        lp.width = resources.displayMetrics.widthPixels - 60.toDp(resources)
        alert.window!!.attributes = lp
        view.placePhoto.setImageDrawable(photo)
    }
}
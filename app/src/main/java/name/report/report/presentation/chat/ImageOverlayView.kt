package name.report.report.presentation.chat

import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout

import name.report.report.Videoplayer

class ImageOverlayView : RelativeLayout {

    private var url: String? = null
    private var playImage: ImageView? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun startVideoActivity() {
        val sendIntent = Intent(context, Videoplayer::class.java)
        sendIntent.putExtra("url", url)
        context.startActivity(sendIntent)
    }

    fun setShareText(text: String) {
        this.url = text
    }

    fun setOverlayVisibility(visibility: Int) {
        this.playImage!!.visibility = visibility
    }

    private fun init() {
        val view = View.inflate(context, name.report.report.R.layout.overlay_view, this)
        playImage = view.findViewById(name.report.report.R.id.play)
        playImage!!.setOnClickListener { startVideoActivity() }
    }
}


package name.report.report.presentation.main.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import name.report.report.App
import name.report.report.domain.ReportRequest
import name.report.report.domain.RequestToAlert
import name.report.report.domain.usecases.auth.GetTokensUseCase
import name.report.report.domain.usecases.auth.GetUserAuthUseCase
import name.report.report.domain.usecases.auth.SetTokensUseCase
import name.report.report.domain.usecases.messages.*
import name.report.report.domain.usecases.position.GetUsersPositionsByRadiusUseCase
import name.report.report.domain.usecases.position.SetCurrentUserPositionUseCase
import name.report.report.domain.usecases.requests.*
import name.report.report.domain.usecases.users.GetCurrUserUseCase
import name.report.report.domain.usecases.users.UpdateAndGetCurrUserUseCase
import name.report.report.presentation.*
import name.report.report.presentation.feed.viewmodel.RequestLiveData
import name.report.report.presentation.searchplace.PlaceAutocompleteAdapter
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import name.report.report.chat.DefaultMessagesActivity
import name.report.report.domain.ReportDialog
import name.report.report.domain.usecases.users.GetUserByIdUseCase
import name.report.report.presentation.alert.view.BottomSheetFragmentInAlert
import javax.inject.Inject

/**
* Created by Hero_in_heaven on 30.11.2017.
*/

class MainActivityViewModel : ViewModel(){
    val markerPosLiveData = MutableLiveData<Marker>()
    val radius = MutableLiveData<Long>()
    val reportRequest = MutableLiveData<RequestToAlert>()
    val requestsInFeed = MutableLiveData<List<ReportRequest>>()
    val fromMap = MutableLiveData<Boolean>()
    val posToMove = MutableLiveData<LatLng>()
    val movingFromAlert = MutableLiveData<Boolean>()
    val sliderMoved = MutableLiveData<Boolean>()
    val searchCreated = MutableLiveData<Boolean>()
    val adapter = MutableLiveData<PlaceAutocompleteAdapter>()
    val runProgressBar = MutableLiveData<Boolean>()
    val logged = MutableLiveData<Boolean>()
    val isWidden = MutableLiveData<Boolean>()
    val firstReport = MutableLiveData<List<ReportRequest>>()
    val sendRequestLiveData = MutableLiveData<Boolean>()
    val requestsToUser = MutableLiveData<List<ReportRequest>>()
    val showRequestFromChat = MutableLiveData<String>()
    val showInFragment = MutableLiveData<String>()
    val addRequest = MutableLiveData<Boolean>()
    val addRequestFromSearchList = MutableLiveData<LatLng>()

    @Inject
    lateinit var useCase: GetUsersPositionsByRadiusUseCase

    @Inject
    lateinit var requests: RequestLiveData

    @Inject
    lateinit var setTokenUseCase: SetTokensUseCase

    @Inject
    lateinit var getTokensUseCase: GetTokensUseCase

    @Inject
    lateinit var getUserAuthUseCase: GetUserAuthUseCase

    @Inject
    lateinit var getMessageUseCase: GetRequestDialogUseCase

    @Inject
    lateinit var sendMessageUseCase: SendMessageUseCase

    @Inject
    lateinit var getEventsUseCase: GetEventsUseCase

    @Inject
    lateinit var getFollowDialogsUseCase : GetJoinerUserDialogsUseCase

    @Inject
    lateinit var getDialogsToYouUseCase : GetReporterUserDialogsUseCase

    @Inject
    lateinit var joinUseCase: JoinUseCase

    @Inject
    lateinit var leaveUseCase: LeaveUseCase

    @Inject
    lateinit var updateAndgetCurrUserUseCase : UpdateAndGetCurrUserUseCase

    @Inject
    lateinit var createRequestToUserUseCase : CreateRequestToUserUseCase

    @Inject
    lateinit var getCurrUserUseCase : GetCurrUserUseCase

    @Inject
    lateinit var getUserByIdUseCase : GetUserByIdUseCase

    init {
        App.appComponent.plusFeedComponent().inject(this)
    }

    fun onEvent(event: UiEvent) = when (event) {
        is Refresh -> TODO()
        is LongClickOnRequestItem -> TODO()
        is ClickOnCurRequestItem -> TODO()
        is ClickOnRequestItem -> {
            reportRequest.value = event.requestToAlert
            BottomSheetFragmentInAlert.sendRequest.value = reportRequest.value
            fromMap.value = false
        }
        is ClickOnIncomingRequest -> {
            DefaultMessagesActivity.open(event.requestToAlertIncoming.context,
                    event.requestToAlertIncoming._id,
                    event.requestToAlertIncoming.title,
                    event.requestToAlertIncoming.userId)
        }

    }


}
package name.report.report.presentation.feed.view

import name.report.report.domain.ReportRequest

class FeedMapper {

    fun map(requests: List<ReportRequest>, type: Int) = requests.map {
        when(type){
            0 -> RequestFeedUiModel(it.time, it.user?.photo, it.message, it._id, it.position, it.description, it.place, it.userId, it.answered)
            1 -> CurFeedUiModel(it.time, it.user?.photo, it.message, it._id, it.position, it.description, it.place, it.userId)
            2 ->  IncomingFeedUiModel(it.time, it.user?.photo, it.description, it._id, it.position, it.message, it.place, it.userId)
            else -> RequestFeedUiModel(it.time, it.user?.photo, it.message, it._id, it.position, it.description, it.place, it.userId, it.answered)
        }

    }
}

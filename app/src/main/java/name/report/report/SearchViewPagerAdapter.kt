package name.report.report

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.MotionEvent
import android.view.View
import name.report.report.presentation.feed.view.FeedFragment
import name.report.report.presentation.search.SearchByTopicFragment
import name.report.report.presentation.searchplace.SearchPlaceFragment
import java.util.ArrayList

@Suppress("DEPRECATION", "OverridingDeprecatedMember")
/**
* Created by Hero_in_heaven on 06.12.2017.
*/

class SearchViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val fragments = ArrayList<Fragment>()
    private val mFragmentTitleList = ArrayList<String>()
    private var currentFragment: Fragment? = null

    init {
        fragments.clear()
        fragments.add(SearchPlaceFragment())
        fragments.add(SearchByTopicFragment())
        mFragmentTitleList.add("SearchPlaces")
        mFragmentTitleList.add("SearchRequests")
    }

    override fun setPrimaryItem(container: View, position: Int, `object`: Any) {
        if (currentFragment !== `object`) {
            currentFragment = `object` as Fragment
        }
        super.setPrimaryItem(container, position, `object`)
    }

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

    override fun getPageTitle(position: Int): CharSequence? = mFragmentTitleList[position]


}
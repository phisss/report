package name.report.report.stores.output

import name.report.report.domain.Tokens

interface TokensCacheStoreOut {

    fun setTokens(tokens: Tokens)

    fun getTokens(): Tokens?

}

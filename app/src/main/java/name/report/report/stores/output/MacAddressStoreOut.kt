package name.report.report.stores.output

interface MacAddressStoreOut {

    fun macAddress(): String

}

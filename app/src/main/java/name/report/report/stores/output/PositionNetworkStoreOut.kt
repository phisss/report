package name.report.report.stores.output

import name.report.report.domain.Coordinate
import name.report.report.domain.UserPosition
import io.reactivex.Single

interface PositionNetworkStoreOut {

    fun usersPositionsByRadius(lng: Double, lat: Double, rad: Long): Single<List<UserPosition>>

    /**
     * Update id for user with id - userId, "" - for current user
     */
    fun userPosition(userId: String = ""): Single<UserPosition>

    /**
     * Set current user position
     *
     * @param coordinate - current user's position
     */
    fun setUserPosition(coordinate: Coordinate): Single<UserPosition>

}
package name.report.report.stores

import name.report.report.domain.ReportMessage
import name.report.report.domain.output.MessageStoreDomainOut
import name.report.report.domain.output.MessageStoreDomainOut.DialogType
import name.report.report.stores.output.MessagesNetworkStoreOut
import io.reactivex.Scheduler

class MessageStore(private val messagesNetworkStoreOut: MessagesNetworkStoreOut,
                   private val ioScheduler: Scheduler,
                   private val uiScheduler: Scheduler)
    : MessageStoreDomainOut {

    override fun sendMessage(requestId: String, message: ReportMessage)
            = messagesNetworkStoreOut.sendMessage(requestId, message)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    override fun requestDialog(requestId: String)
            = messagesNetworkStoreOut.requestDialog(requestId)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    override fun userDialogs(type: DialogType) = messagesNetworkStoreOut.userDialogs(type)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    override fun events() = messagesNetworkStoreOut.events().observeOn(uiScheduler)

    override fun hideEvents() = messagesNetworkStoreOut.hideEvents()

    override fun addRepoints(messageId: String, repoints: Int)
            = messagesNetworkStoreOut.addRepoints(messageId, repoints)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!


}

package name.report.report.stores.output

import name.report.report.domain.UserAuth

interface UserAuthCacheStoreOut {

    fun setUser(user: UserAuth)

    fun getUser(): UserAuth?

}

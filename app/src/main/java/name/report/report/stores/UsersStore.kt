package name.report.report.stores

import name.report.report.domain.output.UsersStoreDomainOut
import name.report.report.stores.output.ProfileCacheStoreOut
import name.report.report.stores.output.UsersNetworkStoreOut
import io.reactivex.Scheduler

class UsersStore(private val usersNetworkStoreOut: UsersNetworkStoreOut,
                 private val cacheStoreOut: ProfileCacheStoreOut,
                 private val ioScheduler: Scheduler,
                 private val uiScheduler: Scheduler) : UsersStoreDomainOut {

    override fun getCurrUser() = cacheStoreOut.getCurrUser()

    override fun updateAndGetCurrUser() = usersNetworkStoreOut.getCurrUser()
            .doOnSuccess(cacheStoreOut::setCurrUser)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    override fun getUser(userId: String) = usersNetworkStoreOut.getUser(userId)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)

}

package name.report.report.stores.output

import name.report.report.domain.ReportUser
import io.reactivex.Single

interface UsersNetworkStoreOut {

    fun getCurrUser(): Single<ReportUser>

    fun getUser(userId: String): Single<ReportUser>

}
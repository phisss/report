package name.report.report.stores

import name.report.report.domain.output.AndroidResourcesDomainOut
import name.report.report.stores.output.MacAddressStoreOut

class AndroidStore(private val macAddressStoreOut: MacAddressStoreOut) : AndroidResourcesDomainOut {

    override fun macAddress() = macAddressStoreOut.macAddress()

}

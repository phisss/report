package name.report.report.stores.output

import name.report.report.domain.MessageEvent
import name.report.report.domain.ReportDialog
import name.report.report.domain.ReportMessage
import name.report.report.domain.output.MessageStoreDomainOut.DialogType
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response

interface MessagesNetworkStoreOut {

    fun sendMessage(requestId: String, message: ReportMessage): Single<String>

    fun requestDialog(requestId: String): Single<ReportDialog>

    fun events(): Observable<MessageEvent>

    fun hideEvents()

    fun userDialogs(type: DialogType): Single<List<ReportDialog>>

    fun addRepoints(messageId: String, repoints: Int): Single<Response<Void>>

}

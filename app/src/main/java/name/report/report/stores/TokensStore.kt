package name.report.report.stores

import name.report.report.domain.Tokens
import name.report.report.domain.UserAuth
import name.report.report.domain.output.OuthDomainOut
import name.report.report.stores.output.ProfileCacheStoreOut
import name.report.report.stores.output.AuthNetworkStoreOut
import io.reactivex.Scheduler

class TokensStore(private val authNetwork: AuthNetworkStoreOut,
                  private val cacheStoreOut: ProfileCacheStoreOut,
                  private val uiScheduler: Scheduler,
                  private val ioScheduler: Scheduler) : OuthDomainOut {

    override fun updateFcm(token: String) = authNetwork.updateFcmToken(token)
            .map { Any() }
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    override fun setFcm(token: String) = cacheStoreOut.setFcm(token)

    override fun getFcm() = cacheStoreOut.getFcm()

    fun update(user: UserAuth)
            = authNetwork.getTokens(user.userId, user.password)
            .doOnSuccess(cacheStoreOut::setTokens)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    fun get() = cacheStoreOut.getTokens()


    //OuthDomainOut

    override fun isAuthorized() = get() != null

    override fun getTokens() = get()

    override fun updateTokensDemo(user: UserAuth) = update(user)

    override fun setTokens(tokens: Tokens) = cacheStoreOut.setTokens(tokens)

    override fun setAuth(user: UserAuth) = cacheStoreOut.setAuth(user)

    override fun getAuth() = cacheStoreOut.getAuth()

    override fun getProfilePhotoUrl() = cacheStoreOut.getUrl()

    override fun setProfilePhotoUrl(url: String) = cacheStoreOut.setUrl(url)

    override fun setUserName(name: String) = cacheStoreOut.setName(name)

    override fun getUserName() = cacheStoreOut.getName()

}

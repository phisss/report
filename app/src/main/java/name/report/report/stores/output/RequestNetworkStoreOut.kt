package name.report.report.stores.output

import name.report.report.domain.ReportRequest
import name.report.report.domain.TransmittedReportRequest
import io.reactivex.Single

interface RequestNetworkStoreOut {

    fun currentUserRequests(): Single<List<ReportRequest>>

    fun requestsByRadius(lng: Double, lat: Double, rad: Long): Single<List<ReportRequest>>

    fun createRequest(request: TransmittedReportRequest): Single<String>

    fun createRequestToUser(request: TransmittedReportRequest, userId: String): Single<String>

    fun deleteRequest(requestId: String): Single<Boolean>

    fun join(requestId: String): Single<Boolean>

    fun leave(requestId: String): Single<Boolean>

}
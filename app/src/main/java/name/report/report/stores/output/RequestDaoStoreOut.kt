package name.report.report.stores.output

import android.arch.persistence.room.*
import name.report.report.domain.ReportRequest
import io.reactivex.Flowable

@Dao
interface RequestDaoStoreOut {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(requests: List<ReportRequest>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(request: ReportRequest)

    @Query("SELECT * FROM report_requests")
    fun getAll(): Flowable<List<ReportRequest>>

    @Delete
    fun delete(request: ReportRequest)

}

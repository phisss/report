package name.report.report.stores.output

import name.report.report.domain.ReportUser
import name.report.report.domain.Tokens
import name.report.report.domain.UserAuth

interface ProfileCacheStoreOut {

    fun setUrl(url: String)

    fun getUrl(): String?

    fun setAuth(user: UserAuth)

    fun getAuth(): UserAuth?

    fun setTokens(tokens: Tokens)

    fun getTokens(): Tokens?

    fun getName(): String

    fun setName(name: String)

    fun setCurrUser(user: ReportUser)

    fun getCurrUser(): ReportUser?

    fun setFcm(token: String)

    fun getFcm(): String?

}

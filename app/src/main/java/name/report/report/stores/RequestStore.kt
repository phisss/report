package name.report.report.stores

import name.report.report.domain.*
import name.report.report.domain.output.RequestStoreDomainOut
import name.report.report.stores.output.CoordinateDaoStoreOut
import name.report.report.stores.output.RequestDaoStoreOut
import name.report.report.stores.output.RequestNetworkStoreOut
import name.report.report.stores.output.UserDaoStoreOut
import io.reactivex.Scheduler

/**
 * TODO add description for this class
 */
class RequestStore(private val requestDao: RequestDaoStoreOut,
                   private val coordinateDao: CoordinateDaoStoreOut,
                   private val userDao: UserDaoStoreOut,
                   private val requestNetwork: RequestNetworkStoreOut,
                   private val ioScheduler: Scheduler,
                   private val uiScheduler: Scheduler) : RequestStoreDomainOut {

    /**
     * This method updates db on success, and returns Single<Boolean>
     * In a onSuccess() callback in subscribe method always emitted true,
     * and in order to getAsFlowable data call [getAll]
     */
    fun updateByRadius(lng: Double, lat: Double, rad: Long) = requestNetwork.requestsByRadius(lng, lat, rad)
            .doOnSuccess {
                it.forEach {
                    val position = it.position
                    val user = it.user

                    when (position) {
                        is Point -> coordinateDao.insert(position.coordinate)
                        is Polygon -> coordinateDao.insert(position.coordinates)
                    }
                    if (user != null) {
                        //userDao.insert(user)
                    }
                }
                requestDao.insert(it)
            }
            .map { true }
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    /**
     * TODO add description for this method
     */
    fun getAll() = requestDao.getAll().map {
                it.map {
                    it.apply {
                        //user = userDao.get(it.userId)
                        val coordinates = coordinateDao.get(it._id)
                        it.position = when (coordinates.size) {
                            1 -> Point(coordinates.first())
                            else -> Polygon(coordinates)
                        }
                    }
                }
            }
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    fun put(reportRequest: TransmittedReportRequest)
            = requestNetwork.createRequest(reportRequest)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    fun remove(requestId: String) = requestNetwork.deleteRequest(requestId)
            //.doOnSuccess { requestDao.delete(request) }
            .map { true }
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!


    //RequestProvider

    override fun updateRequestsInRadius(lng: Double, lat: Double, rad: Long)
            = updateByRadius(lng, lat, rad)

    override fun getRequestsInRadius() = getAll()

    override fun createRequest(request: TransmittedReportRequest) = put(request)

    override fun deleteRequest(requestId: String) = remove(requestId)

    override fun getRequestsInRadiusRemote(lng: Double, lat: Double, rad: Long)
            = requestNetwork.requestsByRadius(lng, lat, rad)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    override fun getCurrentUserRequestsRemote()
            = requestNetwork.currentUserRequests()
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    override fun join(requestId: String) = requestNetwork.join(requestId)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    override fun leave(requestId: String) = requestNetwork.leave(requestId)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    override fun createRequestToUser(request: TransmittedReportRequest, userId: String)
            = requestNetwork.createRequestToUser(request, userId)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

}

package name.report.report.stores

import name.report.report.domain.Coordinate
import name.report.report.domain.output.PositionStoreDomainOut
import name.report.report.stores.output.PositionNetworkStoreOut
import io.reactivex.Scheduler

class PositionStore(private val positionNetworkStoreOut: PositionNetworkStoreOut,
                    private val ioScheduler: Scheduler,
                    private val uiScheduler: Scheduler)
    : PositionStoreDomainOut {

    /**
     * TODO add description for this method
     */
    fun updateByRadius(lng: Double, lat: Double, rad: Long)
            = positionNetworkStoreOut.usersPositionsByRadius(lng, lat, rad )
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    /**
     * TODO add description for this method
     */
    fun update(userId: String = "") = positionNetworkStoreOut.userPosition(userId)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!

    /**
     * Set current user position
     *
     * @param coordinate - current user's position
     */
    fun set(coordinate: Coordinate) = positionNetworkStoreOut.setUserPosition(coordinate)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)!!


    //PositionStoreDomainOut

    override fun userPosition(userId: String) = update(userId)

    override fun usersPositionsByRadius(lng: Double, lat: Double, rad: Long) = updateByRadius(lng, lat, rad)

    override fun setUserPosition(coordinate: Coordinate) = set(coordinate)

}

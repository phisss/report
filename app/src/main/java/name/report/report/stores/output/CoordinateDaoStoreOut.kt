package name.report.report.stores.output

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import name.report.report.domain.Coordinate
import io.reactivex.Flowable

@Dao
interface CoordinateDaoStoreOut {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(coordinate: Coordinate)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(coordinates: List<Coordinate>)

    @Query("SELECT * FROM coordinates WHERE ownerId IS :ownerId")
    fun getAsFlowable(ownerId: String): Flowable<List<Coordinate>>

    @Query("SELECT * FROM coordinates WHERE ownerId IS :ownerId")
    fun get(ownerId: String): List<Coordinate>

}

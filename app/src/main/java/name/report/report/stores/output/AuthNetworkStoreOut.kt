package name.report.report.stores.output

import name.report.report.domain.Tokens
import io.reactivex.Single
import retrofit2.Response

interface AuthNetworkStoreOut {

    fun getTokens(userId: String, password: String): Single<Tokens>

    fun updateFcmToken(token: String): Single<Response<Void>>

}

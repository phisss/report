package name.report.report.backgroundLocation

import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder

class StartStopService : Service() {

    internal var mService: LocationUpdatesService? = null
    internal var mBound = false


    private val mServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as LocationUpdatesService.LocalBinder
            mService = binder.service
            mBound = true
            startservice()


        }

        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
            mBound = false
        }
    }


    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        bindService(Intent(baseContext, LocationUpdatesService::class.java), mServiceConnection,
                Context.BIND_AUTO_CREATE)

    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return Service.START_STICKY
    }

    fun startservice() {
        mService!!.requestLocationUpdates()
    }

    fun stopservicedj1() {
        mService!!.removeLocationUpdates()
    }
}

package name.report.report

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import name.report.report.data.cache.AppPreferences
import name.report.report.presentation.intro.IntroActivity
import javax.inject.Inject

class SplashScreen : AppCompatActivity() {
    @Inject
    lateinit var appPreferences: AppPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_Launcher)
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN;
        App.appComponent.inject(this)

        if (appPreferences.guideShown()) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        } else {
            startActivity(Intent(this,IntroActivity::class.java))
            finish()
        }

    }
}

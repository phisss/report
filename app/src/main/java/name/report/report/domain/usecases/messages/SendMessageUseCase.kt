package name.report.report.domain.usecases.messages

import name.report.report.domain.ReportMessage
import name.report.report.domain.output.MessageStoreDomainOut

class SendMessageUseCase(private val messageStoreDomainOut: MessageStoreDomainOut) {

    fun execute(requestId: String, message: ReportMessage)
            = messageStoreDomainOut.sendMessage(requestId, message)

}

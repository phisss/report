package name.report.report.domain.usecases.auth

import name.report.report.domain.output.OuthDomainOut

class SetUserNameUseCase(private val outhDomainOut: OuthDomainOut) {

    fun execute(name: String) = outhDomainOut.setUserName(name)

}

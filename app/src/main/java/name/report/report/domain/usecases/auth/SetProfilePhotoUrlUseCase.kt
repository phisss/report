package name.report.report.domain.usecases.auth

import name.report.report.domain.output.OuthDomainOut

class SetProfilePhotoUrlUseCase(private val outhDomainOut: OuthDomainOut) {

    fun execute(url: String) = outhDomainOut.setProfilePhotoUrl(url)

}

package name.report.report.domain.usecases.users

import name.report.report.domain.output.UsersStoreDomainOut

class UpdateAndGetCurrUserUseCase(private val usersStoreDomainOut: UsersStoreDomainOut) {

    fun execute() = usersStoreDomainOut.updateAndGetCurrUser()

}

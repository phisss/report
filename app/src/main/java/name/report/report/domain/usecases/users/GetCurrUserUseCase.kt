package name.report.report.domain.usecases.users

import name.report.report.domain.output.UsersStoreDomainOut

class GetCurrUserUseCase(private val usersStoreDomainOut: UsersStoreDomainOut) {

    fun execute() = usersStoreDomainOut.getCurrUser()

}

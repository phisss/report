package name.report.report.domain.usecases.requests

import name.report.report.domain.output.RequestStoreDomainOut

class LeaveUseCase(private val requestStoreDomainOut: RequestStoreDomainOut) {

    fun execute(requestId: String) = requestStoreDomainOut.leave(requestId)

}

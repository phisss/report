package name.report.report.domain.usecases.position

import name.report.report.domain.output.PositionStoreDomainOut

class GetUsersPositionsByRadiusUseCase(private val positionsByRadius: PositionStoreDomainOut) {

    fun execute(lng: Double, lat: Double, rad: Long)
            = positionsByRadius.usersPositionsByRadius(lng, lat, rad)

}

package name.report.report.domain.usecases.messages

import name.report.report.domain.output.MessageStoreDomainOut

class AddRepointsUseCase(private val messageStoreDomainOut: MessageStoreDomainOut) {

    fun execute(messageId: String, repoints: Int)
            = messageStoreDomainOut.addRepoints(messageId, repoints)

}

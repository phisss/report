package name.report.report.domain.usecases.position

import name.report.report.domain.Coordinate
import name.report.report.domain.output.PositionStoreDomainOut

class SetCurrentUserPositionUseCase(private val positionStoreDomainOut: PositionStoreDomainOut) {

    fun execute(coordinate: Coordinate) = positionStoreDomainOut.setUserPosition(coordinate)

}
package name.report.report.domain.usecases.auth

import name.report.report.domain.output.OuthDomainOut

class GetUserNameUseCase(private val outhDomainOut: OuthDomainOut) {

    fun execute() = outhDomainOut.getUserName()

}

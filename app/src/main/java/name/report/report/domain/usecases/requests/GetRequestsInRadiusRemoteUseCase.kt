package name.report.report.domain.usecases.requests

import name.report.report.domain.output.RequestStoreDomainOut

class GetRequestsInRadiusRemoteUseCase(private val requestStoreDomainOut: RequestStoreDomainOut) {

    fun execute(lng: Double, lat: Double, rad: Long)
            = requestStoreDomainOut.getRequestsInRadiusRemote(lng, lat, rad)

}

package name.report.report.domain.usecases.requests

import name.report.report.domain.output.RequestStoreDomainOut

class DeleteRequestUseCase(private val requestStoreDomainOut: RequestStoreDomainOut) {

    fun execute(requestId: String) = requestStoreDomainOut.deleteRequest(requestId)

}

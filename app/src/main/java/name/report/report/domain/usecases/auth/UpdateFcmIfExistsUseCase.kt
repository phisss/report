package name.report.report.domain.usecases.auth

import io.reactivex.Single
import name.report.report.domain.output.OuthDomainOut

class UpdateFcmIfExistsUseCase(private val getFcmUseCase: GetFcmUseCase,
                               private val outhDomainOut: OuthDomainOut) {

    fun execute() = if (getFcmUseCase.execute() != null) {
        outhDomainOut.updateFcm(getFcmUseCase.execute()!!)
    } else {
        Single.error<Any>(IllegalStateException("fcm does not exists"))
    }!!

}
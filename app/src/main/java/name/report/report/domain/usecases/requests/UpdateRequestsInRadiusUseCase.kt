package name.report.report.domain.usecases.requests

import name.report.report.domain.output.RequestStoreDomainOut

class UpdateRequestsInRadiusUseCase(private val requestStoreDomainOut: RequestStoreDomainOut) {

    fun execute(lng: Double, lat: Double, rad: Long)
            = requestStoreDomainOut.updateRequestsInRadius(lng, lat, rad)
}
package name.report.report.domain

import com.google.gson.annotations.SerializedName

data class ReportMessage(
        val message: String? = null,
        val attachments: List<Attachment>? = null,
        val userId: String? = null,
        val hash: String? = null,
        val time: String? = null,
        @SerializedName("_id")
        val id: String? = null,
        val repoints: List<Repoint>? = null)

data class Attachment(val type: String, val src: String, val thumbnail: String? = null)

data class ReportDialog(
        val messages: List<ReportMessage>,
        val users: List<ReportUser>,
        val request: ReportRequest? = null)

data class Repoint(
        val repoints: Int,
        val userId: String,
        @SerializedName("_id")
        val id: String,
        val date: String?,
        val user: ReportUser?)

sealed class MessageEvent

data class NewMessageEvent(val reportMessage: ReportMessage) : MessageEvent()

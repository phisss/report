package name.report.report.domain.usecases.auth

import name.report.report.domain.output.AndroidResourcesDomainOut

class GetMacAddressUseCase(private val androidResourcesDomainOut: AndroidResourcesDomainOut) {

    fun execute() = androidResourcesDomainOut.macAddress()

}

package name.report.report.domain.usecases.requests

import name.report.report.domain.TransmittedReportRequest
import name.report.report.domain.output.RequestStoreDomainOut

class CreateRequestUseCase(private val requestStoreDomainOut: RequestStoreDomainOut) {

    fun execute(request: TransmittedReportRequest) = requestStoreDomainOut.createRequest(request)

}

package name.report.report.domain.usecases.auth

import name.report.report.domain.Tokens
import name.report.report.domain.output.OuthDomainOut

class SetTokensUseCase(private val outhDomainOut: OuthDomainOut) {

    fun execute(tokens: Tokens) = outhDomainOut.setTokens(tokens)

}

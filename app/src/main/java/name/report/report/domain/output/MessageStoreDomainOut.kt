package name.report.report.domain.output

import name.report.report.domain.MessageEvent
import name.report.report.domain.ReportDialog
import name.report.report.domain.ReportMessage
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response

interface MessageStoreDomainOut {

    fun sendMessage(requestId: String, message: ReportMessage): Single<String>

    fun requestDialog(requestId: String): Single<ReportDialog>

    fun userDialogs(type: DialogType): Single<List<ReportDialog>>

    fun events(): Observable<MessageEvent>

    fun hideEvents()

    fun addRepoints(messageId: String, repoints: Int): Single<Response<Void>>

    enum class DialogType {
        OWNER, REPORTER, JOINER, ALL
    }

}

package name.report.report.domain.output

import name.report.report.domain.Tokens
import name.report.report.domain.UserAuth
import io.reactivex.Single

interface OuthDomainOut {

    fun isAuthorized(): Boolean

    fun getTokens(): Tokens?

    fun setTokens(tokens: Tokens)

    fun updateTokensDemo(user: UserAuth): Single<Tokens>

    fun setAuth(user: UserAuth)

    fun getAuth(): UserAuth?

    fun getProfilePhotoUrl(): String?

    fun setProfilePhotoUrl(url: String)

    fun setUserName(name: String)

    fun getUserName(): String

    fun setFcm(token: String)

    fun getFcm(): String?

    fun updateFcm(token: String): Single<Any>?

}

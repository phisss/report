package name.report.report.domain.usecases.requests

import name.report.report.domain.output.RequestStoreDomainOut

class GetRequestsInRadiusUseCase(private val requestStoreDomainOut: RequestStoreDomainOut) {

    fun execute() = requestStoreDomainOut.getRequestsInRadius()

}

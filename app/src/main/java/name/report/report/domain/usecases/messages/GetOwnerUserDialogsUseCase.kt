package name.report.report.domain.usecases.messages

import name.report.report.domain.output.MessageStoreDomainOut
import name.report.report.domain.output.MessageStoreDomainOut.*

class GetOwnerUserDialogsUseCase(private val messageStoreDomainOut: MessageStoreDomainOut) {

    fun execute() = messageStoreDomainOut.userDialogs(DialogType.OWNER)

}

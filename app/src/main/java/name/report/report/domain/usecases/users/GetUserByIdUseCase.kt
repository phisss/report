package name.report.report.domain.usecases.users

import name.report.report.domain.output.UsersStoreDomainOut

class GetUserByIdUseCase(private val usersStoreDomainOut: UsersStoreDomainOut) {

    fun execute(userId: String) = usersStoreDomainOut.getUser(userId)

}

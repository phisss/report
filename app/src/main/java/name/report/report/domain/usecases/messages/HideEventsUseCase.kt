package name.report.report.domain.usecases.messages

import name.report.report.domain.output.MessageStoreDomainOut

class HideEventsUseCase(val messageStoreDomainOut: MessageStoreDomainOut) {

    fun execute() = messageStoreDomainOut.hideEvents()

}

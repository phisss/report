package name.report.report.domain

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.content.Context
import name.report.report.MainActivity
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName

@Entity(tableName = "coordinates")
data class Coordinate(
        val lng: Double,
        val lat: Double,
        val ownerId: String = "",
        @PrimaryKey(autoGenerate = true)
        val id: Long = 0)

sealed class Position

data class Point(val coordinate: Coordinate) : Position()

data class Polygon(val coordinates: List<Coordinate>) : Position()

data class Tokens(
        @SerializedName("access_token")
        var accessToken: String,
        @SerializedName("refresh_token")
        var refreshToken: String,
        var customToken: String,
        var type: TokenType
)

enum class TokenType {
    VK, FB, DEMO
}

data class TransmittedReportRequest(
        var position: Position,
        var message: String,
        var place: String,
        var timeToStayAlive: Long,
        var description: String
)

@Entity(tableName = "report_requests")
data class ReportRequest @JvmOverloads constructor(
        @PrimaryKey
        var _id: String,
        var message: String?,
        var description: String?,
        var place: String?,
        var userId: String,
        var hash: String?,
        var closed: Boolean,
        var answered: Boolean,
        var timeToStayAlive: Long?,
        var time: String,
        var distance: Double?,
        var timeRaw: String,
        @Ignore
        var user: ReportUser? = null,
        @Ignore
        var position: Position? = null)

data class RequestToAlert(
        var position: LatLng,
        var description: String,
        var title: String,
        var photoUrl: String,
        var _id: String,
        var place: String,
        var time: String,
        var userId: String,
        var followed: Boolean
)

data class RequestToAlertCur(
        var position: LatLng,
        var description: String,
        var title: String,
        var photoUrl: String,
        var _id: String,
        var place: String,
        var time: String,
        var userId: String,
        var context: Context,
        var activity: MainActivity
)

data class RequestToAlertIncoming(
        val title: String,
        val description: String,
        val userId: String,
        val photoUrl: String,
        val context: Context,
        val _id: String
)

data class ReportUser(
        @PrimaryKey
        var userId: String,
        var nickName: String? = null,
        var accType: String?= null,
        var firstName: String?= null,
        var lastName: String?= null,
        var photo: String?= null,
        var sex: String?= null,
        var socialLink: String?= null,
        var rating: Int?= null,
        var created: String?= null,
        var fcmToken: String?= null,
        var vkToken: String?= null,
        var fbToken: String?= null,
        var banned: Boolean?= null,
        var type: String?= null,
        var requestCount: Int?= null,
        var dialogsCount: Int?= null,
        var repoints: List<Repoint>? = null)

data class UserPosition(
        var currentPosition: Position,
        var syncTime: String,
        var distance: Double? = null,
        var user: ReportUser? = null
)

data class SendRepoints(
        val messageId: String,
        var value: Int)

data class ShowRepoints(
        val messageId: String,
        var value: Boolean)

data class UserAuth(
        val userId: String,
        val password: String)

data class DeleteRequest(
        val _id: String,
        val title: String,
        val userId: String)

data class MediaContent(
        val type: String,
        val src: String)

data class RepointtoList(
        var userPhoto: String,
        var userName: String,
        var userRepoints: String
)

data class RepointToSend(
        var messageId: String,
        var value: Int
)


data class UserMenuBS(
        var userPhoto: String,
        var userName: String,
        var userId: String
)
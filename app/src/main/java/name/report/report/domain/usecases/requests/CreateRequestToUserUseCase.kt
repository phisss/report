package name.report.report.domain.usecases.requests

import name.report.report.domain.TransmittedReportRequest
import name.report.report.domain.output.RequestStoreDomainOut

class CreateRequestToUserUseCase(val requestStoreDomainOut: RequestStoreDomainOut) {

    fun execute(request: TransmittedReportRequest, userId: String)
            = requestStoreDomainOut.createRequestToUser(request, userId)

}

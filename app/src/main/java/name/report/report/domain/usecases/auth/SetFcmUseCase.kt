package name.report.report.domain.usecases.auth

import name.report.report.domain.output.OuthDomainOut

class SetFcmUseCase(private val outhDomainOut: OuthDomainOut) {

    fun execute(token: String) = outhDomainOut.setFcm(token)

}

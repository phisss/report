package name.report.report.domain.usecases.auth

import name.report.report.domain.output.OuthDomainOut

class GetFcmUseCase(private val outhDomainOut: OuthDomainOut) {

    fun execute() = outhDomainOut.getFcm()

}

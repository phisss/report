package name.report.report.domain.usecases.auth

import name.report.report.domain.output.OuthDomainOut

class GetProfilePhotoUrlUseCase(private val outhDomainOut: OuthDomainOut) {

    fun execute() = outhDomainOut.getProfilePhotoUrl()

}

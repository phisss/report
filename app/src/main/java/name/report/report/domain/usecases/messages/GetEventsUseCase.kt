package name.report.report.domain.usecases.messages

import name.report.report.domain.output.MessageStoreDomainOut

class GetEventsUseCase(private val messageStoreDomainOut: MessageStoreDomainOut) {

    fun execute() = messageStoreDomainOut.events()

}

package name.report.report.domain.usecases.messages

import name.report.report.domain.output.MessageStoreDomainOut

class GetRequestDialogUseCase(private val messageStoreDomainOut: MessageStoreDomainOut) {

    fun execute(requestId: String) = messageStoreDomainOut.requestDialog(requestId)

}

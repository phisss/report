package name.report.report.domain.output

import name.report.report.domain.ReportUser
import io.reactivex.Single

interface UsersStoreDomainOut {

    fun getCurrUser(): ReportUser?

    fun updateAndGetCurrUser(): Single<ReportUser>

    fun getUser(userId: String): Single<ReportUser>

}

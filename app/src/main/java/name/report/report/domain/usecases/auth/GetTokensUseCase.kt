package name.report.report.domain.usecases.auth

import name.report.report.domain.TokenType
import name.report.report.domain.Tokens
import name.report.report.domain.UserAuth
import name.report.report.domain.output.OuthDomainOut
import io.reactivex.Single
import timber.log.Timber
import java.util.*

class GetTokensUseCase(private val outhManager: OuthDomainOut,
                       private val getMacAddressUseCase: GetMacAddressUseCase) {

    /**
     * If authorized then getTokens as Single tokens from a local store else from server
     */
    fun execute(): Single<Tokens> {
        return if (outhManager.isAuthorized()) {
            Single.just(outhManager.getTokens())
        } else {
            val macAddress = getMacAddressUseCase.execute()
//            val newUser = UserAuth(userId = macAddress, password = UUID.randomUUID().toString())
            val newUser = UserAuth(UUID.randomUUID().toString(), UUID.randomUUID().toString())
            outhManager.updateTokensDemo(newUser)
                    .doOnSuccess {
                        outhManager.setTokens(Tokens(it.accessToken, it.customToken, it.refreshToken, TokenType.DEMO))
                        outhManager.setAuth(newUser)
                    }
        }
    }
}

package name.report.report.domain.usecases.position

import name.report.report.domain.output.PositionStoreDomainOut

class GetUserPositionById(private val positionStoreDomainOut: PositionStoreDomainOut) {

    fun execute(userId: String) = positionStoreDomainOut.userPosition(userId)

}

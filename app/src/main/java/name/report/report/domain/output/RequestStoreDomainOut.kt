package name.report.report.domain.output

import name.report.report.domain.ReportRequest
import name.report.report.domain.TransmittedReportRequest
import io.reactivex.Flowable
import io.reactivex.Single

interface RequestStoreDomainOut {

    fun getRequestsInRadius(): Flowable<List<ReportRequest>>

    fun updateRequestsInRadius(lng: Double, lat: Double, rad: Long): Single<Boolean>

    fun createRequest(request: TransmittedReportRequest): Single<String>

    fun createRequestToUser(request: TransmittedReportRequest, userId: String): Single<String>

    fun deleteRequest(requestId: String): Single<Boolean>

    fun getRequestsInRadiusRemote(lng: Double, lat: Double, rad: Long): Single<List<ReportRequest>>

    fun getCurrentUserRequestsRemote(): Single<List<ReportRequest>>

    fun join(requestId: String): Single<Boolean>

    fun leave(requestId: String): Single<Boolean>

}

package name.report.report.registration

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import name.report.report.R
import name.report.report.domain.TokenType
import name.report.report.domain.Tokens
import name.report.report.presentation.main.viewmodel.MainActivityViewModel
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.web.*
import name.report.report.App
import name.report.report.domain.usecases.auth.UpdateFcmIfExistsUseCase
import timber.log.Timber
import javax.inject.Inject

@Suppress("DEPRECATION")
/**
 * A simple [Fragment] subclass.
 */
class FbFragment : Fragment() {

    @Inject
    lateinit var updateFcmIfExists: UpdateFcmIfExistsUseCase

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    private val mainActivityViewModel: MainActivityViewModel by lazy {
        ViewModelProviders.of(activity!!).get(MainActivityViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(name.report.report.R.layout.web, container, false)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        webb.settings.javaScriptEnabled=true
        webb.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                if (url.contains("report://")) {
                    val newAccessToken = url.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0].split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[2]

                    mainActivityViewModel.getTokensUseCase.execute()
                            .subscribeBy(
                                    onSuccess = {
                                        Timber.e("RE:: $it")
                                        Timber.e("RE:: $newAccessToken")
                                        mainActivityViewModel.setTokenUseCase.execute(Tokens(
                                                newAccessToken,
                                                it.refreshToken,
                                                it.customToken,
                                                TokenType.FB
                                        ))
                                        mainActivityViewModel.getTokensUseCase.execute()
                                                .subscribeBy(
                                                        onSuccess = {
                                                            Timber.e("RE:: $it")
                                                            updateFcmIfExists.execute().subscribeBy(
                                                                    onSuccess = { Timber.d("FCM TOKEN UPDATE") },
                                                                    onError = { Timber.d("FCM TOKEN UPDATE FAILURE") }
                                                            )
                                                        },
                                                        onError = {

                                                        }
                                                )

                                        mainActivityViewModel.updateAndgetCurrUserUseCase.execute()
                                                .subscribeBy(
                                                    onSuccess = {
                                                        mainActivityViewModel.logged.value = true
                                                    },
                                                        onError = {}
                                                )


                                    },
                                    onError = {

                                    }
                            )
                    return true
                }
                return super.shouldOverrideUrlLoading(view, url)
            }
        }
        webb.loadUrl("https://report-project.herokuapp.com/register/fb?clientId=android&clientSecret=SomeRandomCharsAndNumbers&uniqueId="+mainActivityViewModel.getUserAuthUseCase.execute()!!.userId)

    }
}
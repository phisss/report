package name.report.report.data.network.api

import name.report.report.data.network.dto.CreateRequestResponse
import name.report.report.data.network.dto.GetRequestsResponse
import name.report.report.domain.TransmittedReportRequest
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface RequestsApi {

    @GET("/requests/{lng}/{lat}/{rad}")
    fun getRequests(@Path("lng") lng: Double,
                    @Path("lat") lat: Double,
                    @Path("rad") rad: Long): Single<GetRequestsResponse>

    @GET("/requests")
    fun getCurUserRequests(): Single<GetRequestsResponse>

    @POST("/requests")
    fun createRequest(@Body createRequestBody: TransmittedReportRequest): Single<CreateRequestResponse>

    @DELETE("/requests/{requestId}")
    fun deleteRequest(@Path("requestId") requestId: String): Single<Response<Void>>

    @PATCH("/requests/join/{requestId}")
    fun join(@Path("requestId") requestId: String): Single<Response<Void>>

    @PATCH("/requests/leave/{requestId}")
    fun leave(@Path("requestId") requestId: String): Single<Response<Void>>

    @POST("/requests/{userId}")
    fun createRequestToUser(@Body createRequestBody: TransmittedReportRequest,
                            @Path("userId") requestId: String): Single<CreateRequestResponse>

}

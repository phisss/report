package name.report.report.data.network.firebase

import android.graphics.Bitmap
import android.net.Uri
import name.report.report.data.network.dto.MediaUploaderEvent
import name.report.report.data.network.dto.OnComplete
import name.report.report.data.network.dto.OnProgress
import name.report.report.data.network.managers.MediaUploader
import name.report.report.data.network.managers.MediaUploader.MediaType.*
import name.report.report.utils.bytesProgressToIntPercent
import com.google.firebase.storage.FirebaseStorage
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.io.File
import android.R.attr.bitmap
import java.io.ByteArrayOutputStream


class FirebaseFileUploader : MediaUploader {

    override fun uploadMedia(path: String, type: MediaUploader.MediaType)
            = uploadMediaToFirebase(path, type)

    override fun uploadMediaList(paths: List<String>, type: MediaUploader.MediaType)
            = uploadMediaListToFirebase(paths, type)

    private val storage = FirebaseStorage.getInstance()

    fun uploadMediaToFirebase(path: String,
                             type: MediaUploader.MediaType,
                             mediaSubject: PublishSubject<MediaUploaderEvent> = PublishSubject.create<MediaUploaderEvent>()): Observable<MediaUploaderEvent> {

        val fileUri = Uri.fromFile(File(path))

        val folder = if (type == PHOTO) "photos/" else "videos/"
        val reference = storage.reference.child(folder + fileUri.lastPathSegment)

        val uploadTask = reference.putFile(fileUri)
        uploadTask
                .addOnFailureListener {
                    mediaSubject.onError(it)
                }
                .addOnSuccessListener {
                    mediaSubject.onNext(OnComplete(it.downloadUrl!!))
                }
                .addOnProgressListener {
                    mediaSubject.onNext(OnProgress(it.bytesProgressToIntPercent()))
                }
        return mediaSubject.hide()
    }

    fun uploadMediaListToFirebase(paths: List<String>, type: MediaUploader.MediaType): Observable<MediaUploaderEvent> {

        val mediaSubject = PublishSubject.create<MediaUploaderEvent>()

        paths.forEach { uploadMediaToFirebase(it, type, mediaSubject) }

        return mediaSubject
    }

    override fun uploadBitmap(bitmap: Bitmap): Observable<String> {

        val subject = PublishSubject.create<String>()

        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()
        val reference = storage.reference.child("/photo/thumb.jpg")

        val uploadTask = reference.putBytes(data)
        uploadTask
                .addOnFailureListener {
                    subject.onError(it)
                }
                .addOnSuccessListener {
                    subject.onNext(it.downloadUrl.toString())
                }
        return subject.hide()
    }

}

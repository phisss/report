package name.report.report.data.os

import android.content.Context
import android.net.wifi.WifiManager
import name.report.report.stores.output.MacAddressStoreOut

class AndroidResourses(private val context: Context) : MacAddressStoreOut {

    override fun macAddress() = (context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager)
            .connectionInfo
            .macAddress!!

}

package name.report.report.data.network.api

import name.report.report.data.network.dto.AuthBody
import name.report.report.data.network.dto.RefreshFCMTokenBody
import name.report.report.data.network.dto.PushMessage
import name.report.report.domain.Tokens
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface OuthApi {

    @POST("/register/demo")
    fun signInAnonymously(@Body authBody: AuthBody): Single<Tokens>

    @POST("/notifications/refresh/fcmToken")
    fun refreshFcmToken(@Body fcmTokenBody: RefreshFCMTokenBody): Single<Response<Void>>

    @POST("/notifications/push_to/{userId}")
    fun testPush(@Body pushMessage: PushMessage, @Path("userId") userId: String): Single<Response<Void>>

}

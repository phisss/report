package name.report.report.data.network.managers

import name.report.report.data.network.RetrofitBuilder
import name.report.report.data.network.api.RequestsApi
import name.report.report.domain.TransmittedReportRequest
import name.report.report.stores.output.RequestNetworkStoreOut

class ReportRequestsNetworkManager(private val retrofitBuilder: RetrofitBuilder) : RequestNetworkStoreOut {

    private fun requestApi() = retrofitBuilder.getRequestsApi()

    override fun createRequestToUser(request: TransmittedReportRequest, userId: String)
            = requestApi().createRequestToUser(request, userId).map { it.requestId }!!

    override fun createRequest(request: TransmittedReportRequest) =
            requestApi().createRequest(request).map { it.requestId }!!

    override fun currentUserRequests()
            = requestApi().getCurUserRequests().map { it.requests }!!

    override fun requestsByRadius(lng: Double, lat: Double, rad: Long)
            = requestApi().getRequests(lng, lat, rad).map { it.requests }!!

    override fun deleteRequest(requestId: String) = requestApi().deleteRequest(requestId).map { true }

    override fun join(requestId: String) = requestApi().join(requestId).map { true }

    override fun leave(requestId: String) = requestApi().leave(requestId).map { true }

}

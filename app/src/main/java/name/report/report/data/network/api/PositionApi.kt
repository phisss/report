package name.report.report.data.network.api

import name.report.report.data.network.dto.GetUsersPositionsResponse
import name.report.report.data.network.dto.PositionBody
import name.report.report.domain.UserPosition
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface PositionApi {

    @GET("/position/{lng}/{lat}/{rad}")
    fun getUsersPositionsByRadius(@Path("lng") lng: Double,
                                  @Path("lat") lat: Double,
                                  @Path("rad") rad: Long): Single<GetUsersPositionsResponse>

    /**
     *  Call without params - return [UserPosition] for current user
     */
    @GET("/position/{userId}")
    fun getUserPosition(@Path("userId") userId: String = ""): Single<UserPosition>

    @POST("/position")
    fun setUserPosition(@Body positionBody: PositionBody): Single<UserPosition>

}

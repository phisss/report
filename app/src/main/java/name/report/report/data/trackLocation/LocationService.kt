package name.report.report.data.trackLocation

import android.Manifest
import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Binder
import android.os.IBinder
import com.google.android.gms.location.LocationServices
import io.reactivex.rxkotlin.subscribeBy
import name.report.report.App
import name.report.report.domain.Coordinate
import name.report.report.domain.usecases.position.SetCurrentUserPositionUseCase
import name.report.report.utils.checkPermissionsGranted
import timber.log.Timber
import javax.inject.Inject

class LocationService : Service() {

    @Inject
    lateinit var setCurrentUserPositionUseCase: SetCurrentUserPositionUseCase

    @SuppressLint("MissingPermission")
    override fun onCreate() {
        App.appComponent.inject(this)
        super.onCreate()

        if (!applicationContext.checkPermissionsGranted(listOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        ))) return

        val locationProvider = LocationServices.getFusedLocationProviderClient(applicationContext)
        locationProvider.lastLocation.addOnSuccessListener {
            if (it != null) {
                setCurrentUserPositionUseCase.execute(Coordinate(it.longitude, it.latitude))
                        .subscribeBy(
                                onSuccess = {
                                    Timber.d("LOCATION SEND")
                                    stopSelf()
                                },
                                onError = {
                                    Timber.d("LOCATION SEND FAIL")
                                    stopSelf()
                                }
                        )
            }
        }
    }

    override fun onBind(intent: Intent?) = Binder()


}
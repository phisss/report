package name.report.report.data.network

import name.report.report.BuildConfig
import name.report.report.data.cache.TokensCache
import name.report.report.data.network.adapters.RequestPositionAdapter
import name.report.report.data.network.adapters.TransmittedRequestAdapter
import name.report.report.data.network.adapters.UserPositionAdapter
import name.report.report.data.network.api.*
import name.report.report.domain.ReportRequest
import name.report.report.domain.TransmittedReportRequest
import name.report.report.domain.UserPosition
import name.report.report.utils.addHeaders
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitBuilder(val tokensCache: TokensCache) {

    private fun signedHeaders() =
        listOf(Pair("Authorization", "Bearer ${tokensCache.getTokens()!!.accessToken}"))


    private val outhHeaders = listOf(
            Pair("content-type", "application/json"),
            Pair("authorization", "Basic ${name.report.report.BuildConfig.APPLICATION_TOKEN}"))


    private fun <T> buildRetrofit(client: OkHttpClient, clazz: Class<T>) = Retrofit.Builder()
            .baseUrl(name.report.report.BuildConfig.API_SERVER_ENDPOINT)
            .addConverterFactory(GsonConverterFactory.create(
                    GsonBuilder()
                            .registerTypeAdapter(ReportRequest::class.java, RequestPositionAdapter())
                            .registerTypeAdapter(TransmittedReportRequest::class.java, TransmittedRequestAdapter())
                            .registerTypeAdapter(UserPosition::class.java, UserPositionAdapter())
                            .create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
            .create(clazz)

    private fun buildClient(headers: List<Pair<String, String>>) = OkHttpClient.Builder()
            .addInterceptor {
                it.proceed(it.request()
                        .newBuilder()
                        .addHeaders(headers)
                        .build())
            }
            .addInterceptor(
                    HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()

    fun getOuthApi() = buildRetrofit(buildClient(outhHeaders), OuthApi::class.java)

    fun getSignedOuthApi() = buildRetrofit(buildClient(signedHeaders()), OuthApi::class.java)

    fun getRequestsApi() = buildRetrofit(buildClient(signedHeaders()), RequestsApi::class.java)

    fun getPositionApi() = buildRetrofit(buildClient(signedHeaders()), PositionApi::class.java)

    fun getMessagesApi() = buildRetrofit(buildClient(signedHeaders()), MessageApi::class.java)

    fun getUsersApi() = buildRetrofit(buildClient(signedHeaders()), UsersApi::class.java)

}

package name.report.report.data.network.adapters

import name.report.report.domain.*
import com.google.gson.*
import java.lang.reflect.Type

class UserPositionAdapter : JsonDeserializer<UserPosition> {

    override fun deserialize(json: JsonElement,
                             typeOfT: Type?,
                             context: JsonDeserializationContext?): UserPosition {

        var jsonObject = json.asJsonObject
        // Oh..
        val positon = jsonObject.getAsJsonObject("positon")
        val userPositionJson = jsonObject.getAsJsonObject("position") ?: positon

        if (userPositionJson != null) {
            jsonObject = userPositionJson
        }

        val syncTime = jsonObject.getAsJsonPrimitive("syncTime").asString

        val currentPosition = jsonObject.getAsJsonObject("currentPosition")
        val type = currentPosition.getAsJsonPrimitive("type").asString
        val coordinatesJson = currentPosition.getAsJsonArray("coordinates")

        val position = when (type) {
            "Point" -> Point(jsonToPoint(coordinatesJson))
            "Polygon" -> Polygon(jsonToPointList(coordinatesJson))
            else -> throw IllegalStateException("bad type: $json")
        }

        var user: ReportUser? = null
        val userJson = jsonObject.getAsJsonObject("user")
        if (userJson != null) {
            user = Gson().fromJson(userJson, ReportUser::class.java)
        }

        val distance = jsonObject.getAsJsonPrimitive("distance")?.asDouble

        return UserPosition(position, syncTime, distance, user)
    }

    private fun jsonToPoint(json: JsonArray)
            = Coordinate(json[0].asDouble, json[1].asDouble)

    private fun jsonToPointList(json: JsonArray) = json[0].asJsonArray.map {
        val coordinateJson = it.asJsonArray
        Coordinate(coordinateJson[0].asDouble, coordinateJson[1].asDouble)
    }

}

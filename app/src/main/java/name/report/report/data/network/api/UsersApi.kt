package name.report.report.data.network.api

import name.report.report.data.network.dto.GetCurrUserInfoResponse
import name.report.report.domain.ReportUser
import io.reactivex.Single
import name.report.report.data.network.dto.GetUserByIdResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface UsersApi {

    @GET("/users")
    fun getCurrUserInfo(): Single<GetCurrUserInfoResponse>

    @GET("/users/{userId}")
    fun getUser(@Path("userId") userId: String): Single<GetUserByIdResponse>

}

package name.report.report.data.network.managers

import name.report.report.data.network.RetrofitBuilder
import name.report.report.data.network.api.PositionApi
import name.report.report.data.network.dto.PositionBody
import name.report.report.domain.Coordinate
import name.report.report.stores.output.PositionNetworkStoreOut

class PositionNetworkManager(private val retrofitBuilder: RetrofitBuilder)
    : PositionNetworkStoreOut {

    private fun positionApi() = retrofitBuilder.getPositionApi()

    override fun setUserPosition(coordinate: Coordinate)
            = positionApi().setUserPosition(PositionBody(listOf(coordinate.lng, coordinate.lat)))

    override fun userPosition(userId: String) = positionApi().getUserPosition(userId)

    override fun usersPositionsByRadius(lng: Double, lat: Double, rad: Long)
            = positionApi().getUsersPositionsByRadius(lng, lat, rad)
            .map { it.users }!!

}

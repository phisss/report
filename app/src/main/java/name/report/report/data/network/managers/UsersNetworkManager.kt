package name.report.report.data.network.managers

import name.report.report.data.network.RetrofitBuilder
import name.report.report.stores.output.UsersNetworkStoreOut

class UsersNetworkManager(private val retrofitBuilder: RetrofitBuilder) : UsersNetworkStoreOut{

    private fun usersApi() = retrofitBuilder.getUsersApi()

    override fun getCurrUser() = retrofitBuilder.getUsersApi().getCurrUserInfo().map { it.user }!!

    override fun getUser(userId: String) = usersApi().getUser(userId).map { it.user }!!

}

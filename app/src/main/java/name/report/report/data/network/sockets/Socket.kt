package name.report.report.data.network.sockets

import name.report.report.data.cache.TokensCache
import name.report.report.domain.MessageEvent
import name.report.report.domain.NewMessageEvent
import name.report.report.domain.ReportMessage
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import io.reactivex.subjects.PublishSubject
import timber.log.Timber

class Socket(url: String, tokensCache: TokensCache) {

    private val socketIo by lazy {
        IO.socket(url, IO.Options().apply {
            query = "token=${tokensCache.getTokens()!!.accessToken}"
        })
    }
    private val subject = PublishSubject.create<MessageEvent>()

    fun connect() {
        socketIo.once(Socket.EVENT_CONNECT, {
            Timber.d("ON CONNECT")
            socketIo.on("newMessage", {
                val mess = Gson().fromJson<ReportMessage>(it[0].toString(), ReportMessage::class.java)
                subject.onNext(NewMessageEvent(mess))
                Timber.d("NEW MESSAGE: $mess")
            })
            socketIo.on("newRequest", {
                Timber.d("NEW REQUEST JSON: ${it[0]}")
            })
        })
        socketIo.connect()
    }

    fun events() = subject.hide()

    fun disconnect() {
        socketIo.disconnect()
    }

}

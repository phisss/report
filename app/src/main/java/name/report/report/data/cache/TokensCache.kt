package name.report.report.data.cache

import name.report.report.domain.Tokens

interface TokensCache {

    fun getTokens(): Tokens?

}

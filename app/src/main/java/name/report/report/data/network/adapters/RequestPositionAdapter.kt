package name.report.report.data.network.adapters

import name.report.report.domain.*
import com.google.gson.*
import java.lang.Exception
import java.lang.reflect.Type

class RequestPositionAdapter : JsonDeserializer<ReportRequest?> {

    override fun deserialize(json: JsonElement,
                             typeOfT: Type?,
                             context: JsonDeserializationContext?): ReportRequest? {


        val request = json.asJsonObject

        if (request.size() == 0) return null

        val positionJson = request.getAsJsonObject("position")
        val coordinatesJson = positionJson.getAsJsonArray("coordinates")

        val requestId = request.getAsJsonPrimitive("_id").asString
        val type = positionJson.getAsJsonPrimitive("type").asString
        val message = request.getAsJsonPrimitive("message")?.asString
        val description = request.getAsJsonPrimitive("description")?.asString
        val place = request.getAsJsonPrimitive("place")?.asString
        val userId = request.getAsJsonPrimitive("userId").asString
        val hash = request.getAsJsonPrimitive("hash")?.asString
        val closed = request.getAsJsonPrimitive("closed").asBoolean
        val answered = request.getAsJsonPrimitive("answered").asBoolean
        val timeToStayAlive = request.getAsJsonPrimitive("timeToStayAlive")?.asLong
        val time = request.getAsJsonPrimitive("time").asString

        val position = when (type) {
            "Point" -> Point(jsonToPoint(coordinatesJson, requestId))
            "Polygon" -> Polygon(jsonToPointList(coordinatesJson, requestId))
            else -> throw IllegalStateException("bad type: $json")
        }

        val distance = request.getAsJsonPrimitive("distance")?.asDouble
        val user = Gson().fromJson(request.getAsJsonObject("user"), ReportUser::class.java)
        val timeRaw: String
        timeRaw = try{
            request!!.getAsJsonPrimitive("timeRaw")!!.asString
        }catch (e: Exception){
            e.printStackTrace()
            " "
        }

        return ReportRequest(requestId, message, description, place, userId, hash, closed, answered,
                timeToStayAlive, time, distance, timeRaw, user, position)
    }

    private fun jsonToPoint(json: JsonArray, requestId: String)
            = Coordinate(json[0].asDouble, json[1].asDouble, requestId)

    private fun jsonToPointList(json: JsonArray, requestId: String) = json[0].asJsonArray.map {
        val coordinateJson = it.asJsonArray
        Coordinate(coordinateJson[0].asDouble, coordinateJson[1].asDouble, requestId)
    }

}
package name.report.report.data.network.managers

import android.graphics.Bitmap
import name.report.report.data.network.dto.MediaUploaderEvent
import io.reactivex.Observable

interface MediaUploader {

    fun uploadMedia(path: String, type: MediaType): Observable<MediaUploaderEvent>

    fun uploadMediaList(paths: List<String>, type: MediaType): Observable<MediaUploaderEvent>

    fun uploadBitmap(bitmap: Bitmap): Observable<String>

    enum class MediaType {
        PHOTO, VIDEO
    }

}

package name.report.report.data.trackLocation

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat

class Util {

    companion object {

        fun scheduleLocationJob(context: Context) {
            val componentName = ComponentName(context, LocationJobService::class.java)
            val jobInfo = JobInfo.Builder(0, componentName)
                    .setMinimumLatency(30 * 1000) //1 minute
                    .setOverrideDeadline(60 * 1000) //3 minutes
                    .setPersisted(true)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                    .build()

            val jobcSheduler = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
            jobcSheduler.schedule(jobInfo)
        }

    }

}
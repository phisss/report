package name.report.report.data.cache

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import name.report.report.BuildConfig
import name.report.report.domain.Coordinate
import name.report.report.domain.ReportRequest
import name.report.report.stores.output.CoordinateDaoStoreOut
import name.report.report.stores.output.RequestDaoStoreOut

@Database(
        entities = arrayOf(
                //ReportUser::class,
                ReportRequest::class,
                Coordinate::class
        ),
        version = name.report.report.BuildConfig.ROOM_VERSION)
abstract class AppDataBase : RoomDatabase() {

    abstract fun requestDaoRepoOut(): RequestDaoStoreOut

    abstract fun coordinateDaoRepoOut(): CoordinateDaoStoreOut

    //abstract fun userDaoRepoOut(): UserDaoStoreOut
}

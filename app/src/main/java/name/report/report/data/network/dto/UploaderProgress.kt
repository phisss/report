package name.report.report.data.network.dto

import android.net.Uri
import android.support.annotation.IntRange

sealed class MediaUploaderEvent

data class OnProgress(@IntRange(from = 0, to = 100) val percent: Double) : MediaUploaderEvent()

data class OnComplete(val url: Uri) : MediaUploaderEvent()

package name.report.report.data.network.api

import name.report.report.data.network.dto.PutMessageResponse
import name.report.report.data.network.dto.UserDialogsResponse
import name.report.report.domain.ReportDialog
import name.report.report.domain.ReportMessage
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface MessageApi {

    companion object {
        const val OWNER = "owner"
        const val REPORTER = "reporter"
        const val JOINER = "joiner"
    }

    @GET("/messages/dialogs")
    fun getAllUserDialogs(): Single<UserDialogsResponse>

    @GET("/messages/get/{requestId}")
    fun getRequestDialog(@Path("requestId") requestId: String): Single<ReportDialog>

    @PUT("/messages/{requestId}")
    fun sendMessage(@Path("requestId") requestId: String, @Body message: ReportMessage): Single<PutMessageResponse>

    @GET("/messages/dialogs")
    fun getDialogsByType(@Query("type") type: String) : Single<UserDialogsResponse>

    @PATCH("/messages/{messageId}/{repoints}")
    fun addRepoints(@Path("messageId") messageId: String, @Path("repoints") repoints: Int): Single<Response<Void>>

}

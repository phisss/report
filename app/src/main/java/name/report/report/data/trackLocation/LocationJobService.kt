package name.report.report.data.trackLocation

import android.Manifest
import android.annotation.SuppressLint
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Intent
import android.widget.Toast
import name.report.report.utils.checkPermissionsGranted
import com.google.android.gms.location.LocationServices
import io.reactivex.rxkotlin.subscribeBy
import name.report.report.App
import name.report.report.domain.Coordinate
import name.report.report.domain.usecases.position.SetCurrentUserPositionUseCase
import timber.log.Timber
import javax.inject.Inject

class LocationJobService : JobService() {


    @Inject
    lateinit var setCurrentUserPositionUseCase: SetCurrentUserPositionUseCase

    init {
        App.appComponent.inject(this)
    }

    override fun onStopJob(params: JobParameters?) = true

    @SuppressLint("MissingPermission")
    override fun onStartJob(params: JobParameters?): Boolean {
        Timber.d("ON START JOB")
        //startService(Intent(applicationContext, LocationService::class.java))

        if (!applicationContext.checkPermissionsGranted(listOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        ))) return false

        val locationProvider = LocationServices.getFusedLocationProviderClient(applicationContext)
        locationProvider.lastLocation.addOnSuccessListener {
            if (it != null) {
                setCurrentUserPositionUseCase.execute(Coordinate(it.longitude, it.latitude))
                        .subscribeBy(
                                onSuccess = {
                                    Timber.d("LOCATION SEND")
                                    stopSelf()
                                },
                                onError = {
                                    Timber.d("LOCATION SEND FAIL")
                                    stopSelf()
                                }
                        )
            }
        }
        Util.scheduleLocationJob(applicationContext)
        return true
    }

}

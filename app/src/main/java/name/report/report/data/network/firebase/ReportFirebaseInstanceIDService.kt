package name.report.report.data.network.firebase

import name.report.report.App
import name.report.report.data.cache.TokensCache
import name.report.report.data.network.managers.OuthNetworkManager
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import name.report.report.domain.usecases.auth.SetFcmUseCase
import timber.log.Timber
import javax.inject.Inject

class ReportFirebaseInstanceIDService : FirebaseInstanceIdService() {

    @Inject
    lateinit var outhNetworkManager: OuthNetworkManager

    @Inject
    lateinit var tokensCache: TokensCache

    @Inject
    lateinit var setFcmUseCase: SetFcmUseCase

    //@Inject
    //lateinit var updateAndGetCurrUserUseCase: UpdateAndGetCurrUserUseCase

    override fun onTokenRefresh() {
        super.onTokenRefresh()
        App.appComponent.inject(this)

        val refreshedToken = FirebaseInstanceId.getInstance().token
        setFcmUseCase.execute(refreshedToken!!)

        Timber.d("NEW FCM TOKEN!: $refreshedToken")
        if (tokensCache.getTokens() != null) {
            Timber.d("TOKENS NOT NULL")
            outhNetworkManager.updateFcmToken(refreshedToken!!)
                    //.flatMap { updateAndGetCurrUserUseCase.execute() }
                    //.flatMap { outhNetworkManager.testPush(PushMessage("testTitle", "testBody", "123"), it.userId) }
                    .subscribeOn(Schedulers.io())
                    .subscribeBy(
                            onSuccess = { Timber.d("FCM TOKEN UPDATED") },
                            onError = { Timber.d("FCM UPDATE FAILURE") }
                    )
        } else {
            Timber.d("TOKENS NULL")
            outhNetworkManager.putOnAuthTaskOnQueue({
                outhNetworkManager.updateFcmToken(refreshedToken!!)
                        //.flatMap { updateAndGetCurrUserUseCase.execute() }
                        //.flatMap { outhNetworkManager.testPush(PushMessage("testTitle", "testBody", "123"), it.userId) }
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                                onSuccess = { Timber.d("FCM TOKEN UPDATED ON QUEUE") },
                                onError = { Timber.d("FCM UPDATE FAILURE ON QUEUE") }
                        )
            })

        }


    }
}

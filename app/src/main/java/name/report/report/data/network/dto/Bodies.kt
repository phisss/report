package name.report.report.data.network.dto

import com.google.gson.annotations.SerializedName

data class AuthBody(
        @SerializedName("userId")
        var userId: String,
        @SerializedName("password")
        var password: String)

data class PositionBody(
        @SerializedName("coordinates")
        val coordinates: List<Double>
)

data class RefreshFCMTokenBody(
        @SerializedName("refreshed_token")
        val refreshedToken: String
)

data class PushMessage(
        @SerializedName("title")
        val title: String,
        @SerializedName("body")
        val body: String,
        @SerializedName("dialogId")
        val dialogId: String
)
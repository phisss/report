package name.report.report.data.network.dto

import name.report.report.domain.ReportDialog
import name.report.report.domain.ReportRequest
import name.report.report.domain.ReportUser
import name.report.report.domain.UserPosition

data class GetRequestsResponse(var requests: List<ReportRequest>)

data class GetUsersPositionsResponse(var users: List<UserPosition>)

data class UserDialogsResponse(var dialogs: List<ReportDialog>, var count: Int)

data class CreateRequestResponse(var requestId: String)

data class PutMessageResponse(var messageId: String)

data class GetCurrUserInfoResponse(var user: ReportUser)

data class GetUserByIdResponse(var user: ReportUser)

class EmptyResponse()

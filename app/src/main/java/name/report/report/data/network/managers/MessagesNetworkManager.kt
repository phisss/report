package name.report.report.data.network.managers

import name.report.report.data.network.RetrofitBuilder
import name.report.report.data.network.api.MessageApi
import name.report.report.data.network.sockets.Socket
import name.report.report.domain.MessageEvent
import name.report.report.domain.ReportMessage
import name.report.report.domain.output.MessageStoreDomainOut.DialogType
import name.report.report.stores.output.MessagesNetworkStoreOut
import io.reactivex.Observable
import name.report.report.BuildConfig
import name.report.report.data.cache.TokensCache

class MessagesNetworkManager(private val tokensCache: TokensCache,
                             private val retrofitBuilder: RetrofitBuilder)
    : MessagesNetworkStoreOut {

    private fun messageApi() = retrofitBuilder.getMessagesApi()
    private lateinit var socket: Socket

    override fun sendMessage(requestId: String, message: ReportMessage)
            = messageApi().sendMessage(requestId, message).map { it.messageId }!!

    override fun requestDialog(requestId: String) = messageApi().getRequestDialog(requestId)

    override fun userDialogs(type: DialogType) = when (type) {
        DialogType.ALL -> messageApi().getAllUserDialogs()
        DialogType.OWNER -> messageApi().getDialogsByType(MessageApi.OWNER)
        DialogType.REPORTER -> messageApi().getDialogsByType(MessageApi.REPORTER)
        DialogType.JOINER -> messageApi().getDialogsByType(MessageApi.JOINER)
    }.map { it.dialogs }!!

    override fun events(): Observable<MessageEvent> {
        socket = Socket(BuildConfig.API_SERVER_ENDPOINT, tokensCache)
        socket.connect()
        return socket.events()
    }

    override fun hideEvents() { socket.disconnect() }

    override fun addRepoints(messageId: String, repoints: Int)
            = messageApi().addRepoints(messageId, repoints)

}

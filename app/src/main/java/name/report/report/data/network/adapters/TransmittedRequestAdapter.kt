package name.report.report.data.network.adapters

import name.report.report.domain.Point
import name.report.report.domain.Polygon
import name.report.report.domain.TransmittedReportRequest
import com.google.gson.*
import java.lang.reflect.Type

class TransmittedRequestAdapter: JsonSerializer<TransmittedReportRequest> {

    override fun serialize(src: TransmittedReportRequest,
                           typeOfSrc: Type?,
                           context: JsonSerializationContext?): JsonElement {
        val requestJson = JsonObject()
        requestJson.addProperty("message", src.message)
        requestJson.addProperty("place", src.place)
        requestJson.addProperty("timeToStayAlive", src.timeToStayAlive)
        requestJson.addProperty("description", src.description)

        val position = src.position
        val positionJson = when (position) {
            is Point -> {
                val json = JsonObject()
                json.addProperty("type", "Point")
                json.addProperty("coordinates",
                        Gson().toJson(listOf(position.coordinate.lat, position.coordinate.lng)))
                json
            }
            is Polygon -> {
                val json = JsonObject()
                json.addProperty("type", "Polygon")
                json.addProperty("coordinates",
                        Gson().toJson(listOf(position.coordinates.map { listOf(it.lat, it.lng) })))
                json
            }
        }

        requestJson.add("position", positionJson)
        return requestJson
    }
}

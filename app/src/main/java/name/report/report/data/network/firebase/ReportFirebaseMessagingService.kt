package name.report.report.data.network.firebase

import android.app.NotificationManager
import android.app.PendingIntent
import com.google.firebase.messaging.FirebaseMessagingService
import timber.log.Timber
import com.google.firebase.messaging.RemoteMessage
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import name.report.report.MainActivity
import name.report.report.R
import name.report.report.chat.DefaultMessagesActivity


class ReportFirebaseMessagingService : FirebaseMessagingService() {

    companion object {
        const val DIALOG_ID_KEY = "DIALOG_ID"
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        Timber.d("From: " + remoteMessage!!.from!!)
        // ...

        val builder = NotificationCompat.Builder(this)
                .setSmallIcon(name.report.report.R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle(remoteMessage!!.notification!!.title)
                .setContentText(remoteMessage.notification!!.body)

        val resultIntent = Intent(this, DefaultMessagesActivity::class.java)
        resultIntent.putExtra(DefaultMessagesActivity.DIALOG_ID_KEY, remoteMessage.data["requestId"])
        resultIntent.putExtra(DefaultMessagesActivity.TITLE_KEY, remoteMessage.data["title"])

        val stackBuilder = TaskStackBuilder.create(this)
        stackBuilder.addParentStack(MainActivity::class.java)
        stackBuilder.addNextIntent(resultIntent)
        val resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        )
        builder.setContentIntent(resultPendingIntent)
        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.notify(1212, builder.build())


        // Check if message contains a data payload.
        if (remoteMessage.data.size > 0) {
            Timber.d("Message data payload: " + remoteMessage.data)
        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Timber.d("Message Notification Body: " + remoteMessage.notification!!.body!!)
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

}

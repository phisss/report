package name.report.report.data.cache

import android.content.SharedPreferences
import com.google.gson.Gson
import name.report.report.domain.ReportUser
import name.report.report.domain.Tokens
import name.report.report.domain.UserAuth
import name.report.report.stores.output.ProfileCacheStoreOut
import name.report.report.stores.output.TokensCacheStoreOut
import name.report.report.utils.getAndDeserialize
import name.report.report.utils.serializeAndPut

class AppPreferences(private val prefs: SharedPreferences)
    : TokensCacheStoreOut, TokensCache, ProfileCacheStoreOut {

    override fun setFcm(token: String) = prefs.edit().putString(FCM_KEY, token).apply()

    override fun getFcm(): String? = prefs.getString(FCM_KEY, null)

    companion object {
        const val TOKENS_KEY = "tokens_key"
        const val USER_KEY = "user_key"
        const val PHOTO_KEY = "photo_key"
        const val NAME_KEY = "name_key"
        const val CURR_USER_KEY = "curr_user_key"
        const val FIRST_LAUNCH = "ONBOARDING"
        const val GUIDE_SHOWN = "guide_shown"
        const val FCM_KEY = "fcm_key"
        const val FIRST_TAB_CLICKED = "first_tab_clicked"
        const val SECOND_TAB_CLICKED = "second_tab_clicked"
        const val THIRD_TAB_CLICKED = "third_tab_clicked"
        const val TIP_IN_CHAT_SHOWN = "tip_in_chat_shown"
    }

    override fun getAuth() = prefs.getAndDeserialize(USER_KEY, UserAuth::class.java)

    override fun setAuth(user: UserAuth) = prefs.serializeAndPut(USER_KEY, user)

    override fun setTokens(tokens: Tokens) {
        prefs.edit().putString(TOKENS_KEY, Gson().toJson(tokens)).commit()
        val new = prefs.getString(TOKENS_KEY, null)
        if (new != null) {
            
        }
    }

    override fun getTokens() = prefs.getAndDeserialize(TOKENS_KEY, Tokens::class.java)

    override fun getName() = prefs.getString(NAME_KEY, null)

    override fun setName(name: String) {
        prefs.edit().putString(NAME_KEY, name).apply()
    }
    override fun setUrl(url: String) {
        prefs.edit().putString(PHOTO_KEY, url).apply()
    }

    override fun getUrl() = prefs.getString(PHOTO_KEY, null)

    override fun setCurrUser(user: ReportUser) = prefs.serializeAndPut(CURR_USER_KEY, user)

    override fun getCurrUser() = prefs.getAndDeserialize(CURR_USER_KEY, ReportUser::class.java)

    fun guideShown() = prefs.getBoolean(GUIDE_SHOWN, false)

    fun setGuideShown() = prefs.edit().putBoolean(GUIDE_SHOWN, true).apply()

    private fun setFirstTabClicked() = prefs.edit().putBoolean(FIRST_TAB_CLICKED, true).apply()

    private fun setSecondTabClicked() = prefs.edit().putBoolean(SECOND_TAB_CLICKED, true).apply()

    private fun setThirdTabClicked() = prefs.edit().putBoolean(THIRD_TAB_CLICKED, true).apply()

    private fun firstTabClicked() = prefs.getBoolean(FIRST_TAB_CLICKED, false)

    private fun secondTabClicked() = prefs.getBoolean(SECOND_TAB_CLICKED, false)

    private fun thirdTabClicked() = prefs.getBoolean(THIRD_TAB_CLICKED, false)

    fun tabClicked(position: Int) =
        when(position){
            0 -> firstTabClicked()
            1 -> secondTabClicked()
            2 -> thirdTabClicked()
            else -> false
        }

    fun setTabClicked(position: Int){
        when(position){
            0 -> setFirstTabClicked()
            1 -> setSecondTabClicked()
            2 -> setThirdTabClicked()
        }
    }

    fun setTabsClicked(){
        prefs.edit().putBoolean(FIRST_TAB_CLICKED, false).apply()
        prefs.edit().putBoolean(SECOND_TAB_CLICKED, false).apply()
        prefs.edit().putBoolean(THIRD_TAB_CLICKED, false).apply()
    }

    fun tipInChatShown() = prefs.getBoolean(TIP_IN_CHAT_SHOWN, false)

    fun setTipInChatShown(){
        prefs.edit().putBoolean(TIP_IN_CHAT_SHOWN, true).apply()
    }
}

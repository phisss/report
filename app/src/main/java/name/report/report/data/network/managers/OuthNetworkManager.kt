package name.report.report.data.network.managers

import name.report.report.data.network.RetrofitBuilder
import name.report.report.data.network.dto.AuthBody
import name.report.report.data.network.dto.RefreshFCMTokenBody
import name.report.report.data.network.dto.PushMessage
import name.report.report.domain.TokenType
import name.report.report.stores.output.AuthNetworkStoreOut
import name.report.report.utils.pollAll
import io.reactivex.schedulers.Schedulers
import name.report.report.data.network.api.OuthApi
import name.report.report.stores.output.ProfileCacheStoreOut
import java.util.*

class OuthNetworkManager(private val retrofitBuilder: RetrofitBuilder,
                         private val cacheStoreOut: ProfileCacheStoreOut) : AuthNetworkStoreOut {

    private var currToken: String? = null

    private val authApi = retrofitBuilder.getOuthApi()

    private lateinit var signedAuthApi: OuthApi

    private fun getSignedAuthApi() = when {
        currToken == null -> {
            signedAuthApi = retrofitBuilder.getSignedOuthApi()
            signedAuthApi
        }
        isTokenChanged() -> {
            currToken = cacheStoreOut.getTokens()!!.accessToken
            retrofitBuilder.getSignedOuthApi()
        }
        else -> signedAuthApi
    }


    private var task: (() -> Unit)? = null

    override fun getTokens(userId: String, password: String)
            = authApi.signInAnonymously(AuthBody(userId, password))
            .doOnSuccess { it.type = TokenType.DEMO }
            .doOnSuccess { cacheStoreOut.setTokens(it) }
            .doOnSuccess {
                if (task != null) {
                    task?.invoke()
                    task = null
                }
            }

    override fun updateFcmToken(token: String) = getSignedAuthApi().refreshFcmToken(RefreshFCMTokenBody(token))

    fun putOnAuthTaskOnQueue(listener: () -> Unit) {
        task = listener
    }

    fun testPush(pushMessage: PushMessage, userId: String) = signedAuthApi.testPush(pushMessage, userId).subscribeOn(Schedulers.io())

    fun isTokenChanged() = if (currToken == cacheStoreOut.getTokens()!!.accessToken) {
        false
    } else {
        currToken = cacheStoreOut.getTokens()!!.accessToken
        true
    }
}

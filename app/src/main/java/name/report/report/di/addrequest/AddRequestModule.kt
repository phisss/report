package name.report.report.di.addrequest

import name.report.report.domain.output.RequestStoreDomainOut
import name.report.report.domain.usecases.requests.CreateRequestUseCase
import dagger.Module
import dagger.Provides

/**
 * Created by Hero_in_heaven on 01.12.2017.
 */
@Module
class AddRequestModule {
    @Provides
    @AddRequestScope
    fun provideCreateRequestUseCase(requestStoreDomainOut: RequestStoreDomainOut)
            = CreateRequestUseCase(requestStoreDomainOut)


}
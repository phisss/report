package name.report.report.di.feed

import android.arch.lifecycle.MutableLiveData
import name.report.report.domain.output.*
import name.report.report.domain.usecases.auth.GetUserAuthUseCase
import name.report.report.domain.usecases.auth.SetTokensUseCase
import name.report.report.domain.usecases.messages.*
import name.report.report.domain.usecases.position.GetUsersPositionsByRadiusUseCase
import name.report.report.domain.usecases.position.SetCurrentUserPositionUseCase
import name.report.report.domain.usecases.requests.*
import name.report.report.presentation.feed.view.FeedMapper
import name.report.report.presentation.feed.viewmodel.RequestLiveData
import dagger.Module
import dagger.Provides
import name.report.report.domain.usecases.users.GetUserByIdUseCase
import javax.inject.Singleton

@Module
class FeedModule {

    @Provides
    @FeedScope
    fun provideGetRequestsInRadiusUseCase(requestStoreDomainOut: RequestStoreDomainOut)
            = GetRequestsInRadiusUseCase(requestStoreDomainOut)

    @Provides
    @FeedScope
    fun provideGetRequestsInRadiusRemoteUseCase(requestStoreDomainOut: RequestStoreDomainOut)
            = GetRequestsInRadiusRemoteUseCase(requestStoreDomainOut)


    @Provides
    @FeedScope
    fun provideUpdateRequestsInRadiusUseCase(requestStoreDomainOut: RequestStoreDomainOut)
            = UpdateRequestsInRadiusUseCase(requestStoreDomainOut)

    @Provides
    @FeedScope
    fun provideRequestMapper() = FeedMapper()

    @Provides
    @FeedScope
    fun provideRequestLiveData(getRequestsInRadiusRemoteUseCase: GetRequestsInRadiusRemoteUseCase)
            = RequestLiveData(getRequestsInRadiusRemoteUseCase)

    @Provides
    @FeedScope
    fun provideCreateRequestUseCase(requestStoreDomainOut: RequestStoreDomainOut)
            = CreateRequestUseCase(requestStoreDomainOut)


    @Provides
    @FeedScope
    fun provideSetTokensUseCase(out: OuthDomainOut)
            = SetTokensUseCase(out)


    @Provides
    @FeedScope
    fun provideGetUserAuthUseCase(out: OuthDomainOut)
            = GetUserAuthUseCase(out)

    @Provides
    @FeedScope
    fun provideGetUsersPositionsByRadiusUseCase(positionStoreDomainOut: PositionStoreDomainOut)
            = GetUsersPositionsByRadiusUseCase(positionStoreDomainOut)

    @Provides
    @FeedScope
    fun provideGetCurrentUserRequestsRemoteUseCase(requestStoreDomainOut: RequestStoreDomainOut)
            = GetCurrentUserRequestsRemoteUseCase(requestStoreDomainOut)

    @Provides
    @FeedScope
    fun provideDeleteRequestUseCase(requestStoreDomainOut: RequestStoreDomainOut)
            = DeleteRequestUseCase(requestStoreDomainOut)

    @Provides
    @FeedScope
    fun provideSendMessageUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = SendMessageUseCase(messageStoreDomainOut)


    @Provides
    @FeedScope
    fun provideGetEventsUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = GetEventsUseCase(messageStoreDomainOut)

    @Provides
    @FeedScope
    fun provideGetFollowDialogsUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = GetJoinerUserDialogsUseCase(messageStoreDomainOut)

    @Provides
    @FeedScope
    fun provideGetDialogsToYouUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = GetReporterUserDialogsUseCase(messageStoreDomainOut)

    @Provides
    @FeedScope
    fun provideJoinUseCase(requestStoreDomainOut: RequestStoreDomainOut)
            = JoinUseCase(requestStoreDomainOut)
    

    @Provides
    @FeedScope
    fun provideLeaveUseCase(requestStoreDomainOut: RequestStoreDomainOut)
            = LeaveUseCase(requestStoreDomainOut)

    @Provides
    @FeedScope
    fun provideCreateRequestToUserUseCase(requestStoreDomainOut: RequestStoreDomainOut)
            = CreateRequestToUserUseCase(requestStoreDomainOut)

    @Provides
    @FeedScope
    fun provideAddRepointsUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = AddRepointsUseCase(messageStoreDomainOut)

    @Provides
    @FeedScope
    fun provideGetUserByIdUseCase(usersStoreDomainOut: UsersStoreDomainOut)
            = GetUserByIdUseCase(usersStoreDomainOut)

}

package name.report.report.di.feed

import name.report.report.chat.IncomingImageMessageViewHolder
import name.report.report.chat.OutcomingImageMessageViewHolder
import name.report.report.presentation.feed.view.FeedFragment
import name.report.report.presentation.main.viewmodel.MainActivityViewModel
import name.report.report.presentation.slider.view.SliderViewModel
import dagger.Subcomponent
import name.report.report.chat.DefaultMessagesActivity

@FeedScope
@Subcomponent (modules = arrayOf(FeedModule::class))
interface FeedComponent {
    fun inject(feedFragment: FeedFragment)
    fun inject(mainActivityViewModel: MainActivityViewModel)
    fun inject(sliderViewModel: SliderViewModel)
    fun inject(viewHolder: IncomingImageMessageViewHolder)
    fun inject(viewHolder: OutcomingImageMessageViewHolder)
}
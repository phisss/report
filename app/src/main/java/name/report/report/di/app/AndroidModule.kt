package name.report.report.di.app

import android.content.Context
import dagger.Module
import dagger.Provides
import name.report.report.data.os.AndroidResourses
import name.report.report.domain.output.AndroidResourcesDomainOut
import name.report.report.stores.output.MacAddressStoreOut
import javax.inject.Singleton

@Module
class AndroidModule {

    @Singleton
    @Provides
    fun provideAndroidResources(context: Context) = AndroidResourses(context)

    @Singleton
    @Provides
    fun provideMacAddressStoreOut(androidResources: AndroidResourses)
            : MacAddressStoreOut = androidResources

}

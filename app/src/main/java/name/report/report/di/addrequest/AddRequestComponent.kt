package name.report.report.di.addrequest

import name.report.report.presentation.addingsheet.view.AddRequestFragment
import name.report.report.presentation.addingsheet.viewmodel.AddRequestViewModel
import dagger.Subcomponent

/**
 * Created by Hero_in_heaven on 01.12.2017.
 */
@AddRequestScope
@Subcomponent(modules = arrayOf(AddRequestModule::class))
interface AddRequestComponent {
    fun inject(addRequestViewModel: AddRequestViewModel)
    fun inject(addRequestFragment:  AddRequestFragment)
}
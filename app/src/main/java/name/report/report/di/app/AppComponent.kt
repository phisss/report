package name.report.report.di.app

import name.report.report.MainActivity
import name.report.report.chat.DefaultMessagesActivity
import name.report.report.data.network.firebase.ReportFirebaseInstanceIDService
import name.report.report.di.addrequest.AddRequestComponent
import name.report.report.di.feed.FeedComponent
import dagger.Component
import name.report.report.SplashScreen
import name.report.report.chat.DefaultMessagesActivityInfo
import name.report.report.data.trackLocation.LocationJobService
import name.report.report.data.trackLocation.LocationService
import name.report.report.presentation.intro.IntroActivity
import name.report.report.presentation.map.view.MapFragment
import name.report.report.presentation.wallet.WalletActivity
import name.report.report.registration.FbFragment
import name.report.report.registration.VkFragment
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        AppModule::class,
        NetworkModule::class,
        CacheModule::class,
        StoreModule::class,
        UseCasesModule::class,
        AndroidModule::class
))
interface AppComponent {

    fun inject(locationJobService: LocationJobService)

    fun inject(mainActivity: MainActivity)

    fun inject(splashScreen: SplashScreen)

    fun inject(defaultMessagesActivity: DefaultMessagesActivity)

    fun inject(defaultMessagesActivity: DefaultMessagesActivityInfo)

    fun inject(reportFirebaseInstanceIDService: ReportFirebaseInstanceIDService)

    fun inject(mapFragment: MapFragment)

    fun inject(locationService: LocationService)

    fun inject(vkFragment: VkFragment)

    fun inject(fbFragment: FbFragment)

    fun plusFeedComponent(): FeedComponent

    fun plusAddRequestComponent(): AddRequestComponent

}

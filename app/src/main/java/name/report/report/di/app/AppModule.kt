package name.report.report.di.app

import android.content.Context
import name.report.report.domain.output.OuthDomainOut
import name.report.report.domain.usecases.auth.GetTokensUseCase
import dagger.Module
import dagger.Provides
import name.report.report.di.feed.FeedScope
import name.report.report.domain.output.PositionStoreDomainOut
import name.report.report.domain.usecases.auth.GetMacAddressUseCase
import name.report.report.domain.usecases.position.SetCurrentUserPositionUseCase
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext() = context

    @Provides
    @Singleton
    fun provideGetTokensUseCase(outhManager: OuthDomainOut, getMacAddressUseCase: GetMacAddressUseCase)
            = GetTokensUseCase(outhManager, getMacAddressUseCase)

    @Provides
    @Singleton
    fun provideSetCurrentUserPositionUseCase(positionStoreDomainOut: PositionStoreDomainOut)
            = SetCurrentUserPositionUseCase(positionStoreDomainOut)

}

package name.report.report.di.app

import dagger.Module
import dagger.Provides
import name.report.report.domain.output.AndroidResourcesDomainOut
import name.report.report.domain.output.MessageStoreDomainOut
import name.report.report.domain.output.OuthDomainOut
import name.report.report.domain.usecases.auth.GetFcmUseCase
import name.report.report.domain.usecases.auth.GetMacAddressUseCase
import name.report.report.domain.usecases.auth.SetFcmUseCase
import name.report.report.domain.usecases.auth.UpdateFcmIfExistsUseCase
import name.report.report.domain.usecases.messages.GetRequestDialogUseCase
import javax.inject.Singleton

@Module
class UseCasesModule {

    @Singleton
    @Provides
    fun provideSetFcmUseCase(outhDomainOut: OuthDomainOut) = SetFcmUseCase(outhDomainOut)

    @Singleton
    @Provides
    fun provideGetFcmUseCase(outhDomainOut: OuthDomainOut) = GetFcmUseCase(outhDomainOut)

    @Singleton
    @Provides
    fun provideUpdateFcmIfExistsUseCase(getFcmUseCase: GetFcmUseCase, outhDomainOut: OuthDomainOut)
            = UpdateFcmIfExistsUseCase(getFcmUseCase, outhDomainOut)

    @Provides
    @Singleton
    fun provideGetRequestDialogUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = GetRequestDialogUseCase(messageStoreDomainOut)

    @Provides
    @Singleton
    fun provideGetMacAddressUseCase(androidResourcesDomainOut: AndroidResourcesDomainOut)
            = GetMacAddressUseCase(androidResourcesDomainOut)

}

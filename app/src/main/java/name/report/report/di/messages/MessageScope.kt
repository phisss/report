package name.report.report.di.messages

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MessagesScope
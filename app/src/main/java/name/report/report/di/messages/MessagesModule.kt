package name.report.report.di.messages

import name.report.report.domain.output.MessageStoreDomainOut
import name.report.report.domain.usecases.messages.*
import dagger.Module
import dagger.Provides

@Module
class MessagesModule {

    @Provides
    @MessagesScope
    fun provideGetEventsUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = GetEventsUseCase(messageStoreDomainOut)

    @Provides
    @MessagesScope
    fun provideGetAllUserDialogsUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = GetAllUserDialogsUseCase(messageStoreDomainOut)

    @Provides
    @MessagesScope
    fun provideGetOwnerUserDialogsUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = GetOwnerUserDialogsUseCase(messageStoreDomainOut)

    @Provides
    @MessagesScope
    fun provideGetReporterUserDialogsUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = GetReporterUserDialogsUseCase(messageStoreDomainOut)

    @Provides
    @MessagesScope
    fun provideGetJoinerUserDialogsUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = GetJoinerUserDialogsUseCase(messageStoreDomainOut)

    @Provides
    @MessagesScope
    fun provideHideEventsUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = HideEventsUseCase(messageStoreDomainOut)

    @Provides
    @MessagesScope
    fun provideSendMessageUseCase(messageStoreDomainOut: MessageStoreDomainOut)
            = GetRequestDialogUseCase(messageStoreDomainOut)



}

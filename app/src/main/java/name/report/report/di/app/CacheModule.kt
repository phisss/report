package name.report.report.di.app

import android.arch.persistence.room.Room
import android.content.Context
import android.content.SharedPreferences
import name.report.report.BuildConfig
import name.report.report.data.cache.AppDataBase
import name.report.report.data.cache.AppPreferences
import name.report.report.data.cache.TokensCache
import name.report.report.stores.output.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CacheModule {

    @Provides
    fun provideSharedPreferences(context: Context)
            = context.getSharedPreferences(name.report.report.BuildConfig.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)!!

    @Provides
    fun provideAppPreferences(prefs: SharedPreferences) = AppPreferences(prefs)

    @Provides
    fun provideTokensCache(prefs: SharedPreferences): TokensCache
            = AppPreferences(prefs)

    @Provides
    @Singleton
    fun provideDataBase(context: Context)
            = Room.databaseBuilder(context, AppDataBase::class.java, name.report.report.BuildConfig.ROOM_NAME).build()


    //Store

    @Provides
    @Singleton
    fun provideCoordinateDaoStoreOut(dataBase: AppDataBase): CoordinateDaoStoreOut
            = dataBase.coordinateDaoRepoOut()

    @Provides
    @Singleton
    fun provideRequestDaoStoreOut(dataBase: AppDataBase): RequestDaoStoreOut
            = dataBase.requestDaoRepoOut()

    @Provides
    @Singleton
    fun provideUserDaoStoreOut(dataBase: AppDataBase): UserDaoStoreOut
//            = dataBase.userDaoRepoOut()
            = object : UserDaoStoreOut {}

    @Provides
    @Singleton
    fun provideTokensCacheStoreOut(prefs: SharedPreferences): TokensCacheStoreOut
            = AppPreferences(prefs)

    @Provides
    @Singleton
    fun provideProfileCacheStoreOut(prefs: SharedPreferences): ProfileCacheStoreOut
            = AppPreferences(prefs)

}

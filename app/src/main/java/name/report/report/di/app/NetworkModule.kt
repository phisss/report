package name.report.report.di.app

import name.report.report.BuildConfig
import name.report.report.data.cache.TokensCache
import name.report.report.data.network.RetrofitBuilder
import name.report.report.data.network.firebase.FirebaseFileUploader
import name.report.report.data.network.managers.*
import name.report.report.data.network.sockets.Socket
import name.report.report.stores.output.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideSocket(tokensCache: TokensCache)
            = Socket(name.report.report.BuildConfig.API_SERVER_ENDPOINT, tokensCache)

    @Provides
    @Singleton
    fun provideRetrofitBuilder(tokensCache: TokensCache) = RetrofitBuilder(tokensCache)

    @Provides
    fun provideRequestNetworkStoreOut(retrofitBuilder: RetrofitBuilder): RequestNetworkStoreOut
            = ReportRequestsNetworkManager(retrofitBuilder)

    @Provides
    fun providePositionNetworkStoreOut(retrofitBuilder: RetrofitBuilder): PositionNetworkStoreOut
            = PositionNetworkManager(retrofitBuilder)

    @Provides
    @Singleton
    fun provideOuthNetworkManager(retrofitBuilder: RetrofitBuilder, cacheStoreOut: ProfileCacheStoreOut)
            = OuthNetworkManager(retrofitBuilder, cacheStoreOut)

    @Provides
    fun provideOuthNetworkStoreOut(outhNetworkManager: OuthNetworkManager): AuthNetworkStoreOut
            = outhNetworkManager

    @Provides
    fun provideMessagesNetworkStoreOut(tokensCache: TokensCache, retrofitBuilder: RetrofitBuilder): MessagesNetworkStoreOut
            = MessagesNetworkManager(tokensCache, retrofitBuilder)

    @Provides
    fun provideUsersNetworkStoreOut(retrofitBuilder: RetrofitBuilder): UsersNetworkStoreOut
            = UsersNetworkManager(retrofitBuilder)

    @Provides
    @Singleton
    fun provideMediaUploader(): MediaUploader = FirebaseFileUploader()
}

package name.report.report.di.app

import name.report.report.domain.output.*
import name.report.report.domain.usecases.users.GetCurrUserUseCase
import name.report.report.domain.usecases.users.UpdateAndGetCurrUserUseCase
import name.report.report.domain.usecases.position.GetUserPositionById
import name.report.report.stores.*
import name.report.report.stores.output.*
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import name.report.report.domain.usecases.users.GetUserByIdUseCase
import javax.inject.Named
import javax.inject.Singleton

@Module
class StoreModule {

    companion object {
        private const val IO_SCHEDULER = "io_scheduler"
        private const val UI_SCHEDULER = "ui_scheduler"
    }

    @Provides
    @Named(IO_SCHEDULER)
    fun provideSchedulerIo() = Schedulers.io()

    @Provides
    @Named(UI_SCHEDULER)
    fun provideSchedulerUi() = AndroidSchedulers.mainThread()!!


    @Provides
    @Singleton
    fun provideRequestStore(requestNetwork: RequestNetworkStoreOut,
                            coordinateDao: CoordinateDaoStoreOut,
                            requestDao: RequestDaoStoreOut,
                            userDao: UserDaoStoreOut,
                            @Named(IO_SCHEDULER) ioScheduler: Scheduler,
                            @Named(UI_SCHEDULER) uiScheduler: Scheduler)
            = RequestStore(requestDao, coordinateDao, userDao, requestNetwork, ioScheduler, uiScheduler)

    @Provides
    @Singleton
    fun provideMessageStore(messagesNetworkStoreOut: MessagesNetworkStoreOut,
                             @Named(IO_SCHEDULER) ioScheduler: Scheduler,
                             @Named(UI_SCHEDULER) uiScheduler: Scheduler)
            = MessageStore(messagesNetworkStoreOut, ioScheduler, uiScheduler)

    @Provides
    @Singleton
    fun providePositionStore(positionNetworkStoreOut: PositionNetworkStoreOut,
                             @Named(IO_SCHEDULER) ioScheduler: Scheduler,
                             @Named(UI_SCHEDULER) uiScheduler: Scheduler)
            = PositionStore(positionNetworkStoreOut, ioScheduler, uiScheduler)

    @Provides
    @Singleton
    fun provideTokensStore(authNetworkStoreOut: AuthNetworkStoreOut,
                           profileCacheStoreOut: ProfileCacheStoreOut,
                           @Named(UI_SCHEDULER) uiScheduler: Scheduler,
                           @Named(IO_SCHEDULER) ioScheduler: Scheduler)
            = TokensStore(authNetworkStoreOut, profileCacheStoreOut, uiScheduler, ioScheduler)

    @Provides
    @Singleton
    fun provideUsersStore(usersNetworkStoreOut: UsersNetworkStoreOut,
                          profileCacheStoreOut: ProfileCacheStoreOut,
                          @Named(IO_SCHEDULER) ioScheduler: Scheduler,
                          @Named(UI_SCHEDULER) uiScheduler: Scheduler)
            = UsersStore(usersNetworkStoreOut, profileCacheStoreOut, ioScheduler, uiScheduler)

    @Provides
    @Singleton
    fun provideAndroidStore(macAddressStoreOut: MacAddressStoreOut)
            = AndroidStore(macAddressStoreOut)

    @Provides
    fun provideAndroidResourcesDomainOut(androidStore: AndroidStore)
            : AndroidResourcesDomainOut = androidStore

    @Provides
    fun provideRequestDomainOut(requestStore: RequestStore): RequestStoreDomainOut = requestStore

    @Provides
    fun providePositionStoreDomainOut(positionStore: PositionStore): PositionStoreDomainOut = positionStore

    @Provides
    fun provideOuthDomainOut(tokensStore: TokensStore): OuthDomainOut = tokensStore

    @Provides
    fun provideMessageStoreDomainOut(messageStore: MessageStore): MessageStoreDomainOut = messageStore

    @Provides
    fun provideUserStoreDomainOut(usersStore: UsersStore): UsersStoreDomainOut = usersStore

    @Provides
    fun provideGetUserPositionByIdUseCase(positionStoreDomainOut: PositionStoreDomainOut)
            = GetUserPositionById(positionStoreDomainOut)

    @Provides
    @Singleton
    fun provideGetCurrUserUseCase(usersStoreDomainOut: UsersStoreDomainOut)
            = GetCurrUserUseCase(usersStoreDomainOut)

    @Provides
    @Singleton
    fun provideUpdateAndGetCurrUserUseCase(usersStoreDomainOut: UsersStoreDomainOut)
            = UpdateAndGetCurrUserUseCase(usersStoreDomainOut)

}

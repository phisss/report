package name.report.report.di.addrequest

import javax.inject.Scope

/**
 * Created by Hero_in_heaven on 01.12.2017.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AddRequestScope
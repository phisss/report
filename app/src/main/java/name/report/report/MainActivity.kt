@file:Suppress("DEPRECATION")

package name.report.report

import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearSnapHelper
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import name.report.report.presentation.map.view.MapFragment
import name.report.report.presentation.slider.view.SliderFragment
import name.report.report.utils.*
import name.sothree.slidinguppanel.SlidingUpPanelLayout
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.adding_bottom_sheet.*
import kotlinx.android.synthetic.main.main_content.*
import kotlinx.android.synthetic.main.main_content_bs.*
import kotlinx.android.synthetic.main.slider_content.*
import kotlinx.android.synthetic.main.slider_main.*
import timber.log.Timber
import javax.inject.Inject
import name.report.report.presentation.main.viewmodel.MainActivityViewModel
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import name.report.report.domain.TokenType
import name.report.report.domain.usecases.auth.GetTokensUseCase
import name.report.report.presentation.market.MarketActivity
import name.report.report.presentation.search.SearchFragment
import name.report.report.presentation.searchplace.PlaceAutocompleteAdapter
import name.report.report.presentation.wallet.WalletActivity
import name.report.report.registration.FbFragment
import name.report.report.registration.VkFragment
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.places.Places
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.michael.easydialog.EasyDialog
import kotlinx.android.synthetic.main.fragment_search_place.*
import java.util.ArrayList
import de.hdodenhof.circleimageview.CircleImageView
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.adding_fragment.*
import kotlinx.android.synthetic.main.alert_photo.view.*
import kotlinx.android.synthetic.main.custom_tab.view.*
import kotlinx.android.synthetic.main.delete_alert.view.*
import kotlinx.android.synthetic.main.fragment_tabs.*
import kotlinx.android.synthetic.main.send_request_fragment.*
import kotlinx.android.synthetic.main.send_request_to_user_bs.*
import kotlinx.android.synthetic.main.user_menu_bs.*
import name.report.report.chat.bottomsheetfragments.BottomSheetFragment
import name.report.report.data.cache.AppPreferences
import name.report.report.presentation.addingsheet.view.AddRequestFragment
import name.report.report.presentation.alert.view.BottomSheetFragmentInAlert
import name.report.report.presentation.sendrequest.SendFragment


class MainActivity : AppCompatActivity(),
        NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        PlaceAutocompleteAdapter.PlaceAutoCompleteInterface {

    @Inject
    lateinit var appPreferences: AppPreferences

    @Inject
    lateinit var getTokensUseCase: GetTokensUseCase
    private val fragment = MapFragment()
    private var mPreviousSliderState = name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.COLLAPSED
    private var onRegistration = false
    private var regFragment: android.support.v4.app.Fragment? = null

    private val viewModel: MainActivityViewModel by lazy { ViewModelProviders.of(this).get(MainActivityViewModel::class.java) }

    private var fromSearch = false
    private var previousSliderState = name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.COLLAPSED

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<*>
    private lateinit var bottomSheetBehavior2: BottomSheetBehavior<*>

    internal var mGoogleApiClient: GoogleApiClient? = null

    internal var mAdapter: PlaceAutocompleteAdapter? = null
    private var BOUNDS = LatLngBounds(
            LatLng(-0.0, 0.0), LatLng(0.0, 0.0))

    companion object {
        val instance
            get() = MainActivity()
        var zoom = false
        var register = false
    }

    public override fun onStart() {
        mGoogleApiClient!!.connect()
        super.onStart()

    }

    public override fun onStop() {
        mGoogleApiClient!!.disconnect()
        super.onStop()
    }

    var photoUrl = ""

    var userName = ""

    var rep = ""

    var numOfRep = 0

    var size = 5

    private fun setAddBSState(state: Int){
        bottomSheetBehavior.state = state
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Fabric.with(this, Crashlytics())

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build()

        mAdapter = PlaceAutocompleteAdapter(this,
                name.report.report.R.layout.view_placesearch,
                mGoogleApiClient!!,
                BOUNDS,
                null)

        checkGuide()

        //Status bar and action bar settings
        supportActionBar?.hide()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        bottomSlider.layoutParams.height = resources.getHeight() * 2 - resources.getStatusBarHeight()
        registrationFragment.layoutParams.height = resources.getHeight() * 2 - resources.getStatusBarHeight()
        registrationFragment.visible()
        leftMenuButtonBelowStatusBar()
        myLocationBelowStatusBar()
        slidingLayout.addPanelSlideListener(object : name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener {
            override fun onPanelSlide(panel: View, slideOffset: Float) {
                if (slideOffset <= 0.5f)
                    setFabMargins((slideOffset * 180).toInt())
            }

            override fun onPanelStateChanged(panel: View, previousState: name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState, newState: name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState) {
                when (newState) {
                    SlidingUpPanelLayout.PanelState.ANCHORED -> {
                        viewModel.sliderMoved.value = true
                        if (fragment.getPP() && mPreviousSliderState != name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.ANCHORED && !fromSearch) {
                            fragment.setMapMoving(true)
                            fragment.zoomToCoordinates(fragment.getZoom()!!, fragment.getCenterPos(), 800)
                            mPreviousSliderState = name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.ANCHORED
                        }
                        setFabMargins(72)
                        fromSearch = false
                        if(!appPreferences.guideShown()) {
                            slidingLayout.panelState = SlidingUpPanelLayout.PanelState.ANCHORED
                        }
                    }
                    SlidingUpPanelLayout.PanelState.COLLAPSED -> {
                        viewModel.sliderMoved.value = true
                        if (mPreviousSliderState != name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.COLLAPSED) {
                            fragment.setMapMoving(false)
                            if (!viewModel.movingFromAlert.value!! && !fromSearch)
                                fragment.zoomToCoordinates(fragment.getZoom()!!, fragment.getCenterPos(), 800)
                            fragment.setPP(true)
                            mPreviousSliderState = name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.COLLAPSED
                        }
                        fromSearch = false
                        if(!appPreferences.guideShown()) {
                            slidingLayout.panelState = SlidingUpPanelLayout.PanelState.ANCHORED
                        }
                    }
                    SlidingUpPanelLayout.PanelState.EXPANDED -> {
                        setFabMargins(72)
                        fragment.setPP(false)
                        mPreviousSliderState = name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.EXPANDED
                        if(!appPreferences.guideShown()) {
                            slidingLayout.panelState = SlidingUpPanelLayout.PanelState.ANCHORED
                        }
                    }
                    else -> {
                    }
                }
            }
        })

        //Add request fab init
        fabAddRequest.setOnClickListener{
            if(appPreferences.guideShown()){
                viewModel.getTokensUseCase.execute().subscribeBy(
                        onSuccess = {
                            if(it.type==TokenType.DEMO){
                                showRegisterAlert()
                            }else{
                                setUiToAdding()
                                viewModel.addRequest.value = true
                            }
                        },
                        onError = {}
                )
            }
        }
        bottomClouds.setOnClickListener({
            Timber.e("RE::")
        })

        //Close request adding fab init
        closeFabAdding.setOnClickListener({
            resetUI()
        })

        //Replace fragments
        getTokensUseCase.execute().subscribeBy(
                onSuccess = {
                    supportFragmentManager.transaction {
                        replace(R.id.flSliderContainer, SliderFragment.instance)
                        replace(R.id.map, fragment)
                    }
                },
                onError = { Timber.d(it.toString()) }
        )
        supportFragmentManager.transaction{
            replace(R.id.addingFragment, AddRequestFragment())
            replace(R.id.sendRequestFragment, SendFragment())
        }
        viewModel.sliderMoved.value = false

        BottomSheetFragmentInAlert.sendRequest.observe(this, Observer {
            if(BottomSheetFragmentInAlert.sendRequest.value!=null)
                BottomSheetFragmentInAlert().show(supportFragmentManager, BottomSheetFragmentInAlert().tag)
        })

        searchField.setOnClickListener({
            fromSearch = true
            previousSliderState = slidingLayout.panelState
            searchField.isCursorVisible = true
            slidingLayout.panelState = name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.EXPANDED
            supportFragmentManager.transaction {
                replace(name.report.report.R.id.flSliderContainer, SearchFragment())
            }
            searchClose.visible()
            fabAddRequest.gone()
        })

        searchClose.setOnClickListener({
            closeSearch()
        })

        findPlaceBtn.setOnClickListener{
            showSearch()
        }

        sendRequest.setOnClickListener({
            viewModel.sendRequestLiveData.value = true
            return@setOnClickListener
        })

        searchField.isCursorVisible = false
        viewModel.adapter.value = mAdapter
        viewModel.searchCreated.observe(this, Observer {
            initViews(it!!)
        })
        viewModel.getTokensUseCase.execute().subscribeBy(onSuccess = {
                            when (it.type) {
                                TokenType.DEMO -> {
                                    navView.inflateHeaderView(name.report.report.R.layout.nav_header_main_pre_auth)
                                    val vkRegistration = navView.getHeaderView(0).findViewById<ImageView>(name.report.report.R.id.vkRegistration)
                                    vkRegistration.setOnClickListener {
                                        drawerLayout.closeDrawers()
                                        regFragment = VkFragment()
                                        onRegistration = true
                                        registrationFragment.visible()
                                        supportFragmentManager.transaction {
                                            replace(R.id.registrationFragment, regFragment)
                                        }
                                    }
                                    val fbRegistration = navView.getHeaderView(0).findViewById<ImageView>(name.report.report.R.id.fbRegistration)
                                    fbRegistration.setOnClickListener({
                                        drawerLayout.closeDrawers()
                                        regFragment = FbFragment()
                                        onRegistration = true
                                        registrationFragment.visible()
                                        supportFragmentManager.transaction {
                                            replace(R.id.registrationFragment, regFragment)
                                        }
                                    })
                                }

                                else -> setAuthMenu()
                            }
                            updateRepInfo()
                            navView.setNavigationItemSelectedListener(this)
                        })
        viewModel.logged.observe(this, Observer {
            setAuthMenu()
            supportFragmentManager.transaction {
                remove(regFragment)
            }
        })

        bottomSheetBehavior = BottomSheetBehavior.from(requestBottomSheet)
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when(newState){
                    BottomSheetBehavior.STATE_EXPANDED -> sendRequest.animate().scaleX(1f).scaleY(1f).setDuration(0).start()
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        Timber.e("RE:: ${myLocation.layoutParams.height}")
                       resetUI()
                        sendRequest.gone()
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                val slide = (resources.getStatusBarHeight() + 8.toDp(resources))*2
                myLocation.animate().translationY(-slide + slide*slideOffset).setDuration(0).start()
                findPlaceBtn.animate().scaleX(slideOffset).scaleY(slideOffset).setDuration(0).start()
                sendRequest.animate().scaleX(slideOffset).scaleY(slideOffset).setDuration(0).start()
            }
        })

        bottomSheetBehavior2 = BottomSheetBehavior.from(sendRequestToUserWithRep)
        bottomSheetBehavior2.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when(newState){
                    BottomSheetBehavior.STATE_EXPANDED ->  fabSendRequestToUser.animate().scaleX(1f).scaleY(1f).setDuration(0).start()
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        closeFabAdding2.gone()
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                fabSendRequestToUser.animate().scaleX(0 + slideOffset).scaleY(0 + slideOffset).setDuration(0).start()
            }
        })

        sendRequest.animate().scaleX(0f).scaleY(0f).setDuration(0).start()
        fabSendRequestToUser.animate().scaleX(0f).scaleY(0f).setDuration(0).start()
        sendRequest.animate().translationY(resources.getStatusBarHeight().toFloat()).setDuration(0).start()
        findPlaceBtn.animate().scaleX(0f).scaleY(0f).setDuration(0).start()
        myLocation.animate().translationY(-64.toDp(resources).toFloat()).setDuration(0).start()

        closeFabAdding2.setOnClickListener({
            closeFabAdding2.gone()
            bottomSheetBehavior2.state = BottomSheetBehavior.STATE_COLLAPSED
        })
    }

    fun getCloseFabAdding() = closeFabAdding2

    override fun onPause() {
        super.onPause()
        BottomSheetFragmentInAlert.sendRequest.value = null
    }

    fun getHorizontalPicker() = number_picker

    fun hideTextClick(){
        seeIt.gone()
        clickIt.gone()
    }

    fun getSend() = fabSendRequestToUser

    fun getET() = requestToUserTextC

    fun getBottomSheetBehavior2() = bottomSheetBehavior2

    fun updateRepInfo(){
        viewModel.updateAndgetCurrUserUseCase.execute()
                .subscribeBy(
                        onSuccess = {
                            photoUrl = ""
                            userName = ""
                            rep = ""

                            it.repoints!!.forEach {
                                rep += it.repoints.toString() + "."
                                photoUrl += it.user!!.photo.toString() + " "
                                userName += it.user.firstName + " " + it.user.lastName + "."
                            }
                        },
                        onError = {

                        }
                )
    }


    @SuppressLint("SetTextI18n")
    private fun setAuthMenu() {
        navView.menu.clear()
        navView.removeHeaderView(navView.getHeaderView(0))
        navView.inflateHeaderView(name.report.report.R.layout.nav_header_main)
        navView.inflateMenu(name.report.report.R.menu.main_drawer_menu_afterauth)
        val imageBlur = navView.getHeaderView(0).findViewById<ImageView>(name.report.report.R.id.headerPhotoBlur)
        val userPhoto = navView.getHeaderView(0).findViewById<CircleImageView>(name.report.report.R.id.headerPhoto)
        val userName = navView.getHeaderView(0).findViewById<TextView>(name.report.report.R.id.headerName)
        val req = navView.getHeaderView(0).findViewById<TextView>(name.report.report.R.id.requestsCount)
        val dialog = navView.getHeaderView(0).findViewById<TextView>(name.report.report.R.id.dialogCount)
        val repoint = navView.getHeaderView(0).findViewById<TextView>(name.report.report.R.id.repointCount)
        viewModel.updateAndgetCurrUserUseCase.execute()
                .subscribeBy(
                        onSuccess = {
                            name.report.report.GlideApp.with(applicationContext)
                                    .load(it.photo)
                                    .transform(name.report.report.Blur(applicationContext))
                                    .into(imageBlur)
                            name.report.report.GlideApp.with(applicationContext)
                                    .load(it.photo)
                                    .into(userPhoto)
                            userName.text = it.firstName + " " + it.lastName
                            req.text = it.requestCount.toString()
                            dialog.text = it.dialogsCount.toString()
                            it.repoints!!.forEach {
                                numOfRep += it.repoints
                            }
                            repoint.text = numOfRep.toString()
                        },
                        onError = {

                        }
                )

    }

    fun updateNavInfo() {
        val req = navView.getHeaderView(0).findViewById<TextView>(name.report.report.R.id.requestsCount)
        val dialog = navView.getHeaderView(0).findViewById<TextView>(name.report.report.R.id.dialogCount)
        val repoint = navView.getHeaderView(0).findViewById<TextView>(name.report.report.R.id.repointCount)
        viewModel.updateAndgetCurrUserUseCase.execute()
                .subscribeBy(
                        onSuccess = {
                            numOfRep = 0
                            try {
                                req.text = it.requestCount.toString()
                                dialog.text = it.dialogsCount.toString()
                                it.repoints!!.forEach {
                                    numOfRep += it.repoints
                                }
                                repoint.text = numOfRep.toString()
                            }catch (e: Exception){

                            }
                        },
                        onError = {

                        }
                )
    }


    private fun closeSearch() {
        slidingLayout.panelState = previousSliderState
        supportFragmentManager.transaction {
            replace(name.report.report.R.id.flSliderContainer, SliderFragment.instance)
        }
        hideKeyBoard()
        searchField.clearFocus()
        searchField.setText("")
        searchField.isCursorVisible = false
        searchClose.gone()
        fabAddRequest.visible()
    }

    private fun initViews(flag: Boolean) {
        if (flag)
            searchField.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (count > 0)
                        if (mAdapter != null) {
                            rvSearchResult.adapter = mAdapter
                        }
                    if (mGoogleApiClient!!.isConnected) {
                        mAdapter!!.filter.filter(s.toString())
                    }

                }

                override fun afterTextChanged(s: Editable) {

                }
            })

    }

    override fun onConnected(bundle: Bundle?) {

    }

    override fun onConnectionSuspended(i: Int) {

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    override fun onResume(){
        super.onResume()
        if(register){
            drawerLayout.openDrawer(GravityCompat.START)
            register = false
        }
        if(BottomSheetFragment.sendRequest.value!=null){
            viewModel.showInFragment.value = BottomSheetFragment.sendRequest.value
        }
        if(appPreferences.guideShown()) {
            guideFirstStep.gone()
            try {
                if (zoom) {
                    fragment.zoomToCoordinates(17.0f, LatLng(52.096602, 23.732200), 800)
                    zoom = false
                    viewPager.setCurrentItem(0, true)
                }

            }catch (e: Exception){

            }
        }
    }

    override fun onPlaceClick(mResultList: ArrayList<PlaceAutocompleteAdapter.PlaceAutocomplete>?, position: Int) {
        if (mResultList != null) {
            try {
                val placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, mResultList[position].placeId.toString())
                placeResult.setResultCallback { places ->
                    if (places.count == 1) {
                        slidingLayout.panelState = previousSliderState
                        supportFragmentManager.transaction {
                            replace(name.report.report.R.id.flSliderContainer, SliderFragment.instance)
                        }
                        hideKeyBoard()
                        searchField.clearFocus()
                        searchField.setText("")
                        searchField.isCursorVisible = false
                        searchClose.gone()
                        fabAddRequest.visible()
                        slidingLayout.panelState = name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.COLLAPSED

                        setUiToAdding()
                        viewModel.addRequestFromSearchList.value = LatLng(places.get(0).latLng.latitude, places.get(0).latLng.longitude)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    fun hideKeyBoard() {
        val view = currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun setPanelState(state: name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState) {
        slidingLayout.addPanelSlideListener(object: SlidingUpPanelLayout.PanelSlideListener {
            override fun onPanelSlide(panel: View?, slideOffset: Float) {}

            override fun onPanelStateChanged(panel: View?, previousState: SlidingUpPanelLayout.PanelState?, newState: SlidingUpPanelLayout.PanelState?) {
                if (newState == SlidingUpPanelLayout.PanelState.ANCHORED) {
                    hideKeyBoard()
                    window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
                }
            }
        })
        slidingLayout.panelState = state
    }

    override fun onBackPressed() {
        when {
            onRegistration -> {
                onRegistration = false
                supportFragmentManager.transaction {
                    remove(regFragment)
                }
            }
            drawerLayout.isDrawerOpen(GravityCompat.START) -> drawerLayout.closeDrawer(GravityCompat.START)
            else -> super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        drawerLayout.closeDrawer(GravityCompat.START)
        when (item.itemId) {
            R.id.nav_share -> {
                val shareSub = "To learn more about why s::Port will change the way in which we inform ourselves, create new contents and collaborate in new ways for a life in a more friendly world, visit our website"
                startSharingIntent("text/plain", "http://www.getreport.co", shareSub, true, "Share using")
            }
            R.id.nav_feedback -> {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.feedback_link))))
            }
            R.id.nav_wallet -> {
                val intent = Intent(this, WalletActivity::class.java)
                intent.putExtra("photo", photoUrl)
                intent.putExtra("user", userName)
                intent.putExtra("rep", rep)
                intent.putExtra("num", numOfRep)
                startActivity(intent)
            }
            R.id.nav_market -> {
                startActivity(Intent(this, MarketActivity::class.java))
            }
            R.id.nav_settings -> {
                Snackbar.make(currentFocus, getString(R.string.sorry_we_will_at_this_feature_soon), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                val view = layoutInflater.inflate(name.report.report.R.layout.coming_soon_alert, null)
                val closeDelBtn = view.findViewById<TextView>(name.report.report.R.id.close)
                val adb = AlertDialog.Builder(this@MainActivity, name.report.report.R.style.CustomAlertDialog)
                adb.setView(view)
                val alert = adb.create()
                alert!!.show()
                val lp = WindowManager.LayoutParams()
                lp.copyFrom(alert.window!!.attributes)
                lp.width = resources.displayMetrics.widthPixels - 80.toDp(resources)
                alert.window!!.attributes = lp
                closeDelBtn.setOnClickListener({ alert.dismiss() })
                    alert.dismiss()
                }
        }
        return true
    }

    //Calls when user click something on permission request alert
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK){
                setUiToAdding()
                viewModel.addRequestFromSearchList.value = PlaceAutocomplete.getPlace(this, data).latLng
            }
        }
    }

    private fun showSearch(){
        try {
            startActivityForResult(PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(this), 1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun resetUI() {
        hideKeyBoard()
        sendRequest.gone()
        if(bottomSheetBehavior.state!=BottomSheetBehavior.STATE_COLLAPSED)
            bottomSheetBehavior.state=BottomSheetBehavior.STATE_COLLAPSED
        sendRequest.animate().scaleX(0f).scaleY(0f).setDuration(0).start()
        moveLeftTopFabs(true)
        fabAddRequest.visible()
        leftMenuButton.visible()
        closeFabAdding.gone()
        addFriendsRL.visible()
        fragment.getMarker()?.remove()
        fragment.getCircle()?.remove()
        etRequestText.setText("")
    }

    //Set UI enabled to add new request
    fun setUiToAdding() {
        sendRequest.visible()
        setAddBSState(BottomSheetBehavior.STATE_EXPANDED)
        slidingLayout.panelState = name.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.COLLAPSED
        fabAddRequest.gone()
        leftMenuButton.gone()
        moveLeftTopFabs(false)
        closeFabAdding.visible()
    }

    private fun moveLeftTopFabs(toTop: Boolean){
        if(toTop){
            findPlaceBtn.animate().scaleX(0f).scaleY(0f).setDuration(500).start()
        }else{
            findPlaceBtn.animate().scaleX(1f).scaleY(1f).setDuration(500).start()
        }
    }

    //Animate adding fab
    private fun setFabMargins(animate: Int) {
        fabAddRequest.animate().translationY(animate.toDp(resources).toFloat()).setDuration(0).start()
    }

    private fun leftMenuButtonBelowStatusBar() {
        leftMenuButton.setMargins(8.toDp(resources), resources.getStatusBarHeight() + 8.toDp(resources), 0)
        leftMenuButton.visible()
        leftMenuButton.setOnClickListener({ drawerLayout.openDrawer(Gravity.START) })
    }

    private fun myLocationBelowStatusBar() {
        closeFabAdding2.setMargins(8.toDp(resources), resources.getStatusBarHeight() + 8.toDp(resources), 0)
        closeFabAdding.setMargins(8.toDp(resources), resources.getStatusBarHeight() + 8.toDp(resources), 0)
        findPlaceBtn.setMargins(0, resources.getStatusBarHeight() + 8.toDp(resources), 8.toDp(resources))
        myLocation.visible()
        findPlaceBtn.visible()
        myLocation.setOnClickListener({
            if(appPreferences.guideShown())
                fragment.zoomToMyCoordinates()
        })
    }

    private fun startSharingIntent(type: String, body: String, sub: String, isShare: Boolean, text: String) {
        val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
        sharingIntent.type = type
        if (!isShare)
            sharingIntent.putExtra(android.content.Intent.EXTRA_EMAIL, arrayOf("feedback@getreport.co"))
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, sub)
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body)
        startActivity(Intent.createChooser(sharingIntent, text))
    }

    fun getGuide() = guideFirstStep

    private fun checkGuide() {
        if (appPreferences.guideShown()) return
        guideFirstStep.visible()
        myLocation.gone()
        val fakePosition = LatLng(52.104969, 23.718071)
        viewModel.posToMove.value = fakePosition
    }

    @SuppressLint("SetTextI18n")
    fun showRegisterAlert(){
        val builder = AlertDialog.Builder(this)
        val dialog = builder.setTitle(getString(R.string.register_1))
                .setMessage(getString(R.string.register_2))
                .setPositiveButton(getString(R.string.register_3), {
                    dialog, _ ->
                    dialog.dismiss()
                    drawerLayout.openDrawer(GravityCompat.START)
                })
                .setNegativeButton(getString(R.string.cancel), {
                    dialog, _ ->
                    dialog.dismiss()
                }).create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.main_red))
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.main_red))
    }
}
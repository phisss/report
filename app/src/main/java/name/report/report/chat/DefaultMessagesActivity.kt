@file:Suppress("DEPRECATION")

package name.report.report.chat

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.media.ThumbnailUtils
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import name.report.report.App
import name.report.report.R
import name.report.report.R.color.*
import name.report.report.R.drawable.arrow_gray
import name.report.report.chat.modele.User
import name.report.report.data.network.dto.OnComplete
import name.report.report.data.network.dto.OnProgress
import name.report.report.data.network.managers.MediaUploader
import name.report.report.presentation.chat.ImageOverlayView
import name.report.report.presentation.main.viewmodel.MainActivityViewModel
import com.stfalcon.chatkit.commons.ImageLoader
import com.stfalcon.chatkit.messages.MessageHolders
import com.stfalcon.chatkit.messages.MessageInput
import com.stfalcon.chatkit.messages.MessagesListAdapter
import com.stfalcon.frescoimageviewer.ImageViewer
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_custom_holder_messages.*
import name.report.report.MainActivity
import name.report.report.chat.bottomsheetfragments.BottomSheetFragment
import name.report.report.chat.bottomsheetfragments.BottomSheetFragmentToRepoints
import name.report.report.chat.model.Message
import name.report.report.data.cache.AppPreferences
import name.report.report.domain.*
import name.report.report.domain.usecases.messages.GetRequestDialogUseCase
import name.report.report.presentation.sendrequest.SendFragment
import name.report.report.utils.transaction
import net.alhazmy13.mediapicker.Image.ImagePicker
import net.alhazmy13.mediapicker.Video.VideoPicker
import timber.log.Timber
import javax.inject.Inject

class DefaultMessagesActivity : DemoMessagesActivity(), ImageLoader, MessageInput.AttachmentsListener, MessageHolders.ContentChecker<Message>,
        MessageInput.InputListener, View.OnClickListener {

    override fun loadImage(imageView: ImageView?, url: String?) {
    }

    override fun hasContentFor(message: Message?, type: Byte): Boolean {
        return false
    }

    @Inject
    lateinit var mediaUploader: MediaUploader

    @Inject
    lateinit var getRequestDialogUseCase: GetRequestDialogUseCase

    @Inject
    lateinit var appPreferences: AppPreferences

    private var userName = ""
    private var shownReq = false
    private var menu: Menu? = null
    private var follow = false
    private lateinit var followed: Drawable
    private lateinit var unfollowed: Drawable
    private var menuOpen = false
    private lateinit var dialogId: String
    private var listOfMediaUrls = ArrayList<MediaContent>()
    private val thumbHash = HashMap<String, String>()
    private val usersHashMap = HashMap<String, String>()
    private val nameHash = HashMap<String, String>()
    private var numOfMessages = 0
    private var isLoggedUser = false

    private fun setActionBarBackArrow() {
        val upArrow = resources.getDrawable(arrow_gray)
        upArrow.setColorFilter(resources.getColor(main_red), PorterDuff.Mode.SRC_ATOP)
        supportActionBar?.setHomeAsUpIndicator(upArrow)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        BottomSheetFragment.sendRequest.value = null
        App.appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(name.report.report.R.layout.activity_custom_holder_messages)
        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        followed = getDrawable(name.report.report.R.drawable.ic_bell)
        unfollowed = getDrawable(name.report.report.R.drawable.ic_bell_g)
        supportActionBar?.show()
        viewModel.getTokensUseCase.execute()
                .subscribeBy(
                        onSuccess ={
                            isLoggedUser = it.type != TokenType.DEMO
                        },
                        onError = {}
                )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
        val tv = TextView(applicationContext)
        val lp = RelativeLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT)
        tv.layoutParams = lp
        if (intent.getStringExtra("title").length > 23)
            tv.text = intent.getStringExtra("title").substring(0, 23) + "..."
        else
            tv.text = intent.getStringExtra("title")
        tv.textSize = 16.toFloat()
        tv.setTextColor(Color.parseColor("#000000"))
        tv.typeface = Typeface.create("sans-serif", Typeface.NORMAL)
        window.statusBarColor = ContextCompat.getColor(this, R.color.main_hint)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.customView = tv
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#ffffffff")))
        setActionBarBackArrow()
        getRequestDialogUseCase.execute(intent.getStringExtra(DIALOG_ID_KEY)).subscribeBy(
                onSuccess = {
                    intent.putExtra(AUTHOR_ID_KEY, it.messages.first().userId)
                    initAdapter()
                    initializeUi()
                    dialogId = intent.getStringExtra("DIALOG_ID")
                    viewModel.getFollowDialogsUseCase.execute()
                            .subscribeBy(
                                    onSuccess = {
                                        it.forEach {
                                            if(it.request!=null)
                                                if (it.request._id == dialogId)
                                                    follow = true
                                        }
                                        if (menu != null) {
                                            if (viewModel.getCurrUserUseCase.execute()!!.userId != intent.getStringExtra("authorId"))
                                                menu!!.getItem(0).icon = when (follow) {
                                                    true -> followed
                                                    false -> unfollowed
                                                }
                                        }
                                    },
                                    onError = {

                                    }
                            )

                    viewModel.getMessageUseCase.execute(dialogId)
                            .subscribeBy(
                                    onSuccess = {
                                        it.users.forEach {
                                            if (it.photo == null) {
                                                it.photo = "null"
                                            }
                                            usersHashMap.put(it.userId, it.photo.toString())
                                            nameHash.put(it.userId, it.firstName.toString() + " " + it.lastName.toString())
                                        }
                                        it.messages.forEach {
                                            numOfMessages += 1
                                            //                                Timber.e("RE:: >>> ${it.attachments!![0].thumbnail}")
                                            if (it.message != null) {
                                                messagesAdapter!!.addToStart(Message(it.id!!, User(it.userId!!, "asd" + " 0 0", usersHashMap[it.userId]), it.message), true)
                                            } else {
                                                val message = if (it.repoints != null) {
                                                    var numOfRepoints = 0
                                                    var iSent = 0
                                                    it.repoints.forEach {
                                                        numOfRepoints += it.repoints
                                                        if (it.userId == viewModel.getCurrUserUseCase.execute()!!.userId)
                                                            iSent = 1
                                                    }
                                                    Message(it.id!!, User(it.userId!!, it.attachments!![0].type + " " + numOfRepoints + " " + iSent, usersHashMap[it.userId]), null)
                                                } else
                                                    Message(it.id!!, User(it.userId!!, it.attachments!![0].type + " 0 0", usersHashMap[it.userId]), null)
                                                if (it.attachments[0].thumbnail == null) {
                                                    thumbHash.put(it.attachments[0].src, it.attachments[0].src)
                                                    message.setImage(Message.Image(it.attachments[0].src))
                                                } else {
                                                    thumbHash.put(it.attachments[0].src, it.attachments[0].thumbnail.toString())
                                                    message.setImage(Message.Image(it.attachments[0].thumbnail!!))
                                                }
                                                listOfMediaUrls.add(MediaContent(it.attachments[0].type, it.attachments[0].src))
                                                messagesAdapter!!.addToStart(message, true)
                                            }
                                        }
                                    },
                                    onError = {

                                    }
                            )
                    viewModel.getEventsUseCase.execute()
                            .subscribeBy(
                                    onNext = {
                                        Timber.e("RE:: $it")
                                        when (it) {
                                            is NewMessageEvent -> {
                                                Timber.e("RE:: $it")
                                                viewModel.getMessageUseCase.execute(dialogId)
                                                        .subscribeBy(
                                                                onSuccess = {
                                                                    it.users.forEach {
                                                                        if (it.photo == null) {
                                                                            it.photo = "null"
                                                                        }
                                                                        usersHashMap.put(it.userId, it.photo.toString())
                                                                        nameHash.put(it.userId, it.firstName.toString() + " " + it.lastName.toString())
                                                                    }
                                                                    it.messages.subList(numOfMessages, it.messages.size).forEach {
                                                                        numOfMessages += 1
                                                                        if (it.message != null) {
                                                                            messagesAdapter!!.addToStart(Message(it.id!!, User(it.userId!!, "asd" + " 0 0", usersHashMap[it.userId]), it.message), true)
                                                                        } else {
                                                                            val message = if (it.repoints != null) {
                                                                                var numOfRepoints = 0
                                                                                var iSent = 0
                                                                                it.repoints.forEach {
                                                                                    numOfRepoints += it.repoints
                                                                                    if (it.userId == viewModel.getCurrUserUseCase.execute()!!.userId)
                                                                                        iSent = 1
                                                                                }
                                                                                Message(it.id!!, User(it.userId!!, it.attachments!![0].type + " " + numOfRepoints + " " + iSent, usersHashMap[it.userId]), null)
                                                                            } else
                                                                                Message(it.id!!, User(it.userId!!, it.attachments!![0].type + " 0 0", usersHashMap[it.userId]), null)
                                                                            Timber.e("RE:: ${it.attachments}")
                                                                            if (it.attachments[0].thumbnail == null) {
                                                                                thumbHash.put(it.attachments[0].src, it.attachments[0].src)
                                                                                message.setImage(Message.Image(it.attachments[0].src))
                                                                            } else {
                                                                                thumbHash.put(it.attachments[0].src, it.attachments[0].thumbnail.toString())
                                                                                message.setImage(Message.Image(it.attachments[0].thumbnail!!))
                                                                            }
                                                                            listOfMediaUrls.add(MediaContent(it.attachments[0].type, it.attachments[0].src))
                                                                            messagesAdapter!!.addToStart(message, true)
                                                                        }
                                                                    }
                                                                },
                                                                onError = {

                                                                }
                                                        )
                                            }
                                        }
                                    },
                                    onError = {

                                    }
                            )

                    supportFragmentManager.transaction {
                        replace(R.id.sendRequestFragment, SendFragment())
                    }

                    BottomSheetFragment.sendRequest.observe(this, Observer {
                        if (it != null)
                            if (isLoggedUser){
                                    finish()
                            }else{
                                showRegisterAlert()
                                BottomSheetFragment.sendRequest.value = null
                            }
                    })

                    messagesAdapter!!.setOnMessageClickListener({ message ->
                        Timber.e(message.user.id)
                        if (message.text == null) {
                            var i = 0
                            val view = ImageOverlayView(this)
                            listOfMediaUrls.forEach {
                                if (thumbHash.getValue(it.src) == message.imageUrl) {
                                    ImageViewer.Builder(this, listOfMediaUrls)
                                            .setFormatter({
                                                thumbHash.getValue(it.src)
                                            })
                                            .setStartPosition(i)
                                            .hideStatusBar(false)
                                            .setImageChangeListener({ position ->
                                                if (listOfMediaUrls[position].type == "video") {
                                                    view.setOverlayVisibility(View.VISIBLE)
                                                    view.setShareText(listOfMediaUrls[position].src)
                                                } else {
                                                    view.setOverlayVisibility(View.GONE)
                                                }
                                            })
                                            .allowZooming(true)
                                            .allowSwipeToDismiss(true)
                                            .setOverlayView(view)
                                            .show()
                                }
                                i++
                            }
                        }
                    })
                },
                onError = {}
        )

    }

    private fun showSendRequest(userInfo: UserMenuBS) {
        val bundle = Bundle()
        bundle.putString("userId", userInfo.userId)
        bundle.putString("userName", nameHash.getValue(userInfo.userId))
        bundle.putString("userPhoto", userInfo.userPhoto)
        val bsFragment = BottomSheetFragment()
        bsFragment.arguments = bundle
        bsFragment.show(supportFragmentManager, bsFragment.tag)
    }

    private fun hideSendRequestLayout() {
        shownReq = false
    }

    override fun onSubmit(input: CharSequence): Boolean {
        if(isLoggedUser)
            viewModel.sendMessageUseCase.execute(dialogId, ReportMessage(
                    input.toString(),
                    null))
                    .subscribeBy(
                            onSuccess = {},
                            onError = {})
        else
            showRegisterAlert()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        getRequestDialogUseCase.execute(intent.getStringExtra(DIALOG_ID_KEY)).subscribeBy(
                onSuccess = {
                    Timber.e("RE:: ${viewModel.getCurrUserUseCase.execute()!!.userId}")
                    if (viewModel.getCurrUserUseCase.execute()!!.userId != it.messages.first().userId)
                        menuInflater.inflate(R.menu.chat_menu, menu)
                    else
                        menuInflater.inflate(R.menu.chat_menu_2, menu)
                    this.menu = menu
                },
                onError = {}
        )
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.chatFollow -> {
                if (item.icon == followed)
                    leaveDialog(item)
                else
                    joinDialog(item)
            }
            R.id.chatInfo -> {
                Snackbar.make(currentFocus, getString(R.string.we_will_add), Snackbar.LENGTH_SHORT).show()
            }
            android.R.id.home -> {
                finish()
            }
            else -> super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun initAdapter() {
        IncomingImageMessageViewHolder.activity.value = this
        val holdersConfig = MessageHolders()
                .setIncomingTextConfig(
                        IncomingTextMessageViewHolder::class.java,
                        R.layout.incoming_text_message)
                .setOutcomingTextLayout(
                        R.layout.outcoming_text_message)
                .setIncomingImageConfig(
                        IncomingImageMessageViewHolder::class.java,
                        R.layout.incoming_image_message)
                .setOutcomingImageConfig(
                        OutcomingImageMessageViewHolder::class.java,
                        R.layout.outcoming_image_message)

        IncomingImageMessageViewHolder.sendRequest.observe(this, Observer {
            if (it != null) {
                it.userName = userName
                showSendRequest(it)
            }
        })
        IncomingTextMessageViewHolder.sendRequest.observe(this, Observer {
            if (it != null) {
                it.userName = userName
                showSendRequest(it)
            }
        })

        BottomSheetFragmentToRepoints.sendRepoints.observe(this, Observer {
            if (it != null)
                IncomingImageMessageViewHolder.sendRepoints.value = SendRepoints(it.messageId, it.value)
        })

        //RE:: repoint
        IncomingImageMessageViewHolder.showRepoints.observe(this, Observer {
            if(it!=null)
                if(isLoggedUser) {
                    Timber.e("RE:: $it")
                    BottomSheetFragmentToRepoints.dismiss.value = true
                    val bundle = Bundle()
                    bundle.putString("messageId", it)
                    val bsFragment = BottomSheetFragmentToRepoints()
                    bsFragment.arguments = bundle
                    bsFragment.show(supportFragmentManager, bsFragment.tag)
                }else{
                    showRegisterAlert()
            }
        })

        super.messagesAdapter = MessagesListAdapter(viewModel.getCurrUserUseCase.execute()!!.userId, holdersConfig, super.imageLoader)

        messagesList.setHasFixedSize(true)
        messagesList.setItemViewCacheSize(30)
        messagesList.isDrawingCacheEnabled = true
        messagesList.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
        messagesList.setAdapter(super.messagesAdapter)

    }

    private fun initializeUi() {
        input.setInputListener(this)
        a_camera.setOnClickListener(this)
        a_video.setOnClickListener(this)
        the_menu.setOnClickListener(this)
        input.setAttachmentsListener(this)
    }

    override fun onSelectionChanged(count: Int) {

    }

    override fun onLoadMore(page: Int, totalItemsCount: Int) {

    }

    override fun onDestroy() {
        super.onDestroy()
        IncomingTextMessageViewHolder.sendRequest.value = null
        IncomingImageMessageViewHolder.sendRequest.value = null
        IncomingImageMessageViewHolder.showRepoints.value = null
        IncomingImageMessageViewHolder.sendRepoints.value = null
    }

    override fun onBackPressed() {
        when {
            menuOpen -> hideMenu()
            shownReq -> hideSendRequestLayout()
            else -> finishAfterTransition()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            val mPaths = data!!.getSerializableExtra(ImagePicker.EXTRA_IMAGE_PATH) as List<*>
            if (mPaths[0] == null)
                return
            mediaUploader.uploadMedia(mPaths[0] as String, MediaUploader.MediaType.PHOTO)
                    .subscribeBy(
                            onNext = {
                                when (it) {
                                    is OnProgress -> {

                                    }
                                    is OnComplete -> {
                                        viewModel.sendMessageUseCase.execute(dialogId, ReportMessage(
                                                null,
                                                listOf(Attachment("photo", it.url.toString()))))
                                                .subscribeBy(
                                                        onSuccess = {},
                                                        onError = {})
                                    }
                                }
                            },
                            onError = {

                            }
                    )
        }
        if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val mPaths = data!!.getSerializableExtra(VideoPicker.EXTRA_VIDEO_PATH) as List<*>

            mediaUploader.uploadBitmap(ThumbnailUtils.createVideoThumbnail(mPaths[0] as String,
                    MediaStore.Images.Thumbnails.MINI_KIND))
                    .doOnNext { Timber.d("THUMB UPLOADED: $it") }
                    .flatMap { thumb ->
                        mediaUploader.uploadMedia(mPaths[0] as String, MediaUploader.MediaType.VIDEO).map { Pair(thumb, it) }
                    }
                    .subscribeBy(
                            onNext = {
                                when (it.second) {
                                    is OnProgress -> {

                                    }
                                    is OnComplete -> {
                                        Timber.d("VIDEO UPLOADED: $it")
                                        viewModel.sendMessageUseCase.execute(dialogId, ReportMessage(
                                                null,
                                                listOf(Attachment("video", (it.second as OnComplete).url.toString(), it.first))))
                                                .subscribeBy(
                                                        onSuccess = {
                                                            Timber.d("MESSAGE SEND: $it")
                                                        },
                                                        onError = {})
                                    }
                                }
                            },
                            onError = {}
                    )
        }
        super.onActivityResult(requestCode, resultCode, data)

    }

    override fun onClick(p0: View?) {
        if (p0 == a_camera) {
            if (menuOpen)
                hideMenu()
            ImagePicker.Builder(this)
                    .mode(ImagePicker.Mode.CAMERA)
                    .directory(ImagePicker.Directory.DEFAULT)
                    .extension(ImagePicker.Extension.JPG)
                    .allowMultipleImages(false)
                    .enableDebuggingMode(true)
                    .compressLevel(ImagePicker.ComperesLevel.SOFT)
                    .scale(480, 480)
                    .build()
        }
        if (p0 == a_video) {
            if (menuOpen)
                hideMenu()
            VideoPicker.Builder(this)
                    .mode(VideoPicker.Mode.CAMERA)
                    .directory(VideoPicker.Directory.DEFAULT)
                    .extension(VideoPicker.Extension.MP4)
                    .enableDebuggingMode(true)
                    .build()
        }
    }

    override fun onAddAttachments() {
        if (!menuOpen) {
            revealMenu()
        } else
            hideMenu()
    }

    private fun revealMenu() {
        menuOpen = true
        div.setBackgroundColor(resources.getColor(name.report.report.R.color.white))
        the_menu.visibility = View.INVISIBLE
        val cx = the_menu.left
        val cy = the_menu.height
        val finalRadius = Math.sqrt((the_menu.height * the_menu.height + the_menu.width * the_menu.width).toDouble()).toInt()
        val anim = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewAnimationUtils.createCircularReveal(the_menu, cx, cy, 0f, finalRadius.toFloat())
        } else {
            TODO("VERSION.SDK_INT < LOLLIPOP")
        }
        anim.duration = 500
        the_menu.visibility = View.VISIBLE
        overlay.visibility = View.VISIBLE
        anim.start()
        val popeup1 = AnimationUtils.loadAnimation(this, name.report.report.R.anim.popeup)
        val popeup2 = AnimationUtils.loadAnimation(this, name.report.report.R.anim.popeup)
        val popeup3 = AnimationUtils.loadAnimation(this, name.report.report.R.anim.popeup)
        popeup1.startOffset = 100
        popeup2.startOffset = 200
        popeup3.startOffset = 300
        menu1.startAnimation(popeup1)
        menu2.startAnimation(popeup2)
        menu3.startAnimation(popeup3)
    }

    private fun hideMenu() {
        div.setBackgroundColor(resources.getColor(name.report.report.R.color.divider))
        menuOpen = false
        val cx = the_menu.left
        val cy = the_menu.height
        val initialRadius = Math.sqrt((the_menu.height * the_menu.height + the_menu.width * the_menu.width).toDouble()).toInt()
        val anim = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewAnimationUtils.createCircularReveal(the_menu, cx, cy, initialRadius.toFloat(), 0f)
        } else {
            TODO("VERSION.SDK_INT < LOLLIPOP")
        }
        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                the_menu.visibility = View.INVISIBLE
                the_menu.visibility = View.GONE
                overlay.visibility = View.INVISIBLE
                overlay.visibility = View.GONE
            }
        })
        anim.start()
    }

    private fun joinDialog(item: MenuItem) {
        viewModel.joinUseCase.execute(dialogId)
                .subscribeBy(
                        onSuccess = {
                            item.icon = followed
                            viewModel.getMessageUseCase.execute(dialogId)
                                    .subscribeBy(
                                            onSuccess = {
                                                usersHashMap.clear()
                                                it.users.forEach {
                                                    if (it.photo == null) {
                                                        it.photo = "null"
                                                    }
                                                    usersHashMap.put(it.userId, it.photo.toString())
                                                    nameHash.put(it.userId, it.firstName.toString() + " " + it.lastName.toString())

                                                }
                                            },
                                            onError = {

                                            }
                                    )
                        },
                        onError = {}
                )
    }

    private fun leaveDialog(item: MenuItem) {
        viewModel.leaveUseCase.execute(dialogId)
                .subscribeBy(
                        onSuccess = {
                            item.icon = unfollowed
                        },
                        onError = {

                        }
                )
    }

    companion object {
        const val DIALOG_ID_KEY = "DIALOG_ID"
        const val AUTHOR_ID_KEY = "authorId"
        const val TITLE_KEY = "title"

        lateinit var viewModel: MainActivityViewModel
        fun open(context: Context, dialogId: String, title: String, authorId: String) {
            val intent = Intent(context, DefaultMessagesActivity::class.java)
            intent.putExtra("DIALOG_ID", dialogId)
            intent.putExtra("title", title)
            intent.putExtra("authorId", authorId)
            context.startActivity(intent)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showRegisterAlert(){
        val builder = AlertDialog.Builder(this)
        val dialog = builder.setTitle(getString(R.string.register_1))
                .setMessage(getString(R.string.register_2))
                .setPositiveButton(getString(R.string.register_3), {
                    dialog, _ ->
                    dialog.dismiss()
                    MainActivity.register = true
                    finish()
                })
                .setNegativeButton(getString(R.string.cancel), {
                    dialog, _ ->
                    dialog.dismiss()
                }).create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.main_red))
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.main_red))
    }
}
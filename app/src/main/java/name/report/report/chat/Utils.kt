package name.report.report.chat

import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.util.TypedValue
import android.widget.ImageView

import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.UUID


import android.os.Environment.getExternalStoragePublicDirectory
import name.report.report.App


object Utils {

    val photosRef: StorageReference
        get() = FirebaseStorage.getInstance().reference.child("photos")

    val videoRef: StorageReference
        get() = FirebaseStorage.getInstance().reference.child("videos")

    fun generateMediaFileName(): String {
        return UUID.randomUUID().toString().replace("-", "")
    }

    fun ext(name: String, isVideo: Boolean): String {
        return if (isVideo) name + ".mp4" else name + ".jpg"
    }

    fun createImagePublicPath(): String {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val storageDir = File(getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Report")
        storageDir.mkdirs()
        return storageDir.absolutePath + "/JPEG_" + timeStamp + ".jpg"
    }

    fun getMediaPath(name: String): String {
        val storageDir = File(getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Report")
        storageDir.mkdirs()
        return (storageDir.absolutePath
                + File.separator + name)
    }

    @Throws(IOException::class)
    fun copyTo(from: String, to: String): Boolean {
        val sourceFile = File(from)
        val destFile = File(to)
        if (!destFile.exists()) {
            destFile.createNewFile()
        }
        var source: FileInputStream? = null
        var destination: FileOutputStream? = null
        try {
            source = FileInputStream(sourceFile)
            destination = FileOutputStream(destFile)
            destination.channel.transferFrom(source.channel, 0, source.channel.size())
        } catch (e: Exception) {
            return false
        } finally {
            if (source != null) {
                source.close()
            }
            if (destination != null) {
                destination.close()
            }
        }
        return true
    }

    fun getVideoThumbPath(name: String): String {
        return App.context.externalCacheDir.absolutePath + "/" + name
    }



    fun createVideoPublicPath(): String {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val storageDir = File(getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Report")
        storageDir.mkdirs()
        return storageDir.absolutePath + "/VID_" + timeStamp + ".mp4"
    }

    fun galleryAddPic(path: String) {
        try {
            val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
            val f = File(path)
            val contentUri = Uri.fromFile(f)
            mediaScanIntent.data = contentUri
            App.context.sendBroadcast(mediaScanIntent)
        } catch (e: Exception) {
        }

    }



    fun transform(image: ImageView, w: Int, h: Int) {
        val targetWidth = dpToPx(220)
        val aspectRatio = h.toDouble() / w.toDouble()
        val targetHeight = (targetWidth * aspectRatio).toInt()
        image.requestLayout()
        image.layoutParams
        image.layoutParams.width = targetWidth
        image.layoutParams.height = targetHeight
        image.scaleType = ImageView.ScaleType.FIT_XY
    }

    fun dpToPx(dp: Int): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(),  App.context.resources.displayMetrics).toInt()
    }


}

package name.report.report.chat.bottomsheetfragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.support.design.widget.BottomSheetDialogFragment
import android.view.View
import kotlinx.android.synthetic.main.send_repoint_bs.*
import kotlinx.android.synthetic.main.send_repoint_bs.view.*
import name.report.report.R
import name.report.report.domain.RepointToSend


class BottomSheetFragmentSendRequestToUser : BottomSheetDialogFragment() {

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val view = View.inflate(context, R.layout.send_request_fragment, null)
        dialog.setContentView(view)

    }
}

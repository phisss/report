@file:Suppress("DEPRECATION")

package name.report.report.chat

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.ActionBar
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import name.report.report.App
import name.report.report.MainActivity
import name.report.report.R
import name.report.report.R.color.*
import name.report.report.R.drawable.arrow_gray
import name.report.report.chat.modele.User
import name.report.report.data.network.dto.OnComplete
import name.report.report.data.network.dto.OnProgress
import name.report.report.data.network.managers.MediaUploader
import name.report.report.presentation.chat.ImageOverlayView
import name.report.report.presentation.main.viewmodel.MainActivityViewModel
import com.stfalcon.chatkit.commons.ImageLoader
import com.stfalcon.chatkit.messages.MessageHolders
import com.stfalcon.chatkit.messages.MessageInput
import com.stfalcon.chatkit.messages.MessagesListAdapter
import com.stfalcon.frescoimageviewer.ImageViewer
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_custom_holder_messages.*
import name.report.report.App.Companion.context
import name.report.report.chat.model.Message
import name.report.report.data.cache.AppPreferences
import name.report.report.domain.*
import name.report.report.utils.gone
import name.report.report.utils.visible
import net.alhazmy13.mediapicker.Image.ImagePicker
import net.alhazmy13.mediapicker.Video.VideoPicker
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DefaultMessagesActivityInfo : DemoMessagesActivity(), ImageLoader,MessageInput.AttachmentsListener, MessageHolders.ContentChecker<Message> ,
        MessageInput.InputListener, View.OnClickListener {

    override fun loadImage(imageView: ImageView?, url: String?) {

    }

    override fun hasContentFor(message: Message?, type: Byte): Boolean {
        return false
    }

    @Inject
    lateinit var mediaUploader: MediaUploader

    @Inject
    lateinit var appPreferences: AppPreferences

    private var menu: Menu? = null
    private var menuOpen = false
    private val thumbHash = HashMap<String, String>()
    private var listOfMediaUrls = ArrayList<MediaContent>()
    private var added = false

    private fun setActionBarBackArrow() {
        val upArrow = resources.getDrawable(arrow_gray)
        upArrow.setColorFilter(resources.getColor(main_red), PorterDuff.Mode.SRC_ATOP)
        supportActionBar?.setHomeAsUpIndicator(upArrow)
    }

    override fun onDestroy(){
        super.onDestroy()
        appPreferences.setGuideShown()
        MainActivity.zoom = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(name.report.report.R.layout.activity_custom_holder_messages)
        supportActionBar?.show()
        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA), 123)
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
        }
        val tv = TextView(applicationContext)
        val lp = RelativeLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT)
        tv.layoutParams = lp
        tv.text = message
        tv.textSize = 16.toFloat()
        tv.setTextColor(Color.parseColor("#000000"))
        tv.typeface = Typeface.create("sans-serif", Typeface.NORMAL);
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.customView = tv
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#ffffffff")))
        setActionBarBackArrow()
        initAdapter()
        initializeUi()
        messagesAdapter!!.setOnMessageClickListener({
            message ->
            var i = 0
            val view = ImageOverlayView(this)
            listOfMediaUrls.forEach {
                try {
                    if (thumbHash.getValue(it.src) == message.imageUrl) {
                        view.setShareText(it.src)
                        ImageViewer.Builder(this, listOfMediaUrls)
                                .setFormatter({ thumbHash.getValue(it.src) })
                                .setStartPosition(i)
                                .hideStatusBar(false)
                                .setImageChangeListener({ position ->
                                    if (listOfMediaUrls[position].type == "video") {
                                        view.setOverlayVisibility(View.VISIBLE)
                                        view.setShareText(listOfMediaUrls[position].src)
                                    } else {
                                        view.setOverlayVisibility(View.GONE)
                                    }
                                })
                                .allowZooming(true)
                                .allowSwipeToDismiss(true)
                                .setOverlayView(view)
                                .show()
                    }
                    i++
                }catch (e: Exception){}
            }
        })
    }

    override fun onSubmit(input: CharSequence): Boolean {
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.chat_menu_2, menu)
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> {
                appPreferences.setGuideShown()
                MainActivity.zoom = true
                finish()
            }
            else -> super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun initAdapter() {
        val holdersConfig = MessageHolders()
                .setIncomingTextConfig(
                        IncomingTextMessageViewHolderInfo::class.java,
                        name.report.report.R.layout.incoming_text_message)
                .setOutcomingTextLayout(
                        name.report.report.R.layout.outcoming_text_message)
                .setIncomingImageConfig(
                        IncomingImageMessageViewHolderInfo::class.java,
                        R.layout.incoming_image_message
                )
                .setOutcomingImageConfig(
                        OutcomingImageMessageViewHolderInfo::class.java,
                        name.report.report.R.layout.outcoming_image_message)


        super.messagesAdapter = MessagesListAdapter("user", holdersConfig, super.imageLoader)

        messagesList.setHasFixedSize(true)
        messagesList.setItemViewCacheSize(30)
        messagesList.isDrawingCacheEnabled = true
        messagesList.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
        messagesList.setAdapter(super.messagesAdapter)

        messagesAdapter!!.addToStart(Message("user", User("user", "", ""), message), true)

        messagesAdapter!!.addToStart(Message("", User("", "", "null"), getString(R.string.f_message)), true)

        val message = Message("1", User("","video 0 0", "null"), null)
        thumbHash.put(getString(R.string.learn_video), "https://user-images.githubusercontent.com/10070770/34083563-f610e0e0-e383-11e7-900e-393a7120cbc8.png")
        message.setImage(Message.Image("https://user-images.githubusercontent.com/10070770/34083563-f610e0e0-e383-11e7-900e-393a7120cbc8.png"))
        listOfMediaUrls.add(MediaContent("video", getString(R.string.learn_video)))
        messagesAdapter!!.addToStart(message,true)

    }

    private fun initializeUi() {
        input.setInputListener(this)
        a_camera.setOnClickListener(this)
        a_video.setOnClickListener(this)
        the_menu.setOnClickListener(this)
        input.setAttachmentsListener(this)
    }

    override fun onStop() {
        super.onStop()
        if(!added) {
            messagesAdapter!!.addToStart(Message("", User("", "", "null"), getString(R.string.s_message)), true)
            added = true
        }
    }

    override fun onSelectionChanged(count: Int) {

    }

    override fun onLoadMore(page: Int, totalItemsCount: Int) {

    }

    override fun onBackPressed() {
        finishAfterTransition()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            if(data!=null) {
                val mPaths = data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_PATH) as List<*>
                mediaUploader.uploadMedia(mPaths[0] as String, MediaUploader.MediaType.PHOTO)
                        .subscribeBy(
                                onNext = {
                                    when (it) {
                                        is OnProgress -> {

                                        }
                                        is OnComplete -> {
                                            listOfMediaUrls.add(MediaContent("photo", it.url.toString()))
                                            val message = Message("1", User("user", "photo 0 0", "null"), null)
                                            message.setImage(Message.Image(it.url.toString()))
                                            messagesAdapter!!.addToStart(message, true)
                                            appPreferences.setGuideShown()
                                            MainActivity.zoom = true
                                            messagesAdapter!!.addToStart(Message("", User("", "", "null"), getString(R.string.t_message)), true)
                                        }
                                    }

                                }
                        )
            }
        }
    }

    override fun onClick(p0: View?){
        if(p0==a_camera) {
            if (menuOpen)
                hideMenu()
            ImagePicker.Builder(this)
                    .mode(ImagePicker.Mode.CAMERA)
                    .compressLevel(ImagePicker.ComperesLevel.SOFT)
                    .scale(480, 480)
                    .directory(ImagePicker.Directory.DEFAULT)
                    .extension(ImagePicker.Extension.JPG)
                    .allowMultipleImages(false)
                    .enableDebuggingMode(true)
                    .build()
        }
        if (p0==a_video) {
            if (menuOpen)
                hideMenu()
            VideoPicker.Builder(this)
                    .mode(VideoPicker.Mode.CAMERA)
                    .directory(VideoPicker.Directory.DEFAULT)
                    .extension(VideoPicker.Extension.MP4)
                    .enableDebuggingMode(true)
                    .build()
        }
    }

    override fun onAddAttachments() {
        if (!menuOpen) {
            revealMenu()
        } else
            hideMenu()
    }

    private fun revealMenu() {
        menuOpen = true
        div.setBackgroundColor(resources.getColor(name.report.report.R.color.white))
        the_menu.visibility = View.INVISIBLE
        val cx = the_menu.left
        val cy = the_menu.height
        val finalRadius = Math.sqrt((the_menu.height * the_menu.height + the_menu.width * the_menu.width).toDouble()).toInt()
        val anim = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewAnimationUtils.createCircularReveal(the_menu, cx, cy, 0f, finalRadius.toFloat())
        } else {
            TODO("VERSION.SDK_INT < LOLLIPOP")
        }
        anim.duration = 500
        the_menu.visibility = View.VISIBLE
        overlay.visibility = View.VISIBLE
        anim.start()
        val popeup1 = AnimationUtils.loadAnimation(this, name.report.report.R.anim.popeup)
        val popeup2 = AnimationUtils.loadAnimation(this, name.report.report.R.anim.popeup)
        val popeup3 = AnimationUtils.loadAnimation(this, name.report.report.R.anim.popeup)
        popeup1.startOffset = 100
        popeup2.startOffset = 200
        popeup3.startOffset = 300
        menu1.startAnimation(popeup1)
        menu2.startAnimation(popeup2)
        menu3.startAnimation(popeup3)
    }

    private fun hideMenu() {
        div.setBackgroundColor(resources.getColor(name.report.report.R.color.divider))
        menuOpen = false
        val cx = the_menu.left
        val cy = the_menu.height
        val initialRadius = Math.sqrt((the_menu.height * the_menu.height + the_menu.width * the_menu.width).toDouble()).toInt()
        val anim = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewAnimationUtils.createCircularReveal(the_menu, cx, cy, initialRadius.toFloat(), 0f)
        } else {
            TODO("VERSION.SDK_INT < LOLLIPOP")
        }
        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                the_menu.visibility = View.INVISIBLE
                the_menu.visibility = View.GONE
                overlay.visibility = View.INVISIBLE
                overlay.visibility = View.GONE
            }
        })
        anim.start()
    }

    companion object {
        lateinit var viewModel: MainActivityViewModel
        var message = "re::Port"
        fun open(context: Context, string: String) {
            val intent = Intent(context, DefaultMessagesActivityInfo::class.java)
            message = string
            context.startActivity(intent)
        }
    }
}
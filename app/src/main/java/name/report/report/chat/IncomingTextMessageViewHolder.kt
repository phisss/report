package name.report.report.chat

import android.arch.lifecycle.MutableLiveData
import android.view.View
import name.report.report.App
import name.report.report.GlideApp

import name.report.report.R
import com.stfalcon.chatkit.messages.MessagesListAdapter
import de.hdodenhof.circleimageview.CircleImageView

import name.report.report.chat.model.Message
import name.report.report.domain.SendRepoints
import name.report.report.domain.ShowRepoints
import name.report.report.domain.UserMenuBS
import timber.log.Timber

@Suppress("DEPRECATION")
class IncomingTextMessageViewHolder(itemView: View) : MessagesListAdapter.IncomingMessageViewHolder<Message>(itemView) {
    private val photoImage: CircleImageView = itemView.findViewById(name.report.report.R.id.photoImage)

    var userName: String = ""
    var userPhoto: String = ""

    companion object {
        var sendRequest = MutableLiveData<UserMenuBS>()
    }

    override fun onBind(message: Message) {
        super.onBind(message)

        if(message.user.avatar!="null") {
            userPhoto = message.user.avatar!!
            userName = message.user.name

            GlideApp.with(App.context)
                    .load(message.user.avatar)
                    .into(photoImage)
        }else{
            userPhoto = "null"
            userName = "null"
        }

        photoImage.setOnClickListener{
            Timber.e("RE:: ${userName}")
            sendRequest.value = UserMenuBS(userPhoto, userName, message.user.id)
        }
    }
}
package name.report.report.chat


import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView
import name.report.report.App

import name.report.report.R
import name.report.report.domain.usecases.messages.AddRepointsUseCase
import name.report.report.utils.visible
import com.stfalcon.chatkit.messages.MessagesListAdapter

import name.report.report.chat.model.Message
import javax.inject.Inject


@Suppress("DEPRECATION")
class OutcomingImageMessageViewHolder(itemView: View) : MessagesListAdapter.OutcomingMessageViewHolder<Message>(itemView) {
    private val playVideo: ImageView = itemView.findViewById(name.report.report.R.id.videoMessage)
    private val imageView: ImageView = itemView.findViewById(name.report.report.R.id.image)
    private val repoints: TextView = itemView.findViewById(name.report.report.R.id.repoints)

    init {
        App.appComponent.plusFeedComponent().inject(this)
    }
    override fun onBind(message: Message) {
        super.onBind(message)
        imageLoader.loadImage(imageView, message.imageUrl)
        if(message.user.name.split(" ")[0]=="video")
            playVideo.visible()
        repoints.text = message.user.name.split(" ")[message.user.name.split(" ").size-2]

    }
}
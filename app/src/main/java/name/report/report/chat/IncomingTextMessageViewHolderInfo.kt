package name.report.report.chat

import android.view.View
import name.report.report.App
import name.report.report.GlideApp

import name.report.report.R
import com.stfalcon.chatkit.messages.MessagesListAdapter
import de.hdodenhof.circleimageview.CircleImageView

import name.report.report.chat.model.Message

@Suppress("DEPRECATION")
class IncomingTextMessageViewHolderInfo(itemView: View) : MessagesListAdapter.IncomingMessageViewHolder<Message>(itemView) {
    private val photoImage: CircleImageView = itemView.findViewById(name.report.report.R.id.photoImage)

    override fun onBind(message: Message) {
        super.onBind(message)

            GlideApp.with(App.context)
                    .load("")
                    .placeholder(R.drawable.tutorial)
                    .into(photoImage)
    }
}
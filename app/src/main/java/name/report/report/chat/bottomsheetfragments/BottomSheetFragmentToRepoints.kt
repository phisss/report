package name.report.report.chat.bottomsheetfragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.support.design.widget.BottomSheetDialogFragment
import android.view.View
import kotlinx.android.synthetic.main.send_repoint_bs.view.*
import name.report.report.R
import name.report.report.domain.RepointToSend


class BottomSheetFragmentToRepoints : BottomSheetDialogFragment() {

    companion object {
        val sendRepoints = MutableLiveData<RepointToSend>()
        val dismiss = MutableLiveData<Boolean>()
    }



    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val view = View.inflate(context, R.layout.send_repoint_bs, null)
        dialog.setContentView(view)
        view.sendRepointsToUser.setOnClickListener {
            sendRepoints.value = RepointToSend(arguments!!.getString("messageId"), view.numberPicker.value)
            sendRepoints.value = null
            dismiss.value = false
        }
        dismiss.observe(this, Observer {
            if(it!=null)
                if(!it)
                    dismiss()
        })
    }
}

package name.report.report.chat

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import name.report.report.GlideApp
import com.stfalcon.chatkit.commons.ImageLoader
import com.stfalcon.chatkit.messages.MessagesListAdapter
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.incoming_text_message.*
import name.report.report.chat.model.Message


open class DemoMessagesActivity : AppCompatActivity(), MessagesListAdapter.SelectionListener, MessagesListAdapter.OnLoadMoreListener {
    override fun onLoadMore(page: Int, totalItemsCount: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSelectionChanged(count: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    protected var messagesAdapter: MessagesListAdapter<Message>? = null
    protected open lateinit var imageLoader: ImageLoader


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageLoader = ImageLoader { imageView, url ->
            name.report.report.GlideApp.with(applicationContext)
                    .load(url)
                    .centerCrop()
                    .into(imageView)
            }

        }

}

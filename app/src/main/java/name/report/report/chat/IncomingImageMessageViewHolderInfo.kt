package name.report.report.chat

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import name.report.report.App
import name.report.report.GlideApp

import name.report.report.R
import name.report.report.domain.usecases.messages.AddRepointsUseCase
import name.report.report.utils.visible
import com.stfalcon.chatkit.messages.MessagesListAdapter
import de.hdodenhof.circleimageview.CircleImageView
import io.reactivex.rxkotlin.subscribeBy
import name.report.report.MainActivity

import name.report.report.chat.model.Message
import name.report.report.domain.SendRepoints
import name.report.report.domain.ShowRepoints
import timber.log.Timber
import javax.inject.Inject

@Suppress("DEPRECATION")
class IncomingImageMessageViewHolderInfo(itemView: View) : MessagesListAdapter.IncomingMessageViewHolder<Message>(itemView) {
    private val playVideo: ImageView = itemView.findViewById(R.id.videoMessage)
    private val imageView: ImageView = itemView.findViewById(R.id.image)
    private val photoImage: CircleImageView = itemView.findViewById(R.id.photoImage)


    @SuppressLint("SetTextI18n")
    override fun onBind(message: Message) {
        super.onBind(message)
        imageLoader.loadImage(imageView, message.imageUrl)
            GlideApp.with(App.context)
                    .load("")
                    .placeholder(R.drawable.tutorial)
                    .into(photoImage)
        if(message.user.name.split(" ")[0]=="video") {
            playVideo.visible()
        }

    }
}
package name.report.report.chat.modele

import com.stfalcon.chatkit.commons.models.IUser


class User(private val id: String, private val name: String, avatar: String?) : IUser {
    private var avatar: String? = null


    init {
        if (avatar == null)
            this.avatar = "null"
        else
            this.avatar = avatar
    }

    override fun getId(): String {
        return id
    }

    override fun getName(): String {
        return name
    }

    override fun getAvatar(): String? {
        return avatar
    }


}

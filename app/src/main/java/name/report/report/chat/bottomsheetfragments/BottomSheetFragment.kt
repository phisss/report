package name.report.report.chat.bottomsheetfragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.MutableLiveData
import android.support.design.widget.BottomSheetDialogFragment
import android.view.View
import kotlinx.android.synthetic.main.user_menu_bs.view.*
import name.report.report.GlideApp
import name.report.report.R


class BottomSheetFragment : BottomSheetDialogFragment() {
    companion object {
        var sendRequest = MutableLiveData<String>()
    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val view = View.inflate(context, R.layout.user_menu_bs, null)
        dialog.setContentView(view)
        view.userNameTv.text = if(arguments!!.getString("userName")=="null null")
             "Unlogged user"
        else
            arguments!!.getString("userName")
        sendRequest.value = null
        GlideApp.with(this)
                .load(arguments!!.getString("userPhoto"))
                .into(view.userPhotoIV)
        view.sendRequestToUserFromChat.setOnClickListener{
            sendRequest.value = arguments!!.getString("userId")
            dialog.dismiss()
        }
    }
}

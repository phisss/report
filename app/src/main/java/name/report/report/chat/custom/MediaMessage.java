package name.report.report.chat.custom;


import android.view.View;


import com.stfalcon.chatkit.messages.MessageHolders;

import name.report.report.chat.model.Message;

public abstract class MediaMessage extends MessageHolders.BaseMessageViewHolder<Message> {

    protected OnClickMessageListener onClickMessageListener;
    protected OnStartUploadListener onStartUploadListener;
    protected OnCancelUploadListener onCancelUploadListener;
    protected OnStartDownloadListener onStartDownloadListener;
    protected OnCancelDownloadListener onCancelDownloadListener;

    protected abstract Message getMessage();

    protected abstract View getOverlay();

    public MediaMessage(View itemView) {
        super(itemView);
    }

    @Override
    public void onBind(Message message) {

    }


    public void setOnStartUploadListener(OnStartUploadListener onStartUpload) {
        this.onStartUploadListener = onStartUpload;
    }

    public void setOnStartDownloadListener(OnStartDownloadListener onStartDownload) {
        this.onStartDownloadListener = onStartDownload;
    }


    public void setOnCancelUploadListener(OnCancelUploadListener onCancelUploadListener) {
        this.onCancelUploadListener = onCancelUploadListener;
    }

    public void setOnCancelDownloadListener(OnCancelDownloadListener onCancelDownloadListener) {
        this.onCancelDownloadListener = onCancelDownloadListener;
    }

    public void setOnClickMessageListener(OnClickMessageListener onClickMessageListener) {
        this.onClickMessageListener = onClickMessageListener;
    }

    protected void notifyOnStartUpload() {
        if (onStartUploadListener != null) {
            onStartUploadListener.onStart(getMessage());
        }
    }

    protected void notifyOnStartDownload() {
        if (onStartDownloadListener != null) {
            onStartDownloadListener.onStart(getMessage());
        }
    }

    protected void notifyOnCancelUpload() {
        if (onCancelUploadListener != null) {
            onCancelUploadListener.onCancel(getMessage());
        }
    }

    protected void notifyOnCancelDownload() {
        if (onCancelDownloadListener != null) {
            onCancelDownloadListener.onCancel(getMessage());
        }
    }

    protected void notifyOnMessageClick() {
        if (onClickMessageListener != null) {
            onClickMessageListener.onClick(getMessage(), getOverlay());
        }
    }

    public interface OnStartUploadListener {

        void onStart(Message video);

    }

    public interface OnCancelUploadListener {

        void onCancel(Message message);

    }

    public interface OnStartDownloadListener {

        void onStart(Message message);

    }

    public interface OnCancelDownloadListener {

        void onCancel(Message message);

    }

    public interface OnClickMessageListener {

        void onClick(Message message, View view);

    }
}

package name.report.report.chat

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import name.report.report.App
import name.report.report.GlideApp

import name.report.report.R
import name.report.report.domain.usecases.messages.AddRepointsUseCase
import name.report.report.utils.visible
import com.stfalcon.chatkit.messages.MessagesListAdapter
import de.hdodenhof.circleimageview.CircleImageView
import io.reactivex.rxkotlin.subscribeBy
import name.report.report.chat.bottomsheetfragments.BottomSheetFragmentToRepoints

import name.report.report.chat.model.Message
import name.report.report.domain.SendRepoints
import name.report.report.domain.UserMenuBS
import timber.log.Timber
import javax.inject.Inject

@Suppress("DEPRECATION")
class IncomingImageMessageViewHolder(itemView: View) : MessagesListAdapter.IncomingMessageViewHolder<Message>(itemView) {
    private val playVideo: ImageView = itemView.findViewById(R.id.videoMessage)
    private val imageView: ImageView = itemView.findViewById(R.id.image)
    private val repoints: TextView = itemView.findViewById(R.id.repoints)
    private val photoImage: CircleImageView = itemView.findViewById(R.id.photoImage)
    private val added: ImageView = itemView.findViewById(R.id.added)
    private val addRepoint: RelativeLayout = itemView.findViewById(R.id.addRepoint)

    @Inject
    lateinit var addRepointsUseCase: AddRepointsUseCase


    companion object {
        var showRepoints = MutableLiveData<String>()
        var sendRepoints = MutableLiveData<SendRepoints>()
        var activity = MutableLiveData<DefaultMessagesActivity>()
        var sendRequest = MutableLiveData<UserMenuBS>()
    }

    var userName: String = ""
    var userPhoto: String = ""

    init {
        App.appComponent.plusFeedComponent().inject(this)
    }

    @SuppressLint("SetTextI18n")
    override fun onBind(message: Message) {
        super.onBind(message)
        imageLoader.loadImage(imageView, message.imageUrl)
        if(message.user.avatar!="null") {
            userPhoto = message.user.avatar!!
            userName = message.user.name
            GlideApp.with(App.context)
                    .load(message.user.avatar)
                    .into(photoImage)
        }else{
            userPhoto = "null"
            userName = "null"
        }
        photoImage.setOnClickListener{
            sendRequest.value = UserMenuBS(userPhoto, userName, message.user.id)
        }
        addRepoint.setOnClickListener{
            Timber.e("RE:: ${message.id}")
            showRepoints.value = message.id
        }

        if(message.user.name.split(" ")[0]=="video") {
            playVideo.visible()
        }

        sendRepoints.observe(activity.value!!, Observer {
            repoint ->
            if(repoint!=null)
                    if(message.id == repoint.messageId) {
                        addRepointsUseCase.execute(message.id, repoint.value)
                                .subscribeBy(
                                        onSuccess = {
                                            try {
                                                setAddedRepLayout()
                                                BottomSheetFragmentToRepoints.dismiss.value = true
                                                repoints.text = (repoints.text.toString().toInt() + repoint.value).toString()
                                            } catch (e: Exception) {
                                                e.printStackTrace()
                                            }
                                        },
                                        onError = {
                                        }
                                )
                    }
        })
        if(message.user.name.split(" ")[0]=="video"||message.user.name.split(" ")[0]=="photo") {
            if(message.user.name.split(" ")[2]=="1") {
               setAddedRepLayout()
            }
        }
        repoints.text = message.user.name.split(" ")[message.user.name.split(" ").size-2]

    }

    private fun setAddedRepLayout(){
        added.setBackgroundDrawable(App.context.getDrawable(R.drawable.repoint_out_bg_added))
        added.setImageDrawable(App.context.getDrawable(R.drawable.ic_rep_added))
        repoints.setTextColor(Color.parseColor("#ff2d55"))
        addRepoint.setOnClickListener{
            Timber.e("RE:: Repoints sended")
        }
    }
}
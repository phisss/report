package name.report.report

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import name.report.report.di.app.AppComponent
import name.report.report.di.app.AppModule
import name.report.report.di.app.DaggerAppComponent
import com.facebook.drawee.backends.pipeline.Fresco
import name.report.report.data.trackLocation.Util
import timber.log.Timber

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
            private set
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
        Timber.plant(Timber.DebugTree())
        App.Companion.context = applicationContext
        App.Companion.appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
        Util.scheduleLocationJob(context)
    }
}

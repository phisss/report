package name.report.report.utils

import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.support.annotation.LayoutRes
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.storage.UploadTask
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request
import java.util.*

/**
 * Check list of permissions for granted
 */
fun Context.checkPermissionsGranted(permissions: List<String>) = permissions.all {
    PackageManager.PERMISSION_GRANTED == this.packageManager.checkPermission(it, this.packageName)
}

/**
 * Poll all elems from queue and execute action
 */
fun <T> Queue<T>.pollAll(action: (element: T) -> Unit) {
    while (size != 0) {
        action(poll())
    }
}

/**
 * Get json from [SharedPreferences] and deserialize
 *
 * @param key - preference's key
 * @param clazz - type of the deserializable object
 */
fun <T> SharedPreferences.getAndDeserialize(key: String, clazz: Class<T>): T? {
    val json = this.getString(key, null) ?: return null
    return Gson().fromJson(json, clazz)
}

/**
 * Serialize the [Any] and put in the shared preferences
 *
 * @param key - preference's key
 * @param any - object to serialize
 */
fun SharedPreferences.serializeAndPut(key: String, any: Any)
        = this.edit().putString(key, Gson().toJson(any)).apply()

/**
 * Convert the file bytes uploaded to percents
 */
fun UploadTask.TaskSnapshot.bytesProgressToIntPercent()
        = ((100.0 * this.bytesTransferred) / this.totalByteCount)

/**
 * Building for [OkHttpClient]
 *
 * @param building - building for client
 */
fun OkHttpClient.Builder.build(building: OkHttpClient.Builder.() -> OkHttpClient.Builder)
        = building().build()!!

/**
 * Add headers for [Request.Builder] as list of Pairs
 *
 * @param headers - list of pairs, first - name, second - value
 */
fun Request.Builder.addHeaders(headers: List<Pair<String, String>>): Request.Builder {
    headers.forEach { addHeader(it.first, it.second) }
    return this
}

/**
 * Fragment transactions.
 */
fun FragmentManager.transaction(transaction: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().transaction().commit()
}

/**
 * View group inflation.
 */
fun ViewGroup.inflate(@LayoutRes layoutRes: Int, parent: ViewGroup = this, attachToRoot: Boolean = false): View =
        LayoutInflater.from(context).inflate(layoutRes, parent, attachToRoot)

/**
 * Sets visibility of view to [View.VISIBLE]
 */
fun View.visible() {
    visibility = View.VISIBLE
}

/**
 * Sets visibility of view to [View.INVISIBLE]
 */
fun View.invisible() {
    visibility = View.INVISIBLE
}

/**
 * Sets visibility of view to [View.GONE]
 */
fun View.gone() {
    visibility = View.GONE
}

/**
 * Converts int value to dp
 */
fun Int.toDp(resources: Resources) = (this * resources.displayMetrics.density + 0.5f).toInt()

/**
 * Returns screen center coordinates
 */
fun Resources.getWidth() = this.displayMetrics.widthPixels / 2

fun Resources.getHeight() = this.displayMetrics.heightPixels / 2

/**
 * Returns status bar height
 */
fun Resources.getStatusBarHeight() = this.getDimensionPixelSize(this.getIdentifier("status_bar_height", "dimen", "android"))

/**
 * Set margins of view
 */
fun View.setMargins(left: Int, top: Int, right: Int) {
    val marginParams = this.layoutParams as ViewGroup.MarginLayoutParams
    marginParams.setMargins(left, top, right, 0)
}


/**
 * Convert Drawable to BitmapDescriptor
 */

fun Drawable.toBitmapDescriptor(): BitmapDescriptor {
    this.setBounds(0, 0, this.intrinsicWidth, this.intrinsicHeight)
    val bitmap = Bitmap.createBitmap(this.intrinsicWidth, this.intrinsicHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    this.draw(canvas)
    return BitmapDescriptorFactory.fromBitmap(bitmap)
}

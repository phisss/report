package name.report.report.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

public class MagicTextView extends android.support.v7.widget.AppCompatTextView {

    // constructors
    public MagicTextView(Context context) {
        this(context, null, 0);
    }

    public MagicTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MagicTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void draw(Canvas canvas) {
        for (int i = 0; i < 5; i++) {
            super.draw(canvas);
        }
    }
}

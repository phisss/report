@file:Suppress("DEPRECATION", "OverridingDeprecatedMember")

package name.report.report

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.View

import name.report.report.presentation.feed.view.FeedFragment


import java.util.ArrayList

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val fragments = ArrayList<Fragment>()
    private val mFragmentTitleList = ArrayList<String>()
    private var currentFragment: Fragment? = null

    init {
        fragments.clear()
        fragments.add(FeedFragment.newInstance(FeedFragment.FEED_TYPE))
        fragments.add(FeedFragment.newInstance(FeedFragment.CUR_USER_REQUEST_TYPE))
        fragments.add(FeedFragment.newInstance(FeedFragment.INCOMING_REQUESTS_TYPE))
        mFragmentTitleList.add("Feed")
        mFragmentTitleList.add("Answers")
        mFragmentTitleList.add("Requests")
    }

    override fun setPrimaryItem(container: View, position: Int, `object`: Any) {
        if (currentFragment !== `object`) {
            currentFragment = `object` as Fragment
        }
        super.setPrimaryItem(container, position, `object`)
    }

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

    override fun getPageTitle(position: Int): CharSequence? = mFragmentTitleList[position]
}
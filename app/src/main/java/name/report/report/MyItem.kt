package name.report.report

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class MyItem(lat: Double, lng: Double, val title: String) : ClusterItem {
    private val mPosition: LatLng = LatLng(lat, lng)

    override fun getPosition(): LatLng {
        return mPosition
    }
}
